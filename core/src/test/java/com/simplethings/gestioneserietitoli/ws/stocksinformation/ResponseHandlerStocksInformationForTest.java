package com.simplethings.gestioneserietitoli.ws.stocksinformation;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerStocksInformation;
import com.simplethings.gestioneserietitoli.parser.StockInformationParser.CompanyInformation;

public class ResponseHandlerStocksInformationForTest extends ResponseHandlerStocksInformation {
	
	private static Logger logger = LoggerFactory.getLogger(ResponseHandlerStocksInformationForTest.class);
	private BlockingQueue<Optional<CompanyInformation>> outputQueue = 
				new LinkedBlockingQueue<Optional<CompanyInformation>>();
	
	public ResponseHandlerStocksInformationForTest() {
		super(null);
	}

	private class StoreInDBSimulator implements Runnable{
		
		private BlockingQueue<Optional<CompanyInformation>> queue;
		
		public StoreInDBSimulator(BlockingQueue<Optional<CompanyInformation>> queue){
			this.queue = queue;
		}
		
		@Override
		public void run() {
			long counter = 0;
			CompanyInformation companyInformation = null;
			while(true){
				
				try {
					// logger.debug(">>>>> BEFORE TAKE");
					Optional<CompanyInformation> optionalCompanyInformation = queue.take();
					// logger.debug("<<<<< AFTER TAKE");
					
					if(!optionalCompanyInformation.isPresent())
						break;
					
					companyInformation = optionalCompanyInformation.get();
					// logger.debug("<<<<< AFTER TAKE2");
					
					outputQueue.put(optionalCompanyInformation);
					
					// logger.debug("*** -> " + counter++);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			}
			
		}
		
		
	}
	
	
	public BlockingQueue<Optional<CompanyInformation>> getQueue() {
		return outputQueue;
	}
	
	public Thread doStartQueueProcessor(
			BlockingQueue<Optional<CompanyInformation>> queueIndustryInformation) {
		Thread td = new Thread(new StoreInDBSimulator(queueIndustryInformation));
		td.start();
		return td;
	}
	
}
