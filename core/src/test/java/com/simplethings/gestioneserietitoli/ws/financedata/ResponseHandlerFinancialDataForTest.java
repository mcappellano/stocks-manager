package com.simplethings.gestioneserietitoli.ws.financedata;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerFinancialData;

public class ResponseHandlerFinancialDataForTest extends ResponseHandlerFinancialData {
	
	private static Logger logger = LoggerFactory.getLogger(ResponseHandlerFinancialDataForTest.class);
	
	private final BlockingQueue<Optional<String>> queueOutput = new LinkedBlockingQueue<Optional<String>>();
	
	public ResponseHandlerFinancialDataForTest() {
		super(null);
	}

	/*
	 * Execute read of queue and stop when finished
	 * 
	 */
	
	public class Worker implements Runnable {
		
		private String stockName;
		private final BlockingQueue<Optional<String>> queue;
		
		
		
		public Worker(String stockName, BlockingQueue<Optional<String>> queue){
			this.stockName = stockName;
			this.queue = queue;
		}
		
		
		public void run(){
			Optional<String> item = null;
			try {
				while((item = queue.take())!=null){
					
					if(!item.isPresent()){
						break;
					}
					
					queueOutput.put(item);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}

	@Override
	public Thread doStartQueueProcessor(String stockName, final BlockingQueue<Optional<String>> queue) throws ApplicationException {
		Thread tdWorker = new Thread(new Worker(stockName, queue));
		tdWorker.start();
		return tdWorker;
	}
	
	public 	BlockingQueue<Optional<String>> getOutputQueue(){
		return queueOutput;
	}
	
	
}
