package com.simplethings.gestioneserietitoli.ws.stocksinformation;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.BlockingQueue;

import org.junit.Test;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerStocksInformation;
import com.simplethings.gestioneserietitoli.parser.StockInformationParser.CompanyInformation;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse.STATUS;

public class StocksInformationWsClientTest {
	
	@Test
	public void AtestDoWork() throws ApplicationException {
		
		ResponseHandlerStocksInformation responseHandlerStocksInformation = new ResponseHandlerStocksInformationForTest();
		
		StocksInformationWsClient client = new StocksInformationWsClient(responseHandlerStocksInformation);
		
		StocksInformationResponse response = client.doWork();
		
		assertEquals(STATUS.OK, response.status);
		
		BlockingQueue<Optional<CompanyInformation>> queue = ((ResponseHandlerStocksInformationForTest) responseHandlerStocksInformation).getQueue();
		
		System.out.println("QUEUE.SIZE: "+ queue.size());
	}

}
