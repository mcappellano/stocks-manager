package com.simplethings.gestioneserietitoli.ws.financedata;

import static com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest.TradingPeriod.DAILY;
import static com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest.TradingPeriod.MONTLY;
import static com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest.TradingPeriod.WEEKLY;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Calendar;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest;

public class FinanceDataRequestTest {
	
	@BeforeClass
	public static void init(){
		 BasicConfigurator.configure();
	}
	
	@Test
	public void testBuildQueryStringFromDate() throws ParseException {
		
		String dateValueFrom = "15/03/2000";
		
		Calendar fromDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueFrom);
		Calendar toDate = null;
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, null);
		
		String query = request.buildQueryString();
		
		assertEquals("?s=IBM&a=2&b=15&c=2000&ignore=.csv", query);
		
	}
	
	@Test
	public void testBuildQueryStringToDate() throws ParseException {
		
		String dateValueTo = "31/01/2010";
		
		Calendar fromDate = null;
		Calendar toDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueTo);
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, null);
		
		String query = request.buildQueryString();
		
		assertEquals("?s=IBM&d=0&e=31&f=2010&ignore=.csv", query);
		
	}

	@Test
	public void testBuildQueryStringCompleteDate() throws ParseException {
		
		String dateValueFrom = "15/03/2000";
		
		Calendar fromDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueFrom);
		
		String dateValueTo = "31/01/2010";
		
		Calendar toDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueTo);
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, null);
		
		String query = request.buildQueryString();
		
		assertEquals("?s=IBM&a=2&b=15&c=2000&d=0&e=31&f=2010&ignore=.csv", query);
		
	}
	
	@Test
	public void testTradingPeriodsWeek(){
		
		Calendar fromDate = null;
		Calendar toDate = null;
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, WEEKLY);
		
		String query = request.buildQueryString();
		
		assertEquals("?s=IBM&g=w&ignore=.csv", query);
		
	}
	
	@Test
	public void testTradingPeriodsDaily(){
		
		Calendar fromDate = null;
		Calendar toDate = null;
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, DAILY);
		
		String query = request.buildQueryString();
		
		assertEquals("?s=IBM&g=d&ignore=.csv", query);
		
	}
	
	@Test
	public void testTradingPeriodsMontly(){
		
		Calendar fromDate = null;
		Calendar toDate = null;
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, MONTLY);
		
		String query = request.buildQueryString();
		
		assertEquals("?s=IBM&g=m&ignore=.csv", query);
		
	}
	
	
	
}
