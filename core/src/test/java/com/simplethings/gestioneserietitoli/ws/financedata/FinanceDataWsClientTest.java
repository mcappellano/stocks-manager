package com.simplethings.gestioneserietitoli.ws.financedata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.text.ParseException;
import java.util.Calendar;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerFinancialData;
import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest.TradingPeriod;


public class FinanceDataWsClientTest {

	@BeforeClass
	public static void init() throws ApplicationException{
		 BasicConfigurator.configure();
	}
	
	@Test
	public void testOkRequest() throws ParseException, ApplicationException {
		
		String dateValueFrom = "15/03/2000";
		
		Calendar fromDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueFrom);
		
		String dateValueTo = "31/01/2010";
		
		Calendar toDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueTo);
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, toDate, TradingPeriod.DAILY);
		
		ResponseHandlerFinancialData responseHandler = new ResponseHandlerFinancialDataForTest();
		
		FinanceDataWsClient wsClient = new FinanceDataWsClient(responseHandler);
				
		FiananceDataResponse resp = wsClient.doRequest(request);
		
		assertEquals(WsClientResponse.STATUS.OK, resp.status);
		
		BlockingQueue<Optional<String>> queue = ((ResponseHandlerFinancialDataForTest)responseHandler).getOutputQueue();
		
		System.out.println("**************************************" + queue.size());
		System.out.println("SIZE QUEUE: " + queue.size());
		
		System.out.println("SIZE QUEUE2: " + queue.size());
	}
	
	@Test
	public void testNoDataRequest() throws ParseException, ApplicationException {
		
		String dateValueFrom = "15/03/2044";
		
		Calendar fromDate = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValueFrom);
		
		FinanceDataRequest request = 
				new FinanceDataRequest("IBM",fromDate, null, TradingPeriod.DAILY);
		
		ResponseHandlerFinancialData responseHandler = new ResponseHandlerFinancialDataForTest();
		
		FinanceDataWsClient wsClient = new FinanceDataWsClient(responseHandler);
			
		WsClientResponse resp = wsClient.doRequest(request);

		assertEquals(WsClientResponse.STATUS.NO_RESPONSE, resp.status);
		assertEquals(0, ((ResponseHandlerFinancialDataForTest)responseHandler).getOutputQueue().size());
		
	}
	
	

}
