package com.simplethings.gestioneserietitoli.conf;

import static org.junit.Assert.*;

import org.junit.Test;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class ConfigTest {

	@Test
	public void testLoadConfig() throws ApplicationException {
		String configFileName = "/config.properties";
		
		Config config = Config.getConfig(configFileName);
		
		assertEquals("jdbc/STOCK_SERIES_DS", config.dataSourceUrl);
		
	}

}
