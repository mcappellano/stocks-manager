package com.simplethings.gestioneserietitoli.conf;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.simplethings.gestioneserietitoli.conf.DbConfig;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class DbConfigTest {
	
	@Test
	public void testLoadConfig() throws ApplicationException {
		String configFileName = "/db_config.properties";
		
		DbConfig config = DbConfig.getConfig(configFileName);
		
		assertEquals("org.h2.Driver", config.dbDriverClassName);
		// assertTrue( config.dbUrl.startsWith("jdbc:h2:~/STOCK_SERIES"));
		assertEquals("sa", config.dbUsername);
		assertEquals("", config.dbPassword);
	}
}
