package com.simplethings.gestioneserietitoli.model;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.model.StockMonthSeriesRow;
import com.simplethings.gestioneserietitoli.utils.DateUtils;

@RunWith(JUnitParamsRunner.class)
public class StockMonthSeriesTest {

	@BeforeClass
	public static void init(){
		 BasicConfigurator.configure();
	}
	
	@Test
	@Parameters(method = "financialDataValues")
	public void testAddFinanceDataToStockMonthSeries(FinanceData d_1, FinanceData d_2, FinanceData d_3) throws ParseException {
		
		StockMonthSeries numericMonthSeries = new StockMonthSeries("IBM");
			
		numericMonthSeries.addFinanceData(d_1);
		numericMonthSeries.addFinanceData(d_2);
		numericMonthSeries.addFinanceData(d_3);
		
		assertEquals("IBM", numericMonthSeries.stockName);
		
		Date startCampioneExpected = 
				DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		
		Date endCampioneExpected = 
				DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		StockMonthSeriesRow stockMonthSeriesRow = numericMonthSeries.convertToStrockMonthSeriesRow();
		
		assertEquals("IBM", stockMonthSeriesRow.stockName);
		assertEquals(startCampioneExpected, stockMonthSeriesRow.startDate);
		assertEquals(endCampioneExpected, stockMonthSeriesRow.endDate);
		
	    /*
		 * Es. dati da feed:
		 * 
		   2013-11-06,177.91,179.75,177.78,179.19,4560700,179.19
		   2013-11-05,179.54,179.80,177.71,177.85,6096800,176.90
	       2013-11-04,179.90,180.80,179.34,180.27,3483300,179.31
	     *
		 */
		
		assertEquals("040506", stockMonthSeriesRow.daySeries);
		
		assertEquals("179.90_179.54_177.91", stockMonthSeriesRow.openValueSeries);
		
		assertEquals("180.80_179.80_179.75", stockMonthSeriesRow.heightValueSeries);
		
		assertEquals("179.34_177.71_177.78", stockMonthSeriesRow.lowValueSeries);
		
		assertEquals("180.27_177.85_179.19", stockMonthSeriesRow.closeValueSeries);
		
		assertEquals("3483300_6096800_4560700", stockMonthSeriesRow.volumeValueSeries);
		
		assertEquals("179.31_176.90_179.19", stockMonthSeriesRow.adjCloseValueSeries);
		
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	@Parameters(method = "failFinancialDataValues")
	public void testFailAddFinanceDataToStockMonthSeries(FinanceData d_1, FinanceData d_2, FinanceData d_3) throws ParseException {
		
		StockMonthSeries numericMonthSeries = new StockMonthSeries("IBM");
			
		numericMonthSeries.addFinanceData(d_1);
		numericMonthSeries.addFinanceData(d_2);
		numericMonthSeries.addFinanceData(d_3);
				
	}

	/*
	 * Es. dati da feed:
	 * 
	   2013-11-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-11-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-11-04,179.90,180.80,179.34,180.27,3483300,179.31
     *
	 */
	
	private Object[] financialDataValues() throws ParseException{
		FinanceData[] financeData = new FinanceData[3];
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		financeData[0] = new FinanceData(data_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeData[1] = new FinanceData(data_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeData[2] = new FinanceData(data_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		return $((Object)financeData);
	}
	
	private Object[] failFinancialDataValues() throws ParseException{
		FinanceData[] financeData_1 = new FinanceData[3];
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		
		// change month: illegal in add
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-06");
		
		financeData_1[0] = new FinanceData(data_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeData_1[1] = new FinanceData(data_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeData_1[2] = new FinanceData(data_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		FinanceData[] financeData_2 = new FinanceData[3];
		
		Date data_A = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_B = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		
		// change year: illegal in add
		Date data_C = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2014-11-06");
		
		financeData_2[0] = new FinanceData(data_A, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeData_2[1] = new FinanceData(data_B, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeData_2[2] = new FinanceData(data_C, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		return $(financeData_1, financeData_2);
	}
	
	@Test
	public void testDeserializer() throws ParseException{
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		/*
		 *
		 * assertEquals("040506", numericMonthSeries.valueSeriesDay());
		
		assertEquals("179.90_179.54_177.91", numericMonthSeries.valueOpenValueSeries());
		
		assertEquals("180.80_179.80_179.75", numericMonthSeries.valueHightValueSeries());
		
		assertEquals("179.34_177.71_177.78", numericMonthSeries.valueLowValueSeries());
		
		assertEquals("180.27_177.85_179.19", numericMonthSeries.valueCloseValueSeries());
		
		assertEquals("3483300_6096800_4560700", numericMonthSeries.valueVolumeSeries());
		
		assertEquals("179.31_176.90_179.19", numericMonthSeries.valueAdjCloseValueSeries());
		 */
		String stock = "IBM";
		String seriesOfValueDay = "040506";
		String seriesOfOpenValue = "179.90_179.54_177.91";
		String seriesOfHightValue = "180.80_179.80_179.75";
		String seriesOfLowValue = "179.34_177.71_177.78";
		String seriesOfCloseValue = "180.27_177.85_179.19";
		String seriesOfVolumeValue = "3483300_6096800_4560700";
		String seriesOfAdjCloseValue = "179.31_176.90_179.19";
		
		StockMonthSeriesRow stockMonthSeriesRow	= new StockMonthSeriesRow(stock, data_1, data_3, seriesOfValueDay, 
				seriesOfOpenValue, seriesOfHightValue, seriesOfLowValue, seriesOfCloseValue, 
				seriesOfVolumeValue, seriesOfAdjCloseValue);
		
		StockMonthSeries numericMonthSeries = stockMonthSeriesRow.convertToNumericMonthSeries();
		
		assertEquals("IBM", numericMonthSeries.stockName);
		
		List<FinanceData> listOfFinanceData = 
				new ArrayList<FinanceData>(numericMonthSeries.getFinanceDataSet());
		
		assertEquals(3, listOfFinanceData.size());
		FinanceData financeData_1 = listOfFinanceData.get(0);
		FinanceData financeData_2 = listOfFinanceData.get(1);
		FinanceData financeData_3 = listOfFinanceData.get(2);
		
		// implementare la equals in finance data
		Date dataExp_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date dataExp_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		Date dataExp_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		FinanceData financeDataExpected_1 = new FinanceData(dataExp_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		FinanceData financeDataExpected_2 = new FinanceData(dataExp_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		FinanceData financeDataExpected_3 = new FinanceData(dataExp_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		assertEquals(financeDataExpected_1, financeData_1);
		assertEquals(financeDataExpected_2, financeData_2);
		assertEquals(financeDataExpected_3, financeData_3);
	}
	
	@Test
	public void testSerializeNumericMonthSeries() throws ParseException{
		List<FinanceData> listFinanceData = buildListOfFinanceData();
				
		List<StockMonthSeries> listNumericMonthSeries = StockMonthSeries.serializeNumericMonthSeries("IBM", listFinanceData);
		
		assertEquals(2, listNumericMonthSeries.size());
		
		StockMonthSeries numericMonthSeries_1 = listNumericMonthSeries.get(0);
		assertEquals("IBM", numericMonthSeries_1.stockName);
		
		StockMonthSeriesRow stockMonthSeriesRow_1 = numericMonthSeries_1.convertToStrockMonthSeriesRow();
		assertEquals("IBM", stockMonthSeriesRow_1.stockName);
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		StockMonthSeries numericMonthSeries_2 = listNumericMonthSeries.get(1);
		assertEquals("IBM", numericMonthSeries_2.stockName);
		
		StockMonthSeriesRow stockMonthSeriesRow_2 = numericMonthSeries_2.convertToStrockMonthSeriesRow();
		assertEquals("IBM", stockMonthSeriesRow_2.stockName);
		
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-04");
		Date data_4 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-06");
	
		assertEquals(data_1, stockMonthSeriesRow_1.startDate);
		assertEquals(data_2, stockMonthSeriesRow_1.endDate);
		
		assertEquals("040506", stockMonthSeriesRow_1.daySeries);
		
		assertEquals("179.90_179.54_177.91", stockMonthSeriesRow_1.openValueSeries);
		
		assertEquals("180.80_179.80_179.75", stockMonthSeriesRow_1.heightValueSeries);
		
		assertEquals("179.34_177.71_177.78", stockMonthSeriesRow_1.lowValueSeries);
		
		assertEquals("180.27_177.85_179.19", stockMonthSeriesRow_1.closeValueSeries);
		
		assertEquals("3483300_6096800_4560700", stockMonthSeriesRow_1.volumeValueSeries);
		
		assertEquals("179.31_176.90_179.19", stockMonthSeriesRow_1.adjCloseValueSeries);
		
		
		assertEquals(data_3, stockMonthSeriesRow_2.startDate);
		assertEquals(data_4, stockMonthSeriesRow_2.endDate);
		
		assertEquals("040506", stockMonthSeriesRow_2.daySeries);
		
		assertEquals("179.90_179.54_177.91", stockMonthSeriesRow_2.openValueSeries);
		
		assertEquals("180.80_179.80_179.75", stockMonthSeriesRow_2.heightValueSeries);
		
		assertEquals("179.34_177.71_177.78", stockMonthSeriesRow_2.lowValueSeries);
		
		assertEquals("180.27_177.85_179.19", stockMonthSeriesRow_2.closeValueSeries);
		
		assertEquals("3483300_6096800_4560700", stockMonthSeriesRow_2.volumeValueSeries);
		
		assertEquals("179.31_176.90_179.19", stockMonthSeriesRow_2.adjCloseValueSeries);
	}
	
	/*
	 * 
	 * Es. dati da feed:
	 * 
	   2013-12-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-12-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-12-04,179.90,180.80,179.34,180.27,3483300,179.31
       
	   2013-11-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-11-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-11-04,179.90,180.80,179.34,180.27,3483300,179.31
     *
	 */
	 
	private List<FinanceData> buildListOfFinanceData() throws ParseException{
		List<FinanceData> listFinanceData = new ArrayList<FinanceData>();
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		listFinanceData.add(new FinanceData(data_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31));
		listFinanceData.add(new FinanceData(data_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90));
		listFinanceData.add(new FinanceData(data_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19));
		
		Date data_4 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-04");
		Date data_5 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-05");
		Date data_6 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-06");
		
		listFinanceData.add(new FinanceData(data_4, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31));
		listFinanceData.add(new FinanceData(data_5, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90));
		listFinanceData.add(new FinanceData(data_6, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19));
		
		return listFinanceData;
	}
	
	@Test
	@Parameters(method = "buildTestListIsMergiable")
	public void testIsMergiableStockMonthSeries(String dateA1, String dateA2, String dateA3,
			String dateB1, String dateB2, String dateB3, boolean isMergiable) throws ParseException{
		
		FinanceData[] financeData = new FinanceData[3];
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, dateA1);
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, dateA2);
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, dateA3);
		
		financeData[0] = new FinanceData(data_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeData[1] = new FinanceData(data_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeData[2] = new FinanceData(data_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		StockMonthSeries numericMonthSeries = new StockMonthSeries("IBM");
			
		numericMonthSeries.addFinanceData(financeData[0]);
		numericMonthSeries.addFinanceData(financeData[1]);
		numericMonthSeries.addFinanceData(financeData[2]);
		
		FinanceData[] financeDataOther = new FinanceData[3];
		
		Date dataOther_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, dateB1);
		Date dataOther_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, dateB2);
		Date dataOther_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, dateB3);
		
		financeDataOther[0] = new FinanceData(dataOther_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeDataOther[1] = new FinanceData(dataOther_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeDataOther[2] = new FinanceData(dataOther_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		StockMonthSeries numericMonthSeriesOther = new StockMonthSeries("IBM");
		
		numericMonthSeriesOther.addFinanceData(financeDataOther[0]);
		numericMonthSeriesOther.addFinanceData(financeDataOther[1]);
		numericMonthSeriesOther.addFinanceData(financeDataOther[2]);
		
		assertEquals(isMergiable, numericMonthSeries.isMergiable(numericMonthSeriesOther));
	}
	
	public Object[] buildTestListIsMergiable() {
		return $(
				$("2013-11-04", "2013-11-05", "2013-11-06", "2013-11-09", "2013-11-10", "2013-11-11", true),
				$("2013-11-04", "2013-11-05", "2013-11-06", "2013-12-09", "2013-12-10", "2013-12-11", false)
				);
		
	}
	
	@Test
	public void testAddStockMonthSeries() throws ParseException {
		
		FinanceData[] financeData = new FinanceData[3];
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		financeData[0] = new FinanceData(data_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeData[1] = new FinanceData(data_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeData[2] = new FinanceData(data_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		StockMonthSeries numericMonthSeries = new StockMonthSeries("IBM");
			
		numericMonthSeries.addFinanceData(financeData[0]);
		numericMonthSeries.addFinanceData(financeData[1]);
		numericMonthSeries.addFinanceData(financeData[2]);
		
		FinanceData[] financeDataOther = new FinanceData[3];
		
		Date dataOther_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-08");
		Date dataOther_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-09");
		Date dataOther_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-10");
		
		financeDataOther[0] = new FinanceData(dataOther_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31);
		financeDataOther[1] = new FinanceData(dataOther_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90);
		financeDataOther[2] = new FinanceData(dataOther_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19);
		
		StockMonthSeries numericMonthSeriesOther = new StockMonthSeries("IBM");
		
		numericMonthSeriesOther.addFinanceData(financeDataOther[0]);
		numericMonthSeriesOther.addFinanceData(financeDataOther[1]);
		numericMonthSeriesOther.addFinanceData(financeDataOther[2]);
		
		assertTrue(numericMonthSeries.isMergiable(numericMonthSeriesOther));
		
		StockMonthSeries numericMonthSeriesMerge = numericMonthSeries.merge(numericMonthSeriesOther);
		
		assertEquals(3, numericMonthSeries.getFinanceDataSet().size());
		assertEquals(3, numericMonthSeriesOther.getFinanceDataSet().size());
		
		assertEquals("IBM", numericMonthSeriesMerge.stockName);
		assertEquals(6, numericMonthSeriesMerge.getFinanceDataSet().size());
		
		StockMonthSeriesRow numericMonthSeriesMergeRow = 
				numericMonthSeriesMerge.convertToStrockMonthSeriesRow();
		
		Date dataResultStart = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date dataResultEnd = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-10");
	
		assertEquals("IBM", numericMonthSeriesMergeRow.stockName);
		assertEquals(dataResultStart, numericMonthSeriesMergeRow.startDate);
		assertEquals(dataResultEnd, numericMonthSeriesMergeRow.endDate);
		
		assertEquals("040506080910", numericMonthSeriesMergeRow.daySeries);
		
		assertEquals("179.90_179.54_177.91_179.90_179.54_177.91", numericMonthSeriesMergeRow.openValueSeries);
		
		assertEquals("180.80_179.80_179.75_180.80_179.80_179.75", numericMonthSeriesMergeRow.heightValueSeries);
		
		assertEquals("179.34_177.71_177.78_179.34_177.71_177.78", numericMonthSeriesMergeRow.lowValueSeries);
		
		assertEquals("180.27_177.85_179.19_180.27_177.85_179.19", numericMonthSeriesMergeRow.closeValueSeries);
		
		assertEquals("3483300_6096800_4560700_3483300_6096800_4560700", numericMonthSeriesMergeRow.volumeValueSeries);
		
		assertEquals("179.31_176.90_179.19_179.31_176.90_179.19", numericMonthSeriesMergeRow.adjCloseValueSeries);
		
	}
	
	@Test
	public void testInvalidAddFinanceData(){
		
	}
}
