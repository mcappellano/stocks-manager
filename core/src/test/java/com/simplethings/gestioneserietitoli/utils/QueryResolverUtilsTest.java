package com.simplethings.gestioneserietitoli.utils;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.utils.QueryResolverUtils;

public class QueryResolverUtilsTest {

	@Test
	public void testFilterFromDate() throws Exception {
		List<FinanceData> listFinanceData = buildListOfFinanceData();
		Date fromDate = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		// 4 element - no elem 0, 1
		List<FinanceData> expectedList = new ArrayList<FinanceData>();
		
		expectedList.add(listFinanceData.get(2));
		expectedList.add(listFinanceData.get(3));
		expectedList.add(listFinanceData.get(4));
		expectedList.add(listFinanceData.get(5));
		
				
		List<FinanceData> resultListFinanceData = 
				QueryResolverUtils.filterFromDate(listFinanceData, fromDate);
		
		
		assertEquals(4, resultListFinanceData.size());
		assertEquals(expectedList, resultListFinanceData);
	}
	
	@Test
	public void testFilterToDate() throws Exception {
		List<FinanceData> listFinanceData = buildListOfFinanceData();
		Date toDate = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-04");
		// 4 element - no elem 4, 5
		List<FinanceData> expectedList = new ArrayList<FinanceData>();
		
		expectedList.add(listFinanceData.get(0));
		expectedList.add(listFinanceData.get(1));
		expectedList.add(listFinanceData.get(2));
		expectedList.add(listFinanceData.get(3));
		
		List<FinanceData> resultListFinanceData = 
				QueryResolverUtils.filterToDate(listFinanceData, toDate);
		
		assertEquals(4, resultListFinanceData.size());
		assertEquals(expectedList, resultListFinanceData);
	}
	
	@Test
	public void testFilterIntervall() throws Exception {
		List<FinanceData> listFinanceData = buildListOfFinanceData();
		Date fromDate = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		Date toDate = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-05");
		
		// 4 elements, no 0, 5 element
		List<FinanceData> expectedList = new ArrayList<FinanceData>();
		
		expectedList.add(listFinanceData.get(1));
		expectedList.add(listFinanceData.get(2));
		expectedList.add(listFinanceData.get(3));
		expectedList.add(listFinanceData.get(4));
		
		List<FinanceData> resultListFinanceData = 
				QueryResolverUtils.filterIntervall(listFinanceData, fromDate, toDate);
		
		assertEquals(4, resultListFinanceData.size());
		assertEquals(expectedList, resultListFinanceData);
		
	}
	
	/*
	 * 
	 * Es. dati da feed:
	 * 
	   2013-12-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-12-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-12-04,179.90,180.80,179.34,180.27,3483300,179.31
       
	   2013-11-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-11-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-11-04,179.90,180.80,179.34,180.27,3483300,179.31
     *
	 */
	 
	private List<FinanceData> buildListOfFinanceData() throws ParseException{
		List<FinanceData> listFinanceData = new ArrayList<FinanceData>();
		
		Date data_1 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04");
		Date data_2 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05");
		Date data_3 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06");
		
		listFinanceData.add(new FinanceData(data_1, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31));
		listFinanceData.add(new FinanceData(data_2, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90));
		listFinanceData.add(new FinanceData(data_3, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19));
		
		Date data_4 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-04");
		Date data_5 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-05");
		Date data_6 = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, "2013-12-06");
		
		listFinanceData.add(new FinanceData(data_4, 179.90, 180.80, 179.34, 180.27, 3483300, 179.31));
		listFinanceData.add(new FinanceData(data_5, 179.54, 179.80, 177.71, 177.85, 6096800, 176.90));
		listFinanceData.add(new FinanceData(data_6, 177.91, 179.75, 177.78, 179.19, 4560700, 179.19));
		
		return listFinanceData;
	}

}
