package com.simplethings.gestioneserietitoli.utils;

import static org.junit.Assert.assertEquals;

import static junitparams.JUnitParamsRunner.$;

import java.util.ArrayList;
import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.simplethings.gestioneserietitoli.utils.StringNumberUtils;

@RunWith(JUnitParamsRunner.class)
public class StringNumberUtilsTest {

	@Test
	public void testFormatDoubleValue() {
		double val_1 = 179.9;
		
		assertEquals("179.90", StringNumberUtils.formatDoubleValue(val_1));
	}

	@Test
	public void testUnserializeSeriesOfDay() {
		String valueSeriesOfDay = "01020304";
		List<Integer> listDays = StringNumberUtils.unserializeSeriesOfDay(valueSeriesOfDay);
		
		List<Integer> listExpectedValues = new ArrayList<Integer>();
		listExpectedValues.add(1);
		listExpectedValues.add(2);
		listExpectedValues.add(3);
		listExpectedValues.add(4);
		
		assertEquals(listExpectedValues, listDays);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	@Parameters( method = "getInvalidValues")
	public void testIllegalValueUnserializeSeriesOfDay(String valueSeriesOfDay) {
		List<Integer> listDays = StringNumberUtils.unserializeSeriesOfDay(valueSeriesOfDay);
					
	}
	
	private Object[] getInvalidValues(){
		return $($((Object)null), $(""), $("01023"), $("AB"));
	}
	
	@Test
	public void testUnserializeDoubleValue(){
		String valueListOfDoubleValue ="12.23_14.34_3.10";
		
		List<Double> values = 
				StringNumberUtils.unserializeSepareatedSeriesOfDouble(valueListOfDoubleValue, '_');
		
		List<Double> expectedValues = new ArrayList<Double>();
		expectedValues.add(12.23);
		expectedValues.add(14.34);
		expectedValues.add(3.1);
		
		assertEquals(expectedValues, values);
		
	}
	
	@Test
	public void testUnserializeLongValue(){
		String valueListOfLongValue ="123456_56545255_23098342";
		
		List<Long> values = StringNumberUtils.unserializeSepareatedSeriesOfLong(valueListOfLongValue, '_');
		
		List<Long> expectedValues = new ArrayList<Long>();
		expectedValues.add(123456L);
		expectedValues.add(56545255L);
		expectedValues.add(23098342L);
		
		assertEquals(expectedValues, values);
		
	}
	
}
