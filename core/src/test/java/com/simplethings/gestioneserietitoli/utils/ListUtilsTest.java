package com.simplethings.gestioneserietitoli.utils;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.simplethings.gestioneserietitoli.utils.ListUtils;

public class ListUtilsTest {

	@Test
	public void testReverseList() {
		List<String> list = new ArrayList<String>();
		list.add("UNO");
		list.add("DUE");
		list.add("TRE");
		
		
		List<String> expectedList = new ArrayList<String>();
		expectedList.add("TRE");
		expectedList.add("DUE");
		expectedList.add("UNO");
		
		assertEquals(expectedList, ListUtils.doReverse(list));
		assertEquals(new ArrayList<String>(), list);
	}
	
	

}
