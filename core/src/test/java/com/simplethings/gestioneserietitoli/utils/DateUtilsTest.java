package com.simplethings.gestioneserietitoli.utils;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class DateUtilsTest {
	
	@BeforeClass
	public static void init(){
		 BasicConfigurator.configure();
	}
	
	@Test
	@Parameters(method = "dateValues")
	public void doTestGeneric(String dateValue, int expectedDay, int expectedMonth, int expectedYear) throws ParseException{
		
		Calendar date = DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, dateValue);
		
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = date.get(Calendar.MONTH);
		int year = date.get(Calendar.YEAR);
		
		assertEquals(expectedDay, day);
		assertEquals(expectedMonth, month);
		assertEquals(expectedYear, year);
	}
	
	private Object[] dateValues(){
		Object[] values = $(
				$("01/02/2010", 1, Calendar.FEBRUARY, 2010),
				$("15/03/2000", 15, Calendar.MARCH, 2000),
				$("31/01/2010", 31, Calendar.JANUARY, 2010)
				);
		
		return values;
	}
	
	@Test
	@Parameters(method = "dateValues")
	public void testBuildDate(String dateValue, int dayOfMonth, int month, int year) throws ParseException{
		Date expectedDate = DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, dateValue);
		
		Date date = DateUtils.buildDate(dayOfMonth, month, year);
		
		assertEquals(expectedDate, date);
	}
	
	@Test
	public void testBuildDateCampioniOfMonth(){
				
		List<Integer> listOfDays = Arrays.asList(5, 8, 13, 27);
		List<Date> dateCampioni = DateUtils.buildDateCampioniOfMonth(Calendar.MARCH, 2010, listOfDays);
		
		Date date_1 = DateUtils.buildDate(5, Calendar.MARCH, 2010);
		Date date_2 = DateUtils.buildDate(8, Calendar.MARCH, 2010);
		Date date_3 = DateUtils.buildDate(13, Calendar.MARCH, 2010);
		Date date_4 = DateUtils.buildDate(27, Calendar.MARCH, 2010);
		
		List<Date> dateExpectedCampioni = new ArrayList<Date>();
		dateExpectedCampioni.add(date_1);
		dateExpectedCampioni.add(date_2);
		dateExpectedCampioni.add(date_3);
		dateExpectedCampioni.add(date_4);
		
		assertEquals(dateExpectedCampioni, dateCampioni);
		
	}
	
	@Test
	public void testStartMonthDay(){
		Date date_1 = DateUtils.buildDate(5, Calendar.MARCH, 2010);
		Date expectedDate = DateUtils.buildDate(1, Calendar.MARCH, 2010);
		
		assertEquals(expectedDate, DateUtils.getStartMonthDate(date_1));
		
	}
	
	@Test
	@Parameters(method = "dateValuesCompareDateInMonth")
	public void testCompareDateInMonth(Date dateA, Date dateB, boolean expectedResult){
		
		assertEquals(expectedResult, DateUtils.compareDateInMonth(dateA, dateB));
		
	}
	
	private Object[] dateValuesCompareDateInMonth() throws ParseException{
		Object[] values = $(
				$(DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, "01/02/2010"), 
						DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, "05/02/2010"), true),
				$(DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, "01/02/2013"), 
						DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, "01/02/2010"), false),
				$(DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, "01/02/2010"), 
						DateUtils.buildDate(DateUtils.SIMPLE_DATE_PATTERN, "01/03/2010"), false)
				);
		
		return values;
	}
	
	@Test
	@Parameters(method = "dateValuesRangeDate")
	public void testRangeDate(Calendar calValue, Calendar expectedResult_1, Calendar expectedResult_2){
		Calendar[] arrCal = DateUtils.getMonthRange(calValue);
		
		assertEquals(expectedResult_1, arrCal[0]);
		
		assertEquals(expectedResult_2, arrCal[1]);
	}
	
	private Object[] dateValuesRangeDate() throws ParseException{
		Object[] values = $(
				$(DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "11/09/2013"), 
						DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "01/09/2013"), 
							DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "30/09/2013")),
				$(DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "07/08/2010"), 
						DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "01/08/2010"), 
							DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "31/08/2010")),
				$(DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "08/02/2011"), 
						DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "01/02/2011"), 
							DateUtils.buildCalendarDate(DateUtils.SIMPLE_DATE_PATTERN, "28/02/2011"))
				);
		
		return values;
	}
	
}
