package com.simplethings.gestioneserietitoli.db;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.h2.jdbcx.JdbcDataSource;

import com.simplethings.gestioneserietitoli.conf.DbConfig;

public class DataSourceRegistrator {

	private static Hashtable<String, DataSource> ctx = 
			new Hashtable<String, DataSource>();
	
	/*
	 * 14 TIMES SLOWER THAN WITH DBCP Connectio Pool 
	 */
	
	public static void initOLD(DbConfig config, String urlDataSource) {
		JdbcDataSource ds = new JdbcDataSource();
		ds.setURL(config.dbUrl);
		ds.setUser(config.dbUsername);
		ds.setPassword(config.dbPassword);
		ctx.put(urlDataSource, ds);
	}
	
	public static void init(DbConfig config, String urlDataSource) {
		BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(config.dbDriverClassName);
        ds.setUsername(config.dbUsername);
        ds.setPassword(config.dbPassword);
        ds.setUrl(config.dbUrl);
        ctx.put(urlDataSource, ds);
	}
	
	public static DataSource lookup(String resourceName) throws NamingException {
		DataSource ds = ctx.get(resourceName);
		if(ds == null)
			throw new NamingException("Resource not found: " + resourceName);
		
		return ds;
	}
}
