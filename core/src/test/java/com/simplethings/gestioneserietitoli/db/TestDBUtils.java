package com.simplethings.gestioneserietitoli.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class TestDBUtils {
	
	private static Logger logger = LoggerFactory.getLogger(TestDBUtils.class);
	
	public static Connection createDirectConnection(String driverName, String dbUrl, String username, String password)
			throws ApplicationException {
			Connection conn = null;
			
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				throw new ApplicationException("Problem loading driver: " + driverName, e);
			}
	        try {
				conn = DriverManager.
				    getConnection(dbUrl, username, password);
			} catch (SQLException e) {
				throw new ApplicationException("Problem creating connection to: " + dbUrl, e);
			}
	        
	        return conn;
		}
	
	public static void cleanTABLE(ApplicationDataSourceContainer applicationDataSourceContainer, String tableName) throws SQLException{
		
		Connection conn = applicationDataSourceContainer.getDataSource().getConnection();
		
		PreparedStatement deleteTable = 
				conn.prepareStatement("DELETE FROM " + tableName);
		
		deleteTable.executeUpdate();
		
		conn.close();
	}
}
