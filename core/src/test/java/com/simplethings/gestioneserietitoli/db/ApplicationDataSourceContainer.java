package com.simplethings.gestioneserietitoli.db;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class ApplicationDataSourceContainer {
	
	private static Logger logger = LoggerFactory
			.getLogger(ApplicationDataSourceContainer.class);
	
	private DataSource dataSource = null;
	
	public ApplicationDataSourceContainer(DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	public ApplicationDataSourceContainer(String resourceBindingName) throws ApplicationException {
		
		try {
			dataSource = (DataSource) DataSourceRegistrator.lookup(resourceBindingName);
		} catch (NamingException e) {
			throw new ApplicationException("Failed to find: " + resourceBindingName, e);
		}
		 		 
	}
	
	public DataSource getDataSource(){
		return dataSource;
	}
	
}
