package com.simplethings.gestioneserietitoli.db;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.conf.DbConfig;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;

public class StocksInformationDBTest extends DataSourceBasedDBTestCase {

	public static final String TABLE_STOCKS_INFORMATION = "STOCKS_INFORMATION";

	private Config appConfig;
	private DbConfig dbConfig;

	private static IDataSet loadedDataSet = null;

	static {
		BasicConfigurator.configure();
	}

	public StocksInformationDBTest() throws ApplicationException {
		super();
		dbConfig = DbConfig.getConfig("/db_config.properties");
		appConfig = Config.getConfig("/config.properties");
	}

	public void setUp() throws MalformedURLException, DataSetException {
		System.out.println("*****************************");
		System.out.println("**** LANCIO SET_UP **********");
		System.out.println("*****************************");

		DataSourceRegistrator.init(dbConfig, appConfig.dataSourceUrl);
	}

	// Provide a connection to the database
	/*
	 * protected IDatabaseConnection getConnection() throws Exception{ appConfig
	 * = Config.getConfig("/config.properties"); dbConfig =
	 * DbConfig.getConfig("/db_config.properties");
	 * 
	 * DataSourceRegistrator.init(dbConfig, appConfig.dataSourceUrl);
	 * 
	 * ApplicationDataSourceContainer appDataSource = new
	 * ApplicationDataSourceContainerForTest(appConfig.dataSourceUrl);
	 * 
	 * Connection jdbcConnection = appDataSource.getConnection();
	 * 
	 * return new DatabaseConnection(jdbcConnection); }
	 */

	// Load the data which will be inserted for the test
	@Override
	protected IDataSet getDataSet() {

		if (loadedDataSet == null) {
			System.out.println("+++++++++++++++++++++++++++++");
			System.out.println("************* GET DATA SETTTTT");
			System.out.println("+++++++++++++++++++++++++++++");
			System.out.println("+++++++++++++++++++++++++++++");
			System.out.println("+++++++++++++++++++++++++++++");
			FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
			builder.setColumnSensing(true);

			try {
				loadedDataSet = builder.build(new File(
						"src/test/xml/stocks_information.xml"));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DataSetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return loadedDataSet;
	}

	public void testCheckLikeFunction() throws Exception {
		System.out.println("***** 1");

		ITable tab1 = getDataSet().getTable(TABLE_STOCKS_INFORMATION);
		int rowCount = tab1.getRowCount();
		assertEquals(8, rowCount);

		assertEquals("IBM.DE", (String) tab1.getValue(0, "STOCK_NAME"));
		assertEquals("FIAT", (String) tab1.getValue(7, "STOCK_NAME"));

		ITable tab2 = getDataSet().getTable("STOCK_SERIES");
		int rowCount2 = tab2.getRowCount();
		assertEquals(0, rowCount2);
		
		IDatabaseConnection dbConn = getConnection();
		DatabaseOperation.CLEAN_INSERT.execute(dbConn, getDataSet());

		StockInformationDAO stockMonthSeriesDAO = 
				new StockInformationDAO(getDataSource());

		// stockMonthSeriesDAO.getActiveStock("IBM");
		List<Stock> listStockMonthSeries = stockMonthSeriesDAO
				.searchByDescrOrStockCode("IBM");

		assertEquals(2, listStockMonthSeries.size());

		listStockMonthSeries = stockMonthSeriesDAO.searchByDescrOrStockCode("oca");

		System.out.println("***** 1A");
		assertEquals(5, listStockMonthSeries.size());
		System.out.println("***** 1B");

		assertTrue(listStockMonthSeries.contains(new Stock("KO",
				"Coca Cola inc.", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("HINFLUR.BO",
				"Hindustan Fluorocarbons Ltd", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("OVXA.DE",
				"Ovoca Gold PLC", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("MNH.V",
				"MENA Hydrocarbons Inc", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("CIEM.PA",
				"Marocaine (Compagnie)", true)));

		assertFalse(listStockMonthSeries
				.contains(new Stock("FIAT", "FIAT inc.", true)));

		listStockMonthSeries = stockMonthSeriesDAO.searchByDescrOrStockCode("OCA");

		assertEquals(5, listStockMonthSeries.size());

		assertTrue(listStockMonthSeries.contains(new Stock("KO",
				"Coca Cola inc.", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("HINFLUR.BO",
				"Hindustan Fluorocarbons Ltd", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("OVXA.DE",
				"Ovoca Gold PLC", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("MNH.V",
				"MENA Hydrocarbons Inc", true)));
		assertTrue(listStockMonthSeries.contains(new Stock("CIEM.PA",
				"Marocaine (Compagnie)", true)));
	}

	@Override
	protected DataSource getDataSource() {

		DataSource dataSource = null;
		try {

			dataSource = (DataSource) DataSourceRegistrator
					.lookup(appConfig.dataSourceUrl);

		} catch (NamingException e) {
			System.out.println("Failed to find: " + appConfig.dataSourceUrl);
		}

		return dataSource;
	}
	
}