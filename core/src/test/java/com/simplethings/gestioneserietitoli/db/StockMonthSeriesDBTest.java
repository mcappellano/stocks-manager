package com.simplethings.gestioneserietitoli.db;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.conf.DbConfig;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.model.StockMonthSeriesRow;
import com.simplethings.gestioneserietitoli.utils.DateUtils;

public class StockMonthSeriesDBTest extends DatabaseTestCase{
	
     public static final String TABLE_STOCK_SERIES = "STOCK_SERIES";
     private static IDataSet loadedDataSet;
     
     private Config appConfig;
     private DbConfig dbConfig;
     
     static {
    	 BasicConfigurator.configure();
     }
     
     // Provide a connection to the database
     protected IDatabaseConnection getConnection() throws Exception{
       appConfig = Config.getConfig("/config.properties");
       dbConfig = DbConfig.getConfig("/db_config.properties");
       
       DataSourceRegistrator.init(dbConfig, appConfig.dataSourceUrl);
       
       ApplicationDataSourceContainer appDataSource = 
    		   new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
       
       Connection jdbcConnection = appDataSource.getDataSource().getConnection();
       
       return new DatabaseConnection(jdbcConnection);
     }

     // Load the data which will be inserted for the test
     protected IDataSet getDataSet() throws Exception{
       
    	 if(loadedDataSet==null){
    		 FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
    		 builder.setColumnSensing(true);
    		 loadedDataSet = builder.build(new File("src/test/xml/stock_series.xml"));
    	 }
       return loadedDataSet;
     }

     // Check that the data has been loaded.
     public void testCheckStockSeriesDataLoaded() throws Exception{
       assertNotNull(getDataSet());
       int rowCount = getDataSet().getTable(TABLE_STOCK_SERIES).getRowCount();
       assertEquals(35, rowCount);
       
       ApplicationDataSourceContainer appDataSource = 
    		   new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
       
	   StockSeriesDAO stockMonthSeriesDAO = 
			   new StockSeriesDAO(appDataSource.getDataSource());
		
       List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.load("IBM");
       
       assertEquals(35, listStockMonthSeries.size());
       
       listStockMonthSeries = stockMonthSeriesDAO.load("GM");
       
       assertEquals(0, listStockMonthSeries.size());
              
     }
     
     public void AtestFromDate() throws ApplicationException, SQLException{
    	 Date date = DateUtils.buildDate(18, Calendar.SEPTEMBER, 2013);
    	 
    	 ApplicationDataSourceContainer appDataSource = 
    			new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
    	
 		StockSeriesDAO stockMonthSeriesDAO = 
 				new StockSeriesDAO(appDataSource.getDataSource());
 		
         List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.loadFromDate("IBM", date);
    	 assertEquals(3, listStockMonthSeries.size());
    	 
    	 List<StockMonthSeries> expectedListStockMonthSeries = new ArrayList<StockMonthSeries>();
    	 
    	 StockMonthSeries stockMonthSeries_1 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(18, Calendar.SEPTEMBER, 2013), 
    			 DateUtils.buildDate(30, Calendar.SEPTEMBER, 2013), 
    			 "181920232425262730", 
    			 "192.60_194.18_193.96_190.06_190.93_190.14_190.10_188.87_185.56", 
    			 "194.89_194.89_194.43_192.41_191.56_190.62_191.76_188.94_186.74", 
    			 "192.00_193.22_190.02_189.33_189.66_188.50_189.46_186.45_184.34", 
    			 "194.42_193.39_190.02_190.99_189.97_189.47_190.22_186.92_185.18", 
    			 "3841800_3178100_8525900_3340800_3085300_2654500_2093600_3905500_3978200", 
    			 "193.38_192.36_189.00_189.97_188.96_188.46_189.20_185.92_184.19")
    	 .convertToNumericMonthSeries();
    	 
    	 StockMonthSeries stockMonthSeries_2 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(1, Calendar.OCTOBER, 2013), 
    			 DateUtils.buildDate(31, Calendar.OCTOBER, 2013), 
    			 "0102030407080910111415161718212223242528293031", 
    			 "185.34_185.54_184.70_184.17_181.85_181.89_179.37_183.17_185.25_185.41_185.74_185.42_173.84_174.80_174.42_173.35_175.09_176.43_178.43_177.04_177.62_181.69_179.65", 
    			 "186.65_186.31_184.96_185.13_183.31_181.99_181.67_184.77_186.23_186.99_185.94_186.73_177.00_175.00_174.75_175.57_175.99_177.89_179.10_177.89_182.32_182.18_181.67", 
    			 "184.65_184.41_183.00_183.58_181.85_178.71_179.10_182.36_184.12_184.42_184.22_184.99_172.57_173.25_172.63_172.95_174.40_176.25_176.26_176.20_177.50_179.86_179.04", 
    			 "186.38_184.96_183.86_184.10_182.01_178.72_181.32_184.77_186.16_186.97_184.66_186.73_174.83_173.78_172.86_174.97_175.77_177.80_176.85_177.35_182.12_180.15_179.21", 
    			 "2681200_3617100_3211800_2863600_3966400_5578300_4423500_3658900_3232600_2663100_3365100_6718000_22368900_10548000_7098700_6977300_5409400_5533300_4842800_3712700_8904600_5273700_4318100", 
    			 "185.38_183.97_182.88_183.12_181.04_177.77_180.35_183.78_185.17_185.97_183.67_185.73_173.90_172.85_171.94_174.04_174.83_176.85_175.91_176.40_181.15_179.19_178.25")
    	 .convertToNumericMonthSeries();
    	 
    	 StockMonthSeries stockMonthSeries_3 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(1, Calendar.NOVEMBER, 2013), 
    			 DateUtils.buildDate(12, Calendar.NOVEMBER, 2013), 
    			 "0104050607081112", 
    			 "179.81_179.90_179.54_177.91_179.60_178.83_180.19_182.53", 
    			 "180.34_180.80_179.80_179.75_181.39_180.08_183.39_184.05", 
    			 "178.88_179.34_177.71_177.78_179.60_177.35_180.04_182.26", 
    			 "179.23_180.27_177.85_179.19_180.00_179.99_182.88_183.07", 
    			 "3644500_3483300_6096800_4560700_5219500_6275000_5222300_4258500", 
    			 "178.27_179.31_176.90_179.19_180.00_179.99_182.88_183.07")
    	 .convertToNumericMonthSeries();
     	
    	 expectedListStockMonthSeries.add(stockMonthSeries_1);
    	 expectedListStockMonthSeries.add(stockMonthSeries_2);
    	 expectedListStockMonthSeries.add(stockMonthSeries_3);
    	 
    	 assertEquals(expectedListStockMonthSeries, listStockMonthSeries);
    	 
     }
     
     
     public void testToDate() throws ApplicationException, SQLException{
    	 Date date = DateUtils.buildDate(18, Calendar.SEPTEMBER, 2013);
    	 
    	 ApplicationDataSourceContainer appDataSource = 
    			 new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
    	 
 		 StockSeriesDAO stockMonthSeriesDAO = 
 				 new StockSeriesDAO(appDataSource.getDataSource());
 		
         List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.loadToDate("IBM", date);
    	 assertEquals(33, listStockMonthSeries.size());
    	 
    	 StockMonthSeries lastStockMonthSeries = listStockMonthSeries.get(listStockMonthSeries.size()-1);
    	 
    	 StockMonthSeries expectedStockMonthRequest = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(3, Calendar.SEPTEMBER, 2013), 
    			 DateUtils.buildDate(18, Calendar.SEPTEMBER, 2013), 
    			 "030405060910111213161718", 
    			 "183.63_183.58_183.35_184.65_183.68_187.20_186.83_190.96_191.21_193.70_193.42_192.60", 
    			 "184.32_184.19_185.00_184.99_185.49_187.65_190.87_191.32_193.10_194.81_194.15_194.89", 
    			 "182.51_182.31_183.07_182.65_183.31_186.37_186.82_189.85_191.00_192.61_191.83_192.00", 
    			 "183.96_183.13_184.15_183.03_184.98_186.60_190.70_190.73_192.17_193.15_192.16_194.42", 
    			 "3487200_2597900_2867600_2903500_3017200_3149600_4962900_3354800_3710400_3902400_2930900_3841800", 
    			 "182.98_182.15_183.17_182.05_183.99_185.60_189.68_189.71_191.14_192.12_191.13_193.38")
    	 .convertToNumericMonthSeries();
    	 
    	 assertEquals(expectedStockMonthRequest, lastStockMonthSeries);
     }
     
     
     public void testRangeDate() throws ApplicationException, SQLException{
    	 Date dateFrom = DateUtils.buildDate(18, Calendar.JULY, 2013);
    	 Date dateTo	 = DateUtils.buildDate(18, Calendar.SEPTEMBER, 2013);
    	 
    	 ApplicationDataSourceContainer appDataSource = 
    			 new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
    	 
 		 StockSeriesDAO stockMonthSeriesDAO = 
 				 new StockSeriesDAO(appDataSource.getDataSource());
 		
         List<StockMonthSeries> listStockMonthSeries = 
        		 stockMonthSeriesDAO.loadIntervall("IBM", dateFrom, dateTo);
    	 
         assertEquals(3, listStockMonthSeries.size());
         
         List<StockMonthSeries> expectedListStockMonthSeries = new ArrayList<StockMonthSeries>();
         
         StockMonthSeries stockMonthSeries_1 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(18, Calendar.JULY, 2013), 
    			 DateUtils.buildDate(31, Calendar.JULY, 2013), 
    			 "18192223242526293031", 
    			 "198.27_197.91_193.40_194.21_195.95_196.30_196.59_196.83_196.99_194.49", 
    			 "200.94_197.99_195.79_196.43_197.30_197.83_197.37_197.19_197.83_196.91", 
    			 "195.99_193.24_193.28_194.10_195.86_195.66_195.00_195.53_195.81_194.49", 
    			 "197.99_193.54_194.09_194.98_196.61_197.22_197.35_196.21_196.01_195.04", 
    			 "8393400_6997600_3398000_2863800_2957900_3014300_2485100_2113700_2663200_3810000", 
    			 "195.95_191.55_192.09_192.97_194.59_195.19_195.32_194.19_193.99_193.03")
         .convertToNumericMonthSeries();
    	 
         
         StockMonthSeries stockMonthSeries_2 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(1, Calendar.AUGUST, 2013), 
    			 DateUtils.buildDate(30, Calendar.AUGUST, 2013), 
    			 "01020506070809121314151619202122232627282930", 
    			 "196.65_195.50_195.16_192.26_189.60_189.45_188.00_186.97_189.54_188.58_186.23_185.54_185.30_184.37_184.67_185.65_185.34_185.27_183.63_182.68_181.96_182.75", 
    			 "197.17_195.50_195.88_192.51_189.93_189.87_189.16_189.56_189.99_188.93_187.00_186.46_186.50_185.63_186.57_186.25_185.74_187.00_184.50_183.47_183.70_182.99", 
    			 "195.41_193.22_194.35_190.27_188.05_186.79_187.00_186.89_187.56_187.30_185.45_185.34_183.98_183.17_184.28_184.25_184.57_184.68_182.57_181.10_181.44_181.51", 
    			 "195.81_195.16_195.50_190.99_188.56_187.93_187.82_189.09_188.42_187.53_185.79_185.34_184.23_184.56_184.86_185.19_185.42_184.74_182.74_182.16_182.64_182.27", 
    			 "2877700_3874000_2490900_5938500_3901700_4547600_3249600_3026500_3542900_2892300_3692400_3426400_3248900_3117800_3551000_2354300_2292700_2170400_3190700_3979200_2980900_2731000", 
    			 "193.80_193.15_193.49_189.02_187.55_186.93_186.82_188.08_187.41_186.53_184.80_184.35_183.25_183.57_183.87_184.20_184.43_183.75_181.76_181.19_181.66_181.30")
         .convertToNumericMonthSeries();
         
    	 StockMonthSeries stockMonthSeries_3 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(3, Calendar.SEPTEMBER, 2013), 
    			 DateUtils.buildDate(18, Calendar.SEPTEMBER, 2013), 
    			 "030405060910111213161718", 
    			 "183.63_183.58_183.35_184.65_183.68_187.20_186.83_190.96_191.21_193.70_193.42_192.60", 
    			 "184.32_184.19_185.00_184.99_185.49_187.65_190.87_191.32_193.10_194.81_194.15_194.89", 
    			 "182.51_182.31_183.07_182.65_183.31_186.37_186.82_189.85_191.00_192.61_191.83_192.00", 
    			 "183.96_183.13_184.15_183.03_184.98_186.60_190.70_190.73_192.17_193.15_192.16_194.42", 
    			 "3487200_2597900_2867600_2903500_3017200_3149600_4962900_3354800_3710400_3902400_2930900_3841800", 
    			 "182.98_182.15_183.17_182.05_183.99_185.60_189.68_189.71_191.14_192.12_191.13_193.38")
    	 .convertToNumericMonthSeries();
    	 
    	 expectedListStockMonthSeries.add(stockMonthSeries_1);
    	 expectedListStockMonthSeries.add(stockMonthSeries_2);
    	 expectedListStockMonthSeries.add(stockMonthSeries_3);
    	 
    	 assertEquals(expectedListStockMonthSeries, listStockMonthSeries);
     }
     
     public void testRangeDateInside() throws ApplicationException, SQLException{
    	 Date dateFrom = DateUtils.buildDate(5, Calendar.NOVEMBER, 2013);
    	 Date dateTo	 = DateUtils.buildDate(18, Calendar.NOVEMBER, 2013);
    	 
    	 ApplicationDataSourceContainer appDataSource = 
    			 new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
    	 
 		 StockSeriesDAO stockMonthSeriesDAO = 
 				 new StockSeriesDAO(appDataSource.getDataSource());
 		
         List<StockMonthSeries> listStockMonthSeries = 
        		 stockMonthSeriesDAO.loadIntervall("IBM", dateFrom, dateTo);
    	 
         assertEquals(1, listStockMonthSeries.size());
         
         List<StockMonthSeries> expectedListStockMonthSeries = new ArrayList<StockMonthSeries>();
         
         StockMonthSeries stockMonthSeries_1 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(5, Calendar.NOVEMBER, 2013), 
    			 DateUtils.buildDate(12, Calendar.NOVEMBER, 2013), 
    			 "050607081112", 
    			 "179.54_177.91_179.60_178.83_180.19_182.53", 
    			 "179.80_179.75_181.39_180.08_183.39_184.05", 
    			 "177.71_177.78_179.60_177.35_180.04_182.26", 
    			 "177.85_179.19_180.00_179.99_182.88_183.07", 
    			 "6096800_4560700_5219500_6275000_5222300_4258500", 
    			 "176.90_179.19_180.00_179.99_182.88_183.07")
         .convertToNumericMonthSeries();
    	    	 
    	 expectedListStockMonthSeries.add(stockMonthSeries_1);
    	    	 
    	 assertEquals(expectedListStockMonthSeries, listStockMonthSeries);
     }
     
     public void testRangeDateInsideDue() throws ApplicationException, SQLException{
    	 Date dateFrom = DateUtils.buildDate(5, Calendar.NOVEMBER, 2013);
    	 Date dateTo	 = DateUtils.buildDate(8, Calendar.NOVEMBER, 2013);
    	 
    	 ApplicationDataSourceContainer appDataSource = 
    			 new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
    	 
 		 StockSeriesDAO stockMonthSeriesDAO = 
 				 new StockSeriesDAO(appDataSource.getDataSource());
 		
         List<StockMonthSeries> listStockMonthSeries = 
        		 stockMonthSeriesDAO.loadIntervall("IBM", dateFrom, dateTo);
    	 
         assertEquals(1, listStockMonthSeries.size());
         
         List<StockMonthSeries> expectedListStockMonthSeries = new ArrayList<StockMonthSeries>();
         
         StockMonthSeries stockMonthSeries_1 = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(5, Calendar.NOVEMBER, 2013), 
    			 DateUtils.buildDate(8, Calendar.NOVEMBER, 2013), 
    			 "05060708", 
    			 "179.54_177.91_179.60_178.83", 
    			 "179.80_179.75_181.39_180.08", 
    			 "177.71_177.78_179.60_177.35", 
    			 "177.85_179.19_180.00_179.99", 
    			 "6096800_4560700_5219500_6275000", 
    			 "176.90_179.19_180.00_179.99")
         .convertToNumericMonthSeries();
    	    	 
    	 expectedListStockMonthSeries.add(stockMonthSeries_1);
    	    	 
    	 assertEquals(expectedListStockMonthSeries, listStockMonthSeries);
     }
     
     
 	public void testLoadLastStockMonthSeries() throws Exception {
 		ApplicationDataSourceContainer appDataSource = 
 				 new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
 		 
		 StockSeriesDAO stockMonthSeriesDAO = 
				 new StockSeriesDAO(appDataSource.getDataSource());
		
    	 StockMonthSeries lastStockMonthSeriesRow = stockMonthSeriesDAO.loadLastStockMonthSeries("IBM");
    	 
    	 StockMonthSeries expectedLastStockMonthSeriesRow = new StockMonthSeriesRow("IBM", 
    			 DateUtils.buildDate(1, Calendar.NOVEMBER, 2013), 
    			 DateUtils.buildDate(12, Calendar.NOVEMBER, 2013), 
    			 "0104050607081112", 
    			 "179.81_179.90_179.54_177.91_179.60_178.83_180.19_182.53", 
    			 "180.34_180.80_179.80_179.75_181.39_180.08_183.39_184.05", 
    			 "178.88_179.34_177.71_177.78_179.60_177.35_180.04_182.26", 
    			 "179.23_180.27_177.85_179.19_180.00_179.99_182.88_183.07", 
    			 "3644500_3483300_6096800_4560700_5219500_6275000_5222300_4258500", 
    			 "178.27_179.31_176.90_179.19_180.00_179.99_182.88_183.07").convertToNumericMonthSeries();
    	 
    	 assertEquals(expectedLastStockMonthSeriesRow, lastStockMonthSeriesRow);
    	 
    	 
 	}
 	
 	public void testLoadStock() throws Exception {
 		ApplicationDataSourceContainer appDataSource = 
 				new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
 		
    	StockInformationDAO stockInformationDAO = 
    			new StockInformationDAO(appDataSource.getDataSource());
		
 		List<Stock> listOfStock = stockInformationDAO.getListOfActiveStock();
 		assertEquals(2, listOfStock.size());
 		
 		assertTrue(listOfStock.contains(new Stock("IBM", "IBM inc.", true)));
 		assertTrue(listOfStock.contains(new Stock("KO", "Coca Cola inc.", true)));
 		
 	}
 	
 	
}