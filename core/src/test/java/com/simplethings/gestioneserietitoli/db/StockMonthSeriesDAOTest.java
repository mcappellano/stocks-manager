package com.simplethings.gestioneserietitoli.db;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.conf.DbConfig;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.model.StockMonthSeriesRow;
import com.simplethings.gestioneserietitoli.utils.DateUtils;

public class StockMonthSeriesDAOTest {

	private static Config appConfig;
	private static DbConfig dbConfig;
	
	private static Logger logger = LoggerFactory.getLogger(StockMonthSeriesDAOTest.class);
	
	@BeforeClass
	public static void init() throws Exception {
		BasicConfigurator.configure();
		appConfig = Config.getConfig("/config.properties");
		dbConfig = DbConfig.getConfig("/db_config.properties");
		
		DataSourceRegistrator.init(dbConfig, appConfig.dataSourceUrl);
	}

	@Before
	public void cleanDb() throws ApplicationException, SQLException {
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
		
		TestDBUtils.cleanTABLE(appDataSource, "STOCK_SERIES");
		TestDBUtils.cleanTABLE(appDataSource, "STOCKS_INFORMATION");
	}

	@Test
	public void testInsertStock() throws SQLException, ApplicationException {
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
		
		StockInformationDAO stockInformationDAO = 
				new StockInformationDAO(appDataSource.getDataSource());
		
		Stock stockRow = new Stock("IBM", "IBM inc.");
		stockInformationDAO.createStock(stockRow);
		stockInformationDAO.activateStock(stockRow.getStockName());

		Stock resultStockRow = stockInformationDAO.getActiveStock("IBM");

		Stock stockRowExpected = new Stock("IBM", "IBM inc.", true);
		assertEquals(stockRowExpected, resultStockRow);

	}

	/*
	 * Fail because not stock in STOCKS table.
	 */

	@Test(expected = SQLException.class)
	public void testFailSimpleInsert() throws SQLException, ParseException,
			ApplicationException {

		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
		
		StockSeriesDAO stockMonthSeriesDAO = new StockSeriesDAO(
				appDataSource.getDataSource());

		Date startDate = DateUtils.buildCalendarDate(
				DateUtils.FEED_DATE_PATTERN, "2013-11-04").getTime();
		Date endDate = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN,
				"2013-11-05").getTime();

		StockMonthSeriesRow numericMonthSeriesRow = new StockMonthSeriesRow(
				"IBM", startDate, endDate, "0405", "11.60_12.50",
				"13.70_14.20", "10.90_12.08", "12.50_13.90", "200000_300000",
				"12.40_13.80");

		StockMonthSeries stockMonthSeries = mock(StockMonthSeries.class);

		when(stockMonthSeries.convertToStrockMonthSeriesRow()).thenReturn(
				numericMonthSeriesRow);

		stockMonthSeriesDAO.save(stockMonthSeries);

	}

	@Test
	public void testSimpleInsert() throws SQLException, ParseException,
			ApplicationException {

		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
		
		StockSeriesDAO stockMonthSeriesDAO = 
				new StockSeriesDAO(appDataSource.getDataSource());
		
		StockInformationDAO stockInformationDAO = 
				new StockInformationDAO(appDataSource.getDataSource());

		Stock stockRow = new Stock("IBM", "IBM inc.");
		stockInformationDAO.createStock(stockRow);

		Date startDate = DateUtils.buildCalendarDate(
				DateUtils.FEED_DATE_PATTERN, "2013-11-04").getTime();
		Date endDate = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN,
				"2013-11-05").getTime();

		StockMonthSeriesRow numericMonthSeriesRow = new StockMonthSeriesRow(
				"IBM", startDate, endDate, "0405", "11.60_12.50",
				"13.70_14.20", "10.90_12.08", "12.50_13.90", "200000_300000",
				"12.40_13.80");

		StockMonthSeries stockMonthSeries = mock(StockMonthSeries.class);

		when(stockMonthSeries.convertToStrockMonthSeriesRow()).thenReturn(
				numericMonthSeriesRow);

		stockMonthSeriesDAO.save(stockMonthSeries);

		List<StockMonthSeries> listMonthSeriesRows = stockMonthSeriesDAO
				.loadFromDate("IBM", startDate);

		assertEquals(1, listMonthSeriesRows.size());
		StockMonthSeries stockMonthSeriesReturn = listMonthSeriesRows.get(0);

		assertEquals(numericMonthSeriesRow,
				stockMonthSeriesReturn.convertToStrockMonthSeriesRow());

	}

	@Test(expected = SQLException.class)
	public void testMultipleInsert() throws SQLException, ParseException,
			ApplicationException {

		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(appConfig.dataSourceUrl);
		
		StockInformationDAO stockInformationDAO = 
				new StockInformationDAO(appDataSource.getDataSource());

		StockSeriesDAO stockMonthSeriesDAO = 
				new StockSeriesDAO(appDataSource.getDataSource());

		Stock stockRow = new Stock("IBM", "IBM inc.");
		stockInformationDAO.createStock(stockRow);
		stockInformationDAO.activateStock(stockRow.getStockName());
		
		Date startDate = DateUtils.buildCalendarDate(
				DateUtils.FEED_DATE_PATTERN, "2013-11-04").getTime();
		Date endDate = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN,
				"2013-11-05").getTime();

		StockMonthSeriesRow numericMonthSeriesRow = new StockMonthSeriesRow(
				"IBM", startDate, endDate, "0405", "11.60_12.50",
				"13.70_14.20", "10.90_12.08", "12.50_13.90", "200000_300000",
				"12.40_13.80");

		StockMonthSeries stockMonthSeries = mock(StockMonthSeries.class);

		when(stockMonthSeries.convertToStrockMonthSeriesRow()).thenReturn(
				numericMonthSeriesRow);

		stockMonthSeriesDAO.save(stockMonthSeries);
		stockMonthSeriesDAO.save(stockMonthSeries);

	}
	
}
