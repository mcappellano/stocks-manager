package com.simplethings.gestioneserietitoli.logic;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.model.StockMonthSeriesRow;
import com.simplethings.gestioneserietitoli.utils.DateUtils;


public class FeedLogicTest {
	
	private static Logger logger = LoggerFactory.getLogger(FeedLogicTest.class);
	
	@BeforeClass
	public static void init(){
		 BasicConfigurator.configure();
	}
	
	@Test
	public void testDoLoadFromDB() throws ParseException, SQLException {
		StockSeriesDAO numMonthSeriesDAO = mock(StockSeriesDAO.class);
		
		when(numMonthSeriesDAO.load("IBM")).thenReturn(buildValue());
		
		List<StockMonthSeries> listNumericMonthSeries = numMonthSeriesDAO.load("IBM");
		assertEquals(2, numMonthSeriesDAO.load("IBM").size());
		
		StockMonthSeries numericMonthSeries_1 = listNumericMonthSeries.get(0);
				
		StockMonthSeries numericMonthSeries_2 = listNumericMonthSeries.get(1);
				

		List<FinanceData> monthFinanceData_1 = 
				new ArrayList<FinanceData>(numericMonthSeries_1.getFinanceDataSet());
		
		List<FinanceData> monthFinanceData_2 = 
				new ArrayList<FinanceData>(numericMonthSeries_2.getFinanceDataSet());
		
		assertEquals(3, monthFinanceData_1.size());
		assertEquals(4, monthFinanceData_2.size());	
		
		Date data_1 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04").getTime();
		Date data_2 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-05").getTime();
		Date data_3 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06").getTime();
		
		Date data_4 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-04").getTime();
		Date data_5 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-05").getTime();
		Date data_6 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-06").getTime();
		Date data_7 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-07").getTime();
		
		List<FinanceData> expectedListFinanceDataMonth_1 = new ArrayList<FinanceData>();
		expectedListFinanceDataMonth_1.add(
				new FinanceData(data_1,179.90,180.80,179.34,180.27,3483300,179.31));
		expectedListFinanceDataMonth_1.add(
				new FinanceData(data_2,179.54,179.80,177.71,177.85,6096800,176.90));
		expectedListFinanceDataMonth_1.add(
				new FinanceData(data_3,177.91,179.75,177.78,179.19,4560700,179.19));
		
		List<FinanceData> expectedListFinanceDataMonth_2 = new ArrayList<FinanceData>();
		expectedListFinanceDataMonth_2.add(
				new FinanceData(data_4,179.90,180.80,179.34,180.27,3483300,179.31));
		expectedListFinanceDataMonth_2.add(
				new FinanceData(data_5,179.54,179.80,177.71,177.85,6096800,176.90));
		expectedListFinanceDataMonth_2.add(
				new FinanceData(data_6,177.91,179.75,177.78,179.19,4560700,179.19));
		expectedListFinanceDataMonth_2.add(
				new FinanceData(data_7,178.91,177.75,176.78,178.19,4560600,178.19));
		
		assertEquals(expectedListFinanceDataMonth_1, monthFinanceData_1);
		assertEquals(expectedListFinanceDataMonth_2, monthFinanceData_2);
		
	}
	
	/*
	 * 
	 * Es. dati da feed:
	 * 
	   2013-12-07,178.91,177.75,176.78,178.19,4560600,178.19
	   2013-12-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-12-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-12-04,179.90,180.80,179.34,180.27,3483300,179.31
       
	   2013-11-06,177.91,179.75,177.78,179.19,4560700,179.19
	   2013-11-05,179.54,179.80,177.71,177.85,6096800,176.90
       2013-11-04,179.90,180.80,179.34,180.27,3483300,179.31
     *
	 */
	
	public List<StockMonthSeries> buildValue() throws ParseException {
		Date dataStart_1 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-04").getTime();
		Date dataEnd_1 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-06").getTime();
		
		Date dataStart_2 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-04").getTime();
		Date dataEnd_2 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-07").getTime();
		
		List<StockMonthSeries> listNumericMonthSeriesRow = new ArrayList<StockMonthSeries>();
		
		StockMonthSeriesRow row1 = new StockMonthSeriesRow("IBM", dataStart_1, dataEnd_1,
				"040506",
				"179.90_179.54_177.91", 
				"180.80_179.80_179.75", 
				"179.34_177.71_177.78", 
				"180.27_177.85_179.19", 
				"3483300_6096800_4560700", 
				"179.31_176.90_179.19");
		
		StockMonthSeriesRow row2 = new StockMonthSeriesRow("IBM", dataStart_2, dataEnd_2,
				"04050607",
				"179.90_179.54_177.91_178.91", 
				"180.80_179.80_179.75_177.75", 
				"179.34_177.71_177.78_176.78", 
				"180.27_177.85_179.19_178.19", 
				"3483300_6096800_4560700_4560600", 
				"179.31_176.90_179.19_178.19");
		
		listNumericMonthSeriesRow.add(row1.convertToNumericMonthSeries());
		listNumericMonthSeriesRow.add(row2.convertToNumericMonthSeries());
		
		return listNumericMonthSeriesRow;
	}
	
	
}
