package com.simplethings.gestioneserietitoli.logic;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.conf.DbConfig;
import com.simplethings.gestioneserietitoli.db.ApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.db.DataSourceRegistrator;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.TestDBUtils;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;

public class ApplicationTest {

	private static Config config;
	private static DbConfig dbConfig;
		
	@BeforeClass
	public static void init() throws Exception {
		BasicConfigurator.configure();
		config = Config.getConfig("/config.properties");
		dbConfig = DbConfig.getConfig("/db_config.properties");
		DataSourceRegistrator.init(dbConfig, config.dataSourceUrl);
	}

	@Before
	public void cleanDb() throws ApplicationException, SQLException {
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(config.dataSourceUrl);
		
		TestDBUtils.cleanTABLE(appDataSource, "STOCK_SERIES");
		TestDBUtils.cleanTABLE(appDataSource, "STOCKS_INFORMATION");
	}
	
	@Test
	public void testFeedLoadNew() throws ApplicationException, SQLException {
		
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(config.dataSourceUrl);
		
		StockSeriesDAO stockMonthSeriesDAO = 
				new StockSeriesDAO(appDataSource.getDataSource());
		
		StockInformationDAO stockInformationDAO = 
				new StockInformationDAO(appDataSource.getDataSource());
		
		Stock stockRow_1 = new Stock("IBM", "IBM inc.");
		stockInformationDAO.createStock(stockRow_1);
		stockInformationDAO.activateStock(stockRow_1.getStockName());
		
//		Stock stockRow_2 = new Stock("KO", "Coca Cola inc.");
//		stockInformationDAO.createStock(stockRow_2);
//		stockInformationDAO.activateStock(stockRow_2);
				
		long start = System.currentTimeMillis();
		
		FinanceDataWsClient wsClient = new FinanceDataWsClient(new ResponseHandlerFinancialData(stockMonthSeriesDAO));
		
		FinanceDataSincronizerController dataSincronizer = new FinanceDataSincronizerController(stockMonthSeriesDAO, wsClient);
		
		dataSincronizer.doWork(stockInformationDAO.getListOfActiveStock());
		
		long endTime = System.currentTimeMillis();
			
		System.out.println("**** SECONDS:" + (endTime - start)/1000.0);
	}
	
	@Test
	public void testTimerSetting() throws ApplicationException, SQLException, ParseException{
		
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(config.dataSourceUrl);
		
		StockSeriesDAO stockMonthSeriesDAO = 
				new StockSeriesDAO(appDataSource.getDataSource());
		
		StockInformationDAO stockInformationDAO = 
				new StockInformationDAO(appDataSource.getDataSource());
		
		Stock stockRow_1 = new Stock("IBM", "IBM inc.");
		stockInformationDAO.createStock(stockRow_1);
		stockInformationDAO.activateStock(stockRow_1.getStockName());
		
		Stock stockRow_2 = new Stock("KO", "Coca Cola inc.");
		stockInformationDAO.createStock(stockRow_2);
		stockInformationDAO.activateStock(stockRow_2.getStockName());
		
		FinanceDataWsClient wsClient = new FinanceDataWsClient(new ResponseHandlerFinancialData(stockMonthSeriesDAO));
		
		Timer timer = mock(Timer.class);
		
		FinanceDataSincronizerController dataSincronizer = 
				new DataSincronizerSettingTimer(stockMonthSeriesDAO, wsClient, timer);
		
		List<Stock> listActiveStocks = stockInformationDAO.getListOfActiveStock();
		when(timer.getTime()).thenReturn(buildTestCalendarCase(0));
		assertEquals(buildTestCalendarCase(0), timer.getTime());
		dataSincronizer.doWork(listActiveStocks);	
		
		when(timer.getTime()).thenReturn(buildTestCalendarCase(1));
		assertEquals(buildTestCalendarCase(1), timer.getTime());
		dataSincronizer.doWork(listActiveStocks);
		
		when(timer.getTime()).thenReturn(buildTestCalendarCase(2));
		assertEquals(buildTestCalendarCase(2), timer.getTime());
		dataSincronizer.doWork(listActiveStocks);
		
		
	}
	
	private Calendar buildTestCalendarCase(int index) throws ParseException {
		
		Calendar cal1 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-13");
		Calendar cal2 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-11-17");
		Calendar cal3 = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, "2013-12-06");
		Calendar[] arrCal = {cal1, cal2, cal3};
		return arrCal[index];
	}

}
