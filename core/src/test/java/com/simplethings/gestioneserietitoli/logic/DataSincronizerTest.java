package com.simplethings.gestioneserietitoli.logic;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.model.StockMonthSeriesRow;
import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest.TradingPeriod;

public class DataSincronizerTest {

	@Test
	public void testRangeLoadRequest() throws SQLException, ParseException {
		StockSeriesDAO stockMonthSeriesDAO = mock(StockSeriesDAO.class);
		FinanceDataWsClient wsClient = null;
		FinanceDataSincronizerController dataSincronizer = new FinanceDataSincronizerController(stockMonthSeriesDAO, wsClient);
		
		String nowValueDate = "2013-11-30";
		
		Calendar calNow = buildDate(nowValueDate);
		
		when(stockMonthSeriesDAO.loadLastStockMonthSeries("IBM"))
			.thenReturn(getData("IBM", "2013-11-01", "2013-11-12", 
					"0104050607081112", "179.81_179.90_179.54_177.91_179.60_178.83_180.19_182.53",
					"180.34_180.80_179.80_179.75_181.39_180.08_183.39_184.05",
					"178.88_179.34_177.71_177.78_179.60_177.35_180.04_182.26",
					"179.23_180.27_177.85_179.19_180.00_179.99_182.88_183.07",
					"3644500_3483300_6096800_4560700_5219500_6275000_5222300_4258500",
					"178.27_179.31_176.90_179.19_180.00_179.99_182.88_183.07"));
		
		FinanceDataRequest gestioneTitoliRequest = 
				dataSincronizer.doGestioneTitoliRequest("IBM", calNow);
		
		Calendar calFromDate = buildDate("2013-11-13");
		FinanceDataRequest expectedGestioneTitoliRequest = 
				new FinanceDataRequest("IBM", calFromDate, calNow, TradingPeriod.DAILY);
		
		assertEquals(expectedGestioneTitoliRequest, gestioneTitoliRequest);
	}

	@Test
	public void testNewLoadRequest() throws SQLException, ParseException {
		StockSeriesDAO stockMonthSeriesDAO = mock(StockSeriesDAO.class);
		FinanceDataWsClient wsClient = null;
		FinanceDataSincronizerController dataSincronizer = new FinanceDataSincronizerController(stockMonthSeriesDAO, wsClient);
		
		String nowValueDate = "2013-11-30";
		
		Calendar calNow = buildDate(nowValueDate);
		
		when(stockMonthSeriesDAO.loadLastStockMonthSeries("GOOGLE"))
			.thenReturn(null);
		
		FinanceDataRequest gestioneTitoliRequest = 
				dataSincronizer.doGestioneTitoliRequest("GOOGLE", calNow);
		
		Calendar calFromDate = null;
		FinanceDataRequest expectedGestioneTitoliRequest = 
				new FinanceDataRequest("GOOGLE", calFromDate, calNow, TradingPeriod.DAILY);
		
		assertEquals(expectedGestioneTitoliRequest, gestioneTitoliRequest);
	}
    
	private Calendar buildDate(String strDate) throws ParseException{
		Calendar calDate = DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, strDate);
		return calDate;
	}
	
	private StockMonthSeries getData(String stockName, String strStartDate,
			String strEndDate, String daySeries, String openValueSeries, 
			String heightValueSeries, String lowValueSeries, String closeValueSeries,
			String volumeValueSeries, String adjCloseValueSeries) throws ParseException{
		
		Date startDate = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, strStartDate);
		Date endDate = DateUtils.buildDate(DateUtils.FEED_DATE_PATTERN, strEndDate);;
		return new StockMonthSeriesRow(stockName, startDate, endDate, daySeries, openValueSeries, 
				heightValueSeries, lowValueSeries, closeValueSeries, volumeValueSeries, adjCloseValueSeries)
		.convertToNumericMonthSeries();
	}
	
}
