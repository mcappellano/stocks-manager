package com.simplethings.gestioneserietitoli.logic;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.conf.DbConfig;
import com.simplethings.gestioneserietitoli.db.ApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.db.DataSourceRegistrator;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.TestDBUtils;
import com.simplethings.gestioneserietitoli.ws.stocksinformation.StocksInformationWsClient;

public class StocksInformationSincronizerControllerTest {

	private static Config config;
	private static DbConfig dbConfig;
		
	@BeforeClass
	public static void init() throws Exception {
		BasicConfigurator.configure();
		config = Config.getConfig("/config.properties");
		dbConfig = DbConfig.getConfig("/db_config.properties");
		DataSourceRegistrator.init(dbConfig, config.dataSourceUrl);
	}

	@Before
	public void cleanDb() throws ApplicationException, SQLException {
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(config.dataSourceUrl);
		
		TestDBUtils.cleanTABLE(appDataSource, "STOCK_SERIES");
		TestDBUtils.cleanTABLE(appDataSource, "STOCKS_INFORMATION");
	}
	
	@Test
	public void testDoWork() throws ApplicationException {
		
		ApplicationDataSourceContainer appDataSource = 
				new ApplicationDataSourceContainer(config.dataSourceUrl);
		
		StockInformationDAO stockInformationDAO = 
				new StockInformationDAO(appDataSource.getDataSource());
		
		ResponseHandlerStocksInformation responseHandlerStocksInformation = 
				new ResponseHandlerStocksInformation(stockInformationDAO);
		
		StocksInformationWsClient stocksInformationWsClient = new StocksInformationWsClient(responseHandlerStocksInformation);
		
		StocksInformationSincronizerController workerController = 
				new StocksInformationSincronizerController(stocksInformationWsClient);
		
		workerController.doWork();
		System.out.println("*********** FINISHED!!!!!");
	}

}
