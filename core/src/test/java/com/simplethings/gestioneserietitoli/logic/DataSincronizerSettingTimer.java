package com.simplethings.gestioneserietitoli.logic;

import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;

public class DataSincronizerSettingTimer extends FinanceDataSincronizerController {

	public DataSincronizerSettingTimer(StockSeriesDAO stockMonthSeriesDAO,
			FinanceDataWsClient wsClient, Timer timer) {
		super(stockMonthSeriesDAO, wsClient);
		super.timer = timer;
	}

}
