package com.simplethings.gestioneserietitoli.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.utils.DateUtils;

import static com.simplethings.gestioneserietitoli.parser.FeedParser.doParseLine;

public class FinanceDataFeedParserTest {

	private static Logger logger = LoggerFactory.getLogger(FinanceDataFeedParserTest.class);
	
	@BeforeClass
	public static void init(){
		 BasicConfigurator.configure();
	}
	
	@Test
	public void testLine() throws ParseException {
		String line = "2010-01-25,546.59,549.88,525.61,529.94,4021800,529.94";
		
		FinanceData financeData = doParseLine(line);
		
		Date dataCampioneExpected = DateUtils.buildCalendarDate("yyyy-MM-dd", "2010-01-25").getTime();
		
		assertEquals(dataCampioneExpected, financeData.getDate());
		assertTrue(546.59 == financeData.getOpenValue());
		assertTrue(549.88 == financeData.getHightValue());
		assertTrue(525.61 == financeData.getLowValue());
		assertTrue(529.94 == financeData.getCloseValue());
		assertEquals(4021800, financeData.getVolume());
		assertTrue(529.94 == financeData.getAdjCloseValue());
		
	}
	
}
