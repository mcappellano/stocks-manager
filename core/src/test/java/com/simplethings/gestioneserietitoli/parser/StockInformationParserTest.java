package com.simplethings.gestioneserietitoli.parser;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.codehaus.jackson.JsonParseException;
import org.junit.Test;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.parser.StockInformationParser.CompanyInformation;
import com.simplethings.gestioneserietitoli.parser.StockInformationParser.IndustryInformation;

public class StockInformationParserTest {
	
	private StockInformationParser stockInformationParser = new StockInformationParser();

	@Test
	public void testDoWorkNew() throws JsonParseException, IOException {
		FileInputStream input = new FileInputStream("./src/test/input/json.results_PROPER_FORMATTED.js");
		BlockingQueue<Optional<CompanyInformation>> queue =  
				new LinkedBlockingQueue<Optional<CompanyInformation>>();
		stockInformationParser.doWork(input, queue);
		doDump(queue);
	}
	
	private void doDump(BlockingQueue<Optional<CompanyInformation>> queue) throws IOException{
		PrintWriter out
		   = new PrintWriter(new BufferedWriter(new FileWriter("./src/test/input/foo.out")));
		
		try {
			Optional<CompanyInformation> optCompanyInformation = null;
			IndustryInformation industryInformationCurr = null;
			
			while((optCompanyInformation = queue.take())!=null){
				if(!optCompanyInformation.isPresent())
					break;
				
				CompanyInformation companyInformation = optCompanyInformation.get();
				IndustryInformation industryInformation = companyInformation.idIndustryInformation;
				
				if(!industryInformation.equals(industryInformationCurr)){
					industryInformationCurr = industryInformation;
					// print
					out.println("**************************************");
					out.println(industryInformationCurr.toString());
				}
				
				// print companyInformation
				out.println(">>>>> " + companyInformation.toString());
				
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		out.close();
		
	}

}
