package com.simplethings.gestioneserietitoli.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.utils.StringNumberUtils;

/**
 * Group series for month of the year.
 * 
 * Es: January 2012 or Decamber 1999
 * 
 * @author CAM
 *
 */
public class StockMonthSeries {
	
	public final String stockName;
	
	private Set<FinanceData> setOfFinanceData = new TreeSet<FinanceData>();
	
	// for checking FinanceData compatibility
	private int month=-1;
	private int year = -1;
	
	public StockMonthSeries(String stockName){
		this.stockName = stockName;
	}
	
	/*
	 * Add FinanceData 
	 * 
	 * @throws IllegalArgumentException if FinanceData is non compatible
	 * 
	 */
	public synchronized void  addFinanceData(FinanceData financeData){
		if(this.year!=-1){
			Calendar cal= Calendar.getInstance();
			cal.setTime(financeData.getDate());
			if(this.month != cal.get(Calendar.MONTH) || this.year != cal.get(Calendar.YEAR))
				throw new IllegalArgumentException("Invalid add");
			
			
		} else {
			Calendar cal= Calendar.getInstance();
			cal.setTime(financeData.getDate());
			this.month = cal.get(Calendar.MONTH);
			this.year = cal.get(Calendar.YEAR);
		}
		
		setOfFinanceData.add(financeData);
	}

	public synchronized Set<FinanceData> getFinanceDataSet(){
		return setOfFinanceData;
	}
	
	public synchronized StockMonthSeriesRow convertToStrockMonthSeriesRow(){
		StockMonthSeriesRow stockMonthSeriesRow = null;
		
		Date startDate = null;
		Date endDate = null;
		
		List<Integer> seriesDay = new ArrayList<Integer>();
		
		List<Double> openValueSeries = new ArrayList<Double>();
		List<Double> hightValueSeries = new ArrayList<Double>();
		List<Double> lowValueSeries = new ArrayList<Double>();
		List<Double> closeValueSeries = new ArrayList<Double>();
		
		List<Long> volumeSeries = new ArrayList<Long>(); 
		
		List<Double> adjCloseValueSeries = new ArrayList<Double>(); 
		
		Iterator<FinanceData> iterator = setOfFinanceData.iterator();
		
		while(iterator.hasNext()){
			FinanceData financeData = iterator.next();
			
			if(startDate==null){
				startDate = financeData.getDate();
			}
			
			endDate = financeData.getDate();
			
			Date dateCampione = financeData.getDate();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateCampione);
			
			int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
			
			seriesDay.add(dayOfMonth);
			
			openValueSeries.add(financeData.getOpenValue());
			hightValueSeries.add(financeData.getHightValue());
			lowValueSeries.add(financeData.getLowValue());
			closeValueSeries.add(financeData.getCloseValue());
			volumeSeries.add(financeData.getVolume());
			adjCloseValueSeries.add(financeData.getAdjCloseValue());
		}
		
		if(setOfFinanceData.size() > 0){
			String valueSeriesDay = 
					StringNumberUtils.buildSeriesOfDay(seriesDay);
			String valueOpenValueSeries = 
					StringNumberUtils.buildSeparatedSeriesOfDouble(openValueSeries, '_');
			String valueHightValueSeries =
					StringNumberUtils.buildSeparatedSeriesOfDouble(hightValueSeries, '_');
			String valueLowValueSeries =
					StringNumberUtils.buildSeparatedSeriesOfDouble(lowValueSeries, '_');
			String valueCloseValueSeries =
					StringNumberUtils.buildSeparatedSeriesOfDouble(closeValueSeries, '_');
			String valueVolumeSeries =
					StringNumberUtils.buildSeparatedSeriesOfLong(volumeSeries, '_');
			String valueAdjCloseValueSeries =
					StringNumberUtils.buildSeparatedSeriesOfDouble(adjCloseValueSeries, '_');
			
			stockMonthSeriesRow = new StockMonthSeriesRow(stockName, startDate, endDate, 
					valueSeriesDay, valueOpenValueSeries, valueHightValueSeries, valueLowValueSeries, 
					valueCloseValueSeries, valueVolumeSeries, valueAdjCloseValueSeries);
			
		}
		
		
		return stockMonthSeriesRow;
		
	}
	
	public boolean isMergiable(StockMonthSeries otherNumericMonthSeries){
		
		if(otherNumericMonthSeries == null){
			return true;
		}
		
		if(!this.stockName.equals(otherNumericMonthSeries.stockName))
			return false;
		
		if(setOfFinanceData.size() > 0){
			Set<FinanceData> financeDatas = otherNumericMonthSeries.getFinanceDataSet();
			
			if(financeDatas.size() > 0){
				Iterator<FinanceData> thisFinanceDataIterator = 
						setOfFinanceData.iterator();
				
				Iterator<FinanceData> otherFinanceDataIterator = 
						financeDatas.iterator();
				
				Date thisDate = thisFinanceDataIterator.next().getDate();
				Date otherDate = otherFinanceDataIterator.next().getDate();
				return DateUtils.compareDateInMonth(thisDate, otherDate);
			} else {
				return true;
			}
		}
		
		if(setOfFinanceData.size() == 0){
			return true;
		}
		
		return false;
	}
	
	/*
	 * ADD operation.
	 */
	
	public StockMonthSeries merge(StockMonthSeries otherNumericMonthSeries){
		if(otherNumericMonthSeries==null)
			return this;
		if(this.isMergiable(otherNumericMonthSeries)){
			StockMonthSeries newStockMonthSeries = new StockMonthSeries(this.stockName);
			Set<FinanceData> firstSet = this.getFinanceDataSet();
			Set<FinanceData> otherSet =  otherNumericMonthSeries.getFinanceDataSet();
			
			Set<FinanceData> summDataSet = new TreeSet<FinanceData>();
			summDataSet.addAll(firstSet);
			summDataSet.addAll(otherSet);
			Iterator<FinanceData> iterator = summDataSet.iterator();
			
			while(iterator.hasNext()){
				FinanceData elem = iterator.next();
				newStockMonthSeries.addFinanceData(elem);
			}
			
			return newStockMonthSeries;
		}
		
		return null;
	}
	
	/*
	 * Builder from ordered list asc by date and grouped by ticket. 
	 * 
	 */
	
	public static List<StockMonthSeries> serializeNumericMonthSeries(String ticket, List<FinanceData> listOfFinanceData){
		List<StockMonthSeries> listNumericMonthSeries = new ArrayList<StockMonthSeries>();
		
		Calendar cal = Calendar.getInstance();
		
		int currentMonth = -1;
		int currentYear = -1;
		StockMonthSeries curentNumericMonthSeries = null;
		
		for(FinanceData item: listOfFinanceData){
			cal.setTime(item.getDate());
			
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			
			if( year != currentYear || month != currentMonth ){
				
				currentYear = year;
				currentMonth = month;
				curentNumericMonthSeries = new StockMonthSeries(ticket);
				listNumericMonthSeries.add(curentNumericMonthSeries);
			}
			
			curentNumericMonthSeries.addFinanceData(item);
			
		}
		
		return listNumericMonthSeries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + month;
		result = prime
				* result
				+ ((setOfFinanceData == null) ? 0 : setOfFinanceData.hashCode());
		result = prime * result
				+ ((stockName == null) ? 0 : stockName.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockMonthSeries other = (StockMonthSeries) obj;
		if (month != other.month)
			return false;
		if (setOfFinanceData == null) {
			if (other.setOfFinanceData != null)
				return false;
		} else if (!setOfFinanceData.equals(other.setOfFinanceData))
			return false;
		if (stockName == null) {
			if (other.stockName != null)
				return false;
		} else if (!stockName.equals(other.stockName))
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StockMonthSeries [stockName=" + stockName + ", month=" + month
				+ ", year=" + year + ", setOfFinanceData=" + setOfFinanceData
				+ "]";
	}
	
			
}
