package com.simplethings.gestioneserietitoli.model;

import java.util.LinkedList;
import java.util.List;

public class FinanceDataYearGroup {
	
	private List<FinanceData> yearFinaceData = new LinkedList<FinanceData>();
	
	private final int year;
	
	public FinanceDataYearGroup(int year){
		this.year = year;
	}
	
	public void addFinanceData(FinanceData financeData){
		yearFinaceData.add(financeData);
	}

	public List<FinanceData> getYearFinaceData() {
		return yearFinaceData;
	}

	public int getYear() {
		return year;
	}

	
}
