package com.simplethings.gestioneserietitoli.model;

public class Stock {
	
	private long id;
	private final String stockName;
	private final String stockDescription;
	private final boolean isActive;
	
	/**
	 * Constructor for stock inactive.
	 * 
	 * @param stockName
	 * @param stockDescription
	 */
	
	public Stock(String stockName, String stockDescription){
		this(stockName, stockDescription, false);
	}
	
	public Stock(String stockName, String stockDescription, boolean isActive){
		this.stockName = stockName;
		this.stockDescription = stockDescription;
		this.isActive = isActive;
	}

	public Stock(long id, String stockName, String stockDescription, boolean isActive){
		this(stockName, stockDescription, isActive);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getStockName() {
		return stockName;
	}

	public String getStockDescription() {
		return stockDescription;
	}

	public boolean isActive() {
		return isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isActive ? 1231 : 1237);
		result = prime
				* result
				+ ((stockDescription == null) ? 0 : stockDescription.hashCode());
		result = prime * result
				+ ((stockName == null) ? 0 : stockName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stock other = (Stock) obj;
		if (isActive != other.isActive)
			return false;
		if (stockDescription == null) {
			if (other.stockDescription != null)
				return false;
		} else if (!stockDescription.equals(other.stockDescription))
			return false;
		if (stockName == null) {
			if (other.stockName != null)
				return false;
		} else if (!stockName.equals(other.stockName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Stock [stockName=" + stockName + ", stockDescription="
				+ stockDescription + ", isActive=" + isActive + "]";
	}
	
		
}
