package com.simplethings.gestioneserietitoli.model;

import java.util.Date;

public class FinanceData implements Comparable<FinanceData>{
	
	private final Date date;
	
	private final double openValue;
	private final double hightValue;
	private final double lowValue;
	private final double closeValue;
	
	public final long volume;
	
	public final double adjCloseValue;

	public FinanceData(Date date, double openValue, double hightValue,
			double lowValue, double closeValue, long volume,
			double adjCloseValue) {
		this.date = date;
		this.openValue = openValue;
		this.hightValue = hightValue;
		this.lowValue = lowValue;
		this.closeValue = closeValue;
		this.volume = volume;
		this.adjCloseValue = adjCloseValue;
	}
	
	public Date getDate() {
		return date;
	}



	public double getOpenValue() {
		return openValue;
	}



	public double getHightValue() {
		return hightValue;
	}



	public double getLowValue() {
		return lowValue;
	}



	public double getCloseValue() {
		return closeValue;
	}



	public long getVolume() {
		return volume;
	}



	public double getAdjCloseValue() {
		return adjCloseValue;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(adjCloseValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(closeValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		temp = Double.doubleToLongBits(hightValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lowValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(openValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (volume ^ (volume >>> 32));
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinanceData other = (FinanceData) obj;
		if (Double.doubleToLongBits(adjCloseValue) != Double
				.doubleToLongBits(other.adjCloseValue))
			return false;
		if (Double.doubleToLongBits(closeValue) != Double
				.doubleToLongBits(other.closeValue))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (Double.doubleToLongBits(hightValue) != Double
				.doubleToLongBits(other.hightValue))
			return false;
		if (Double.doubleToLongBits(lowValue) != Double
				.doubleToLongBits(other.lowValue))
			return false;
		if (Double.doubleToLongBits(openValue) != Double
				.doubleToLongBits(other.openValue))
			return false;
		if (volume != other.volume)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "FinanceData [date=" + date + ", openValue=" + openValue
				+ ", hightValue=" + hightValue + ", lowValue=" + lowValue
				+ ", closeValue=" + closeValue + ", volume=" + volume
				+ ", adjCloseValue=" + adjCloseValue + "]";
	}


	
	public int compareTo(FinanceData other) {
		return this.date.compareTo(other.date);
	}
	
	
		
}
