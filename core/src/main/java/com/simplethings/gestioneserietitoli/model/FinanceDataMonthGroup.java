package com.simplethings.gestioneserietitoli.model;

import java.util.LinkedList;
import java.util.List;

public class FinanceDataMonthGroup {

private List<FinanceData> monthFinaceData = new LinkedList<FinanceData>();
	
	private final int year;
	private final int month;
	
	public FinanceDataMonthGroup(int year, int month){
		this.year = year;
		this.month = month;
	}
	
	public void addFinanceData(FinanceData financeData){
		monthFinaceData.add(financeData);
	}

	public List<FinanceData> getMonthFinaceData() {
		return monthFinaceData;
	}

	public int getYear() {
		return year;
	}
	
	public int getMonth(){
		return month;
	}

	@Override
	public String toString() {
		return "FinanceDataMonthGroup [year=" + year + ", month=" + month
				+ ", monthFinaceData=" + monthFinaceData + "]";
	}
	
	
}
