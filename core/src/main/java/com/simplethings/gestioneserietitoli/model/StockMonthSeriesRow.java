package com.simplethings.gestioneserietitoli.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.utils.StringNumberUtils;

public class StockMonthSeriesRow {
	
	private long rowId;
	public final String stockName;
	public final Date startDate;
	public final Date endDate;
	public final String daySeries;
	public final String openValueSeries;
	public final String heightValueSeries;
	public final String lowValueSeries;
	public final String closeValueSeries;
	public final String volumeValueSeries;
	public final String adjCloseValueSeries;
	
	

	public StockMonthSeriesRow(String stockName, Date startDate,
			Date endDate, String daySeries, String openValueSeries,
			String heightValueSeries, String lowValueSeries,
			String closeValueSeries, String volumeValueSeries,
			String adjCloseValueSeries) {
		super();
		this.stockName = stockName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.daySeries = daySeries;
		this.openValueSeries = openValueSeries;
		this.heightValueSeries = heightValueSeries;
		this.lowValueSeries = lowValueSeries;
		this.closeValueSeries = closeValueSeries;
		this.volumeValueSeries = volumeValueSeries;
		this.adjCloseValueSeries = adjCloseValueSeries;
	}
	
	
	public StockMonthSeriesRow(long rowId, String stock, Date startDate,
			Date endDate, String daySeries, String openValueSeries,
			String heightValueSeries, String lowValueSeries,
			String closeValueSeries, String volumeValueSeries,
			String adjCloseValueSeries) {
		this(stock, startDate,
				endDate, daySeries, openValueSeries,
				heightValueSeries, lowValueSeries,
				closeValueSeries, volumeValueSeries,
				adjCloseValueSeries);
		this.rowId = rowId;
	}


	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}
	
	/*
	 *
	 *
	 */
	public StockMonthSeries convertToNumericMonthSeries(){
		String stock = this.stockName;
		Date startData = this.startDate;
		Date endDate = this.endDate; 
		String seriesOfValueDay = this.daySeries; 
		String seriesOfOpenValue = this.openValueSeries; 
		String seriesOfHightValue = this.heightValueSeries; 
		String seriesOfLowValue = this.lowValueSeries;
		String seriesOfCloseValue = this.closeValueSeries; 
		String seriesOfVolumeValue = this.volumeValueSeries; 
		String seriesOfAdjCloseValue = this.adjCloseValueSeries;
		
		List<FinanceData> listFinanceDate = unserializeNumericMonthSeries(startData, endDate, 
				seriesOfValueDay, seriesOfOpenValue, seriesOfHightValue, 
				seriesOfLowValue, seriesOfCloseValue, seriesOfVolumeValue, 
				seriesOfAdjCloseValue);
		
		StockMonthSeries numericMonthSeries = null;
		
		if(listFinanceDate.size()>0){
			numericMonthSeries = new StockMonthSeries(stock);
			for(FinanceData financeData: listFinanceDate){
				numericMonthSeries.addFinanceData(financeData);
			}
			
		} 
		
		return numericMonthSeries;
	}
	
	private List<FinanceData> unserializeNumericMonthSeries(Date startData, Date endDate, 
			String seriesOfValueDay, String seriesOfOpenValue, String seriesOfHightValue, 
			String seriesOfLowValue, String seriesOfCloseValue, String seriesOfVolumeValue, 
			String seriesOfAdjCloseValue){
		
		List<FinanceData> listOfFinanceData = new ArrayList<FinanceData>();
		
		Calendar calStartDate = Calendar.getInstance();
		calStartDate.setTime(startData);
		
		Calendar calEndDate = Calendar.getInstance(); 
		calEndDate.setTime(endDate);
		
		List<Integer> listOfDays = 
				StringNumberUtils.unserializeSeriesOfDay(seriesOfValueDay);
		
		List<Date> listOfDateCampioni = DateUtils.buildDateCampioniOfMonth(calStartDate.get(Calendar.MONTH), 
				calStartDate.get(Calendar.YEAR), listOfDays);
		
		List<Double> listOfOpenValues = 
				StringNumberUtils.unserializeSepareatedSeriesOfDouble(seriesOfOpenValue, '_');
		List<Double> listOfHightValues = 
				StringNumberUtils.unserializeSepareatedSeriesOfDouble(seriesOfHightValue, '_');
		List<Double> listOfLowValues = 
				StringNumberUtils.unserializeSepareatedSeriesOfDouble(seriesOfLowValue, '_');
		List<Double> listOfCloseValues = 
				StringNumberUtils.unserializeSepareatedSeriesOfDouble(seriesOfCloseValue, '_');
		
		List<Long> listOfVolumeValue = 
				StringNumberUtils.unserializeSepareatedSeriesOfLong(seriesOfVolumeValue, '_');
		
		List<Double> listOfAdjCloseValues = 
				StringNumberUtils.unserializeSepareatedSeriesOfDouble(seriesOfAdjCloseValue, '_');
		
		int index = 0;
		for(Date date: listOfDateCampioni){
			FinanceData finData = new FinanceData(date, 
					listOfOpenValues.get(index), 
					listOfHightValues.get(index), 
					listOfLowValues.get(index), 
					listOfCloseValues.get(index), 
					listOfVolumeValue.get(index), 
					listOfAdjCloseValues.get(index));
			index++;
			listOfFinanceData.add(finData);
		}
		
		return listOfFinanceData;
	}
	
	@Override
	public String toString() {
		return "NumericMonthSeriesRow [rowId=" + rowId + ", stock=" + stockName
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", daySeries=" + daySeries + ", openValueSeries="
				+ openValueSeries + ", hightValueSeries=" + heightValueSeries
				+ ", lowValueSeries=" + lowValueSeries + ", closeValueSeries="
				+ closeValueSeries + ", volumeValueSeries=" + volumeValueSeries
				+ ", adjCloseValueSeries=" + adjCloseValueSeries + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((adjCloseValueSeries == null) ? 0 : adjCloseValueSeries
						.hashCode());
		result = prime
				* result
				+ ((closeValueSeries == null) ? 0 : closeValueSeries.hashCode());
		result = prime * result
				+ ((daySeries == null) ? 0 : daySeries.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime
				* result
				+ ((heightValueSeries == null) ? 0 : heightValueSeries
						.hashCode());
		result = prime * result
				+ ((lowValueSeries == null) ? 0 : lowValueSeries.hashCode());
		result = prime * result
				+ ((openValueSeries == null) ? 0 : openValueSeries.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((stockName == null) ? 0 : stockName.hashCode());
		result = prime
				* result
				+ ((volumeValueSeries == null) ? 0 : volumeValueSeries
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockMonthSeriesRow other = (StockMonthSeriesRow) obj;
		if (adjCloseValueSeries == null) {
			if (other.adjCloseValueSeries != null)
				return false;
		} else if (!adjCloseValueSeries.equals(other.adjCloseValueSeries))
			return false;
		if (closeValueSeries == null) {
			if (other.closeValueSeries != null)
				return false;
		} else if (!closeValueSeries.equals(other.closeValueSeries))
			return false;
		if (daySeries == null) {
			if (other.daySeries != null)
				return false;
		} else if (!daySeries.equals(other.daySeries))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (heightValueSeries == null) {
			if (other.heightValueSeries != null)
				return false;
		} else if (!heightValueSeries.equals(other.heightValueSeries))
			return false;
		if (lowValueSeries == null) {
			if (other.lowValueSeries != null)
				return false;
		} else if (!lowValueSeries.equals(other.lowValueSeries))
			return false;
		if (openValueSeries == null) {
			if (other.openValueSeries != null)
				return false;
		} else if (!openValueSeries.equals(other.openValueSeries))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (stockName == null) {
			if (other.stockName != null)
				return false;
		} else if (!stockName.equals(other.stockName))
			return false;
		if (volumeValueSeries == null) {
			if (other.volumeValueSeries != null)
				return false;
		} else if (!volumeValueSeries.equals(other.volumeValueSeries))
			return false;
		return true;
	}

		

}
