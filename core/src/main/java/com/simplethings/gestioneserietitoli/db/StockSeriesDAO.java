package com.simplethings.gestioneserietitoli.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.model.StockMonthSeriesRow;
import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.utils.StockMonthSeriesFilter;

public class StockSeriesDAO {

	private static Logger logger = LoggerFactory
			.getLogger(StockSeriesDAO.class);

	private enum QUERY_TYPE {
		NO_RANGE, FROM_DATE, TO_DATE, RANGE
	};

	private DataSource dataSource;

	public StockSeriesDAO(DataSource applicationDataSource) {
		this.dataSource = applicationDataSource;
	}

   	public void save(StockMonthSeries stockMonthSeries) throws SQLException {

   		Connection conn = null;
   		PreparedStatement prepareInsertNumericMonthSeriesRow = null;
   		
   		try {
			conn = dataSource.getConnection();
	
			StockMonthSeriesRow numericMonthSeriesRow = stockMonthSeries
					.convertToStrockMonthSeriesRow();
	
			prepareInsertNumericMonthSeriesRow = conn
					.prepareStatement("INSERT INTO STOCK_SERIES "
							+ "(id_stock, start_date, end_date, day_series, open_value_series, "
							+ "height_value_series, low_value_series, close_value_series, "
							+ "volume_value_series, adj_close_value_series) "
							+ "VALUES ((SELECT ID FROM STOCKS_INFORMATION WHERE STOCK_NAME=?),?,?,?,?,?,?,?,?,?)");
	
			prepareInsertNumericMonthSeriesRow.setString(1,
					numericMonthSeriesRow.stockName);
	
			prepareInsertNumericMonthSeriesRow.setDate(2, new java.sql.Date(
					numericMonthSeriesRow.startDate.getTime()));
			prepareInsertNumericMonthSeriesRow.setDate(3, new java.sql.Date(
					numericMonthSeriesRow.endDate.getTime()));
	
			prepareInsertNumericMonthSeriesRow.setString(4,
					numericMonthSeriesRow.daySeries);
			prepareInsertNumericMonthSeriesRow.setString(5,
					numericMonthSeriesRow.openValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(6,
					numericMonthSeriesRow.heightValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(7,
					numericMonthSeriesRow.lowValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(8,
					numericMonthSeriesRow.closeValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(9,
					numericMonthSeriesRow.volumeValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(10,
					numericMonthSeriesRow.adjCloseValueSeries);
	
			prepareInsertNumericMonthSeriesRow.executeUpdate();
	        
   		} finally {
   			if( prepareInsertNumericMonthSeriesRow!= null )
   				prepareInsertNumericMonthSeriesRow.close();
            if( conn!= null )
            	conn.close();
   		}

	}

	public void update(StockMonthSeries stockMonthSeries) throws SQLException {

		Connection conn = null;
		PreparedStatement prepareInsertNumericMonthSeriesRow = null;

		try {
			conn = dataSource.getConnection();

			StockMonthSeriesRow numericMonthSeriesRow = stockMonthSeries
					.convertToStrockMonthSeriesRow();

			prepareInsertNumericMonthSeriesRow = conn
					.prepareStatement("UPDATE STOCK_SERIES A"
							+ " SET A.end_date = ? , A.day_series = ? , A.open_value_series = ?, "
							+ "A.height_value_series = ?, A.low_value_series = ?, A.close_value_series = ?, "
							+ "A.volume_value_series = ?, A.adj_close_value_series = ? WHERE A.ID = "
							+ "(SELECT C.ID FROM STOCK_SERIES C, STOCKS_INFORMATION B WHERE C.ID_STOCK = B.ID and B.STOCK_NAME=? and C.start_date = ?)");

			prepareInsertNumericMonthSeriesRow.setDate(1, new java.sql.Date(
					numericMonthSeriesRow.endDate.getTime()));

			prepareInsertNumericMonthSeriesRow.setString(2,
					numericMonthSeriesRow.daySeries);
			prepareInsertNumericMonthSeriesRow.setString(3,
					numericMonthSeriesRow.openValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(4,
					numericMonthSeriesRow.heightValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(5,
					numericMonthSeriesRow.lowValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(6,
					numericMonthSeriesRow.closeValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(7,
					numericMonthSeriesRow.volumeValueSeries);
			prepareInsertNumericMonthSeriesRow.setString(8,
					numericMonthSeriesRow.adjCloseValueSeries);

			prepareInsertNumericMonthSeriesRow.setString(9,
					numericMonthSeriesRow.stockName);

			prepareInsertNumericMonthSeriesRow.setDate(10, new java.sql.Date(
					numericMonthSeriesRow.startDate.getTime()));

			prepareInsertNumericMonthSeriesRow.executeUpdate();
		} finally {
			if (prepareInsertNumericMonthSeriesRow != null)
				prepareInsertNumericMonthSeriesRow.close();
			if (conn != null)
				conn.close();
		}
	}

	public List<StockMonthSeries> load(String ticket) throws SQLException {
		return loadToDate(ticket, null);
	}

	public StockMonthSeries loadLastStockMonthSeries(String ticket)
			throws SQLException {
		StockMonthSeriesRow stockMonthSeriesRow = null;
		StockMonthSeries stockMonthSeries = null;

		Connection conn = dataSource.getConnection();

		PreparedStatement preparedStatement = null;

		String selectTableSQL = "SELECT A.ID, B.stock_name, A.start_date, A.end_date, A.day_series, A.open_value_series, "
				+ "A.height_value_series, A.low_value_series, A.close_value_series, "
				+ "A.volume_value_series, A.adj_close_value_series from STOCK_SERIES A, "
				+ "STOCKS_INFORMATION B where A.ID_STOCK = B.ID and B.STOCK_NAME = ? order by start_date desc";

		try {
			preparedStatement = conn.prepareStatement(selectTableSQL);

			preparedStatement.setString(1, ticket);

			// execute select SQL stetement
			ResultSet rs = preparedStatement.executeQuery();

			if (rs.next()) {
				long rowId = rs.getLong("ID");
				String stock = rs.getString("stock_name");
				Date startDate = rs.getDate("start_date");
				Date endDate = rs.getDate("end_date");
				String daySeries = rs.getString("day_series");
				String openValueSeries = rs.getString("open_value_series");
				String heightValueSeries = rs.getString("height_value_series");
				String lowValueSeries = rs.getString("low_value_series");
				String closeValueSeries = rs.getString("close_value_series");
				String volumeValueSeries = rs.getString("volume_value_series");
				String adjCloseValueSeries = rs
						.getString("adj_close_value_series");
				stockMonthSeriesRow = new StockMonthSeriesRow(rowId, stock,
						startDate, endDate, daySeries, openValueSeries,
						heightValueSeries, lowValueSeries, closeValueSeries,
						volumeValueSeries, adjCloseValueSeries);
			}

			rs.close();

		} catch (SQLException e) {

			logger.error(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (conn != null) {
				conn.close();
			}

		}

		if (stockMonthSeriesRow != null)
			stockMonthSeries = stockMonthSeriesRow
					.convertToNumericMonthSeries();

		return stockMonthSeries;
	}

	public List<StockMonthSeries> loadToDate(String ticket, Date toDate)
			throws SQLException {
		return load(ticket, null, toDate);
	}

	public List<StockMonthSeries> loadFromDate(String ticket, Date fromDate)
			throws SQLException {
		return load(ticket, fromDate, null);
	}

	public List<StockMonthSeries> loadIntervall(String ticket, Date fromDate,
			Date toDate) throws SQLException {
		return load(ticket, fromDate, toDate);
	}

	private List<StockMonthSeries> load(String ticket, Date fromDate,
			Date toDate) throws SQLException {
		List<StockMonthSeries> listNumericMonthSeries = new ArrayList<StockMonthSeries>();

		QUERY_TYPE queryType = null;

		String selectTableSQL = null;
		if (fromDate != null) {

			if (toDate != null) {
				queryType = QUERY_TYPE.RANGE;

				selectTableSQL = "SELECT A.ID, B.stock_name, A.start_date, A.end_date, A.day_series, A.open_value_series, "
						+ "A.height_value_series, A.low_value_series, A.close_value_series, "
						+ "A.volume_value_series, A.adj_close_value_series from STOCK_SERIES A, "
						+ "STOCKS_INFORMATION B where A.ID_STOCK = B.ID and B.STOCK_NAME = ? and start_date >= ? and start_date <= ? order by start_date asc";
			} else {
				queryType = QUERY_TYPE.FROM_DATE;

				selectTableSQL = "SELECT A.ID, B.stock_name, A.start_date, A.end_date, A.day_series, A.open_value_series, "
						+ "A.height_value_series, A.low_value_series, A.close_value_series, "
						+ "A.volume_value_series, A.adj_close_value_series from STOCK_SERIES A, "
						+ "STOCKS_INFORMATION B where A.ID_STOCK = B.ID and B.STOCK_NAME = ? and start_date >= ? order by start_date asc";
			}
		} else if (toDate != null) {
			queryType = QUERY_TYPE.TO_DATE;

			selectTableSQL = "SELECT A.ID, B.stock_name, A.start_date, A.end_date, A.day_series, A.open_value_series, "
					+ "A.height_value_series, A.low_value_series, A.close_value_series, "
					+ "A.volume_value_series, A.adj_close_value_series from STOCK_SERIES A, "
					+ "STOCKS_INFORMATION B where A.ID_STOCK = B.ID and B.STOCK_NAME = ? and start_date <= ? order by start_date asc";
		} else {
			queryType = QUERY_TYPE.NO_RANGE;

			selectTableSQL = "SELECT A.ID, B.stock_name, A.start_date, A.end_date, A.day_series, A.open_value_series, "
					+ "A.height_value_series, A.low_value_series, A.close_value_series, "
					+ "A.volume_value_series, A.adj_close_value_series from STOCK_SERIES A, "
					+ "STOCKS_INFORMATION B where A.ID_STOCK = B.ID and B.STOCK_NAME = ? order by start_date asc";
		}

		Connection conn = dataSource.getConnection();
		PreparedStatement prepareInsertNumericMonthSeriesRow = conn
				.prepareStatement(selectTableSQL);

		prepareInsertNumericMonthSeriesRow.setString(1, ticket);

		switch (queryType) {
			case RANGE: {
				Date fromDateLower = DateUtils.getStartMonthDate(fromDate);
				prepareInsertNumericMonthSeriesRow.setDate(2, new java.sql.Date(
					fromDateLower.getTime()));
				prepareInsertNumericMonthSeriesRow.setDate(3, new java.sql.Date(
					toDate.getTime()));
				break;
			}
			case FROM_DATE: {
				Date fromDateLower = DateUtils.getStartMonthDate(fromDate);
				prepareInsertNumericMonthSeriesRow.setDate(2, new java.sql.Date(
					fromDateLower.getTime()));
				break;
			}
			case TO_DATE:
				prepareInsertNumericMonthSeriesRow.setDate(2, new java.sql.Date(
					toDate.getTime()));
				break;

			}

		ResultSet rs = prepareInsertNumericMonthSeriesRow.executeQuery();
		while (rs.next()) {
			long rowId = rs.getLong("ID");
			String stockName = rs.getString("stock_name");
			Date startDate = rs.getDate("start_date");
			Date endDate = rs.getDate("end_date");
			String daySeries = rs.getString("day_series");
			String openValueSeries = rs.getString("open_value_series");
			String heightValueSeries = rs.getString("height_value_series");
			String lowValueSeries = rs.getString("low_value_series");
			String closeValueSeries = rs.getString("close_value_series");
			String volumeValueSeries = rs.getString("volume_value_series");
			String adjCloseValueSeries = rs.getString("adj_close_value_series");
			StockMonthSeriesRow numericMonthSeriesRow = new StockMonthSeriesRow(
					rowId, stockName, startDate, endDate, daySeries,
					openValueSeries, heightValueSeries, lowValueSeries,
					closeValueSeries, volumeValueSeries, adjCloseValueSeries);

			StockMonthSeries stockMonthSeries = numericMonthSeriesRow
					.convertToNumericMonthSeries();
			
			stockMonthSeries = StockMonthSeriesFilter.doFilterStockMonthSeries(stockMonthSeries, fromDate, toDate);			
			
			if(stockMonthSeries!=null)
				listNumericMonthSeries.add(stockMonthSeries);
		}

		rs.close();
		prepareInsertNumericMonthSeriesRow.close();
		conn.close();

		return listNumericMonthSeries;
	}

	
}
