package com.simplethings.gestioneserietitoli.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.model.Stock;

public class StockInformationDAO {
	
	private static Logger logger = LoggerFactory
			.getLogger(StockInformationDAO.class);
	
	private DataSource dataSource;
	
	public StockInformationDAO(DataSource applicationDataSource) {
		this.dataSource = applicationDataSource;
	}
	
	public void createStock(Stock stockRow) throws SQLException {
		Connection conn = null;
		PreparedStatement prepareInsertStockRow = null;
		
		try {
			conn = dataSource.getConnection();
			prepareInsertStockRow = conn
					.prepareStatement("INSERT INTO STOCKS_INFORMATION "
							+ "(stock_name, stock_descr, active) " + "VALUES (?, ?, ?)");
	
			prepareInsertStockRow.setString(1, stockRow.getStockName());
			prepareInsertStockRow.setString(2, stockRow.getStockDescription());
			prepareInsertStockRow.setBoolean(3, stockRow.isActive());
			
			prepareInsertStockRow.executeUpdate();
			prepareInsertStockRow.close();
			conn.close();
		} finally {
			if(prepareInsertStockRow != null) 
				prepareInsertStockRow.close();
			if(conn != null)
				conn.close();
		}
	}
	
	public boolean activateStock(String stockName) throws SQLException {
		Connection conn = dataSource.getConnection();
		PreparedStatement prepareInsertStockRow = conn
				.prepareStatement("UPDATE STOCKS_INFORMATION "
						+ "SET ACTIVE = TRUE WHERE STOCK_NAME = ?");

		prepareInsertStockRow.setString(1, stockName);

		int count = prepareInsertStockRow.executeUpdate();
		prepareInsertStockRow.close();
		conn.close();
		
		return (count==1)? true: false;
		
	}
	
	public boolean deactivateStock(String stockName) throws SQLException {
		Connection conn = dataSource.getConnection();
		PreparedStatement prepareInsertStockRow = conn
				.prepareStatement("UPDATE STOCKS_INFORMATION "
						+ "SET ACTIVE = FALSE WHERE STOCK_NAME = ?");

		prepareInsertStockRow.setString(1, stockName);

		int count = prepareInsertStockRow.executeUpdate();
		prepareInsertStockRow.close();
		conn.close();
		
		return (count==1)? true: false;
		
	}
	
	/*
	 * Method 
	 *  
	 */
	
	public void getListOfActiveStock(BlockingQueue<Optional<Stock>> queue) throws SQLException {
		
		String selectTableSQL = 
				"SELECT ID, STOCK_NAME, STOCK_DESCR FROM STOCKS_INFORMATION WHERE ACTIVE IS TRUE";

		Connection conn = null;
		PreparedStatement prepareInsertNumericMonthSeriesRow = null;
		ResultSet rs =  null;
		
		try {
			conn = dataSource.getConnection();
			prepareInsertNumericMonthSeriesRow = conn
					.prepareStatement(selectTableSQL);
			rs = prepareInsertNumericMonthSeriesRow.executeQuery();
			while (rs.next()) {
				long idStock = rs.getLong("ID");
				String stockName = rs.getString("STOCK_NAME");
				String stockDescription = rs.getString("STOCK_DESCR");
				
				queue.add(Optional.of(new Stock(idStock, stockName, stockDescription, true)));
			}
			
			queue.add(Optional.<Stock>absent());
		} finally {
			// no problem if more
			queue.add(Optional.<Stock>absent());
			
			if(rs!=null) 
				rs.close();
			if(prepareInsertNumericMonthSeriesRow != null) 
				prepareInsertNumericMonthSeriesRow.close();
			if(conn!=null) 
				conn.close();
		}
	
	}
	
	public List<Stock> getListOfActiveStock() throws SQLException {
		List<Stock> listOfStock = new ArrayList<Stock>();

		String selectTableSQL = 
				"SELECT ID, STOCK_NAME, STOCK_DESCR FROM STOCKS_INFORMATION WHERE ACTIVE IS TRUE ORDER BY STOCK_NAME";

		Connection conn = null;
		PreparedStatement prepareInsertNumericMonthSeriesRow = null;
		ResultSet rs =  null;
		
		try {
			conn = dataSource.getConnection();
			prepareInsertNumericMonthSeriesRow = conn
					.prepareStatement(selectTableSQL);
			rs = prepareInsertNumericMonthSeriesRow.executeQuery();
			while (rs.next()) {
				long idStock = rs.getLong("ID");
				String stockName = rs.getString("STOCK_NAME");
				String stockDescription = rs.getString("STOCK_DESCR");
				listOfStock.add(new Stock(idStock, stockName, stockDescription, true));
			}
		} finally {
			if(rs!=null) 
				rs.close();
			if(prepareInsertNumericMonthSeriesRow != null) 
				prepareInsertNumericMonthSeriesRow.close();
			if(conn!=null) 
				conn.close();
		}

		return listOfStock;
	}
	
	public int getCountNumberOfAllStock() throws SQLException {
		int numStock = 0;

		String selectTableSQL = "SELECT count(*) FROM STOCKS_INFORMATION";

		Connection conn = null;
		PreparedStatement prepareInsertNumericMonthSeriesRow = null;
		ResultSet rs =  null;
		
		try {
			conn = dataSource.getConnection();
			prepareInsertNumericMonthSeriesRow = conn
					.prepareStatement(selectTableSQL);
			
			
			
			rs = prepareInsertNumericMonthSeriesRow.executeQuery();
			if(rs.next()) {
				numStock = rs.getInt(1);
			}
		} finally {
			if(rs!=null) 
				rs.close();
			if(prepareInsertNumericMonthSeriesRow != null) 
				prepareInsertNumericMonthSeriesRow.close();
			if(conn!=null) 
				conn.close();
		}

		return numStock;
	}
	
	public List<Stock> getListOfAllStock(int limit, int offset) throws SQLException {
		List<Stock> listOfStock = new ArrayList<Stock>();

		String selectTableSQL = 
				"SELECT ID, STOCK_NAME, STOCK_DESCR, ACTIVE FROM STOCKS_INFORMATION order by STOCK_NAME LIMIT ? OFFSET ?";

		Connection conn = null;
		PreparedStatement prepareInsertNumericMonthSeriesRow = null;
		ResultSet rs =  null;
		
		try {
			conn = dataSource.getConnection();
			prepareInsertNumericMonthSeriesRow = conn
					.prepareStatement(selectTableSQL);
			
			prepareInsertNumericMonthSeriesRow.setInt(1, limit);
			prepareInsertNumericMonthSeriesRow.setInt(2, offset);
			
			rs = prepareInsertNumericMonthSeriesRow.executeQuery();
			while (rs.next()) {
				long idStock = rs.getLong("ID");
				String stockName = rs.getString("STOCK_NAME");
				String stockDescription = rs.getString("STOCK_DESCR");
				boolean isActive = rs.getBoolean("ACTIVE");
				listOfStock.add(new Stock(idStock, stockName, stockDescription, isActive));
			}
		} finally {
			if(rs!=null) 
				rs.close();
			if(prepareInsertNumericMonthSeriesRow != null) 
				prepareInsertNumericMonthSeriesRow.close();
			if(conn!=null) 
				conn.close();
		}

		return listOfStock;
	}
	
	public Stock getActiveStock(String stockName) throws SQLException {
		Connection conn = dataSource.getConnection();

		Stock stockRow = null;
		PreparedStatement statement = null;

		String selectTableSQL = 
				"SELECT ID, stock_name, stock_descr from STOCKS_INFORMATION where STOCK_NAME = ? AND ACTIVE IS TRUE";

		try {
			statement = conn.prepareStatement(selectTableSQL);
			statement.setString(1, stockName);

			// execute select SQL stetement
			ResultSet rs = statement.executeQuery();

			if (rs.next()) {
				long rowId = rs.getLong("ID");
				String stock = rs.getString("stock_name");
				String stockDescription = rs.getString("stock_descr");
				stockRow = new Stock(rowId, stock, stockDescription, true);
			}

			rs.close();

		} catch (SQLException e) {

			logger.error(e.getMessage());

		} finally {

			if (statement != null) {
				statement.close();
			}

			if (conn != null) {
				conn.close();
			}

		}

		return stockRow;
	}
	
	public Stock searchByStockName(String stockName) throws SQLException {
		
		if(stockName==null)
			throw new IllegalArgumentException("stock name can't be null");
			
		Connection conn = dataSource.getConnection();

		Stock stockRow = null;
		PreparedStatement statement = null;

		String selectTableSQL = 
				"SELECT * FROM STOCKS_INFORMATION where STOCK_NAME = ?";

		try {
			statement = conn.prepareStatement(selectTableSQL);
			statement.setString(1, stockName.toUpperCase());

			// execute select SQL stetement
			ResultSet rs = statement.executeQuery();

			if (rs.next()) {
				long rowId = rs.getLong("ID");
				String stock = rs.getString("stock_name");
				String stockDescription = rs.getString("stock_descr");
				boolean isActive = rs.getBoolean("active");
				stockRow = new Stock(rowId, stock, stockDescription, isActive);
			}

			rs.close();

		} catch (SQLException e) {

			logger.error(e.getMessage());

		} finally {

			if (statement != null) {
				statement.close();
			}

			if (conn != null) {
				conn.close();
			}

		}
		
		
		return stockRow;
	}

	public List<Stock> searchByDescrOrStockCode(String stockDescr) throws SQLException {
		
		List<Stock> listOfStock = new LinkedList<Stock>();
		
		Connection conn = dataSource.getConnection();

		Stock stockRow = null;
		PreparedStatement statement = null;

		String selectTableSQL = 
				"SELECT * FROM STOCKS_INFORMATION where UPPER(STOCK_DESCR) LIKE ? OR UPPER(STOCK_NAME) LIKE ?";

		try {
			statement = conn.prepareStatement(selectTableSQL);
			statement.setString(1, "%" + stockDescr.toUpperCase() + "%");
			statement.setString(2, "%" + stockDescr.toUpperCase() + "%");
			
			// execute select SQL stetement
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				long rowId = rs.getLong("ID");
				String stock = rs.getString("stock_name");
				String stockDescription = rs.getString("stock_descr");
				boolean isActive = rs.getBoolean("active");
				stockRow = new Stock(rowId, stock, stockDescription, isActive);
				listOfStock.add(stockRow);
			}

			rs.close();

		} catch (SQLException e) {

			logger.error(e.getMessage());

		} finally {

			if (statement != null) {
				statement.close();
			}

			if (conn != null) {
				conn.close();
			}

		}
		
		
		return listOfStock;
	}

}
