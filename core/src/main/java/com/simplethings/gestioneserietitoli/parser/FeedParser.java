package com.simplethings.gestioneserietitoli.parser;

import java.text.ParseException;
import java.util.Date;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.utils.DateUtils;

public class FeedParser {
	
	private static Logger logger = LoggerFactory.getLogger(FeedParser.class);
	
	/*
	 * Example: 2010-01-25,546.59,549.88,525.61,529.94,4021800,529.94
	 */
	
	public static FinanceData doParseLine(String line) {
		Date dateCampione = null;

		double openValue = -1;
		double hightValue = -1;
		double lowValue = -1;
		double closeValue = -1;

		long volume = -1;

		double adjCloseValue = -1;

		StringTokenizer st = new StringTokenizer(line, ",");

		int index = 0;
		while (st.hasMoreTokens()) {
			String value = st.nextToken();
			index++;

			switch (index) {
			case 1:
				// date
				try {
					dateCampione = extractDateValue(value);
				} catch (ParseException e) {
					logger.warn("dateCampione ParseException on value: {} - line: {}", value, line);
				}

				break;
			case 2:
				// open
				try {
					openValue = extractDoubleValue(value);
				} catch (NumberFormatException e) {
					logger.warn("openValue ParseException on value: {} - line: {}", value, line);
				}
				break;
			case 3:
				// hight
				try {
					hightValue = extractDoubleValue(value);
				} catch (NumberFormatException e) {
					logger.warn("hightValue ParseException on value: {} - line: {}", value, line);
				}
				break;
			case 4:
				// low
				try {
					lowValue = extractDoubleValue(value);
				} catch (NumberFormatException e) {
					logger.warn("lowValue ParseException on value: {} - line: {}", value, line);
				}
				break;
			case 5:
				// closeValue
				try {
					closeValue = extractDoubleValue(value);
				} catch (NumberFormatException e) {
					logger.warn("closeValue ParseException on value: {} - line: {}", value, line);
				}
				break;
			case 6:
				// volume
				try {
					volume = extractLongValue(value);
				} catch (NumberFormatException e) {
					logger.warn("volume ParseException on value: {} - line: {}", value, line);
				}
				break;
			case 7:
				// adj close
				try {
					adjCloseValue = extractDoubleValue(value);
				} catch (NumberFormatException e) {
					logger.warn("adjCloseValue ParseException on value: {} - line: {}", value, line);
				}
				break;
			}

		}

		return new FinanceData(dateCampione, openValue, hightValue, lowValue,
				closeValue, volume, adjCloseValue);
	}

	private static Date extractDateValue(String dateValue) throws ParseException {
		return DateUtils.buildCalendarDate(DateUtils.FEED_DATE_PATTERN, dateValue).getTime();
	}

	private static double extractDoubleValue(String doubleValue)
			throws NumberFormatException {
		return Double.parseDouble(doubleValue);
	}

	private static long extractLongValue(String longValue)
			throws NumberFormatException {
		return Long.parseLong(longValue);
	}


}
