package com.simplethings.gestioneserietitoli.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

public class StockInformationParser {
	
private static Logger logger = LoggerFactory.getLogger(StockInformationParser.class);
	
	private BlockingQueue<Optional<CompanyInformation>> queueIndustryInformation = new LinkedBlockingQueue<Optional<CompanyInformation>>();
	
	public class IndustryInformation {
		public final String id;
		public final String name;
		
		public IndustryInformation(String id, String name) {
			this.id = id;
			this.name = name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			IndustryInformation other = (IndustryInformation) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}


		@Override
		public String toString() {
			return "IndustryInformation [id=" + id + ", name=" + name + "]";
		}


		private StockInformationParser getOuterType() {
			return StockInformationParser.this;
		}
		
		
		
	}
	
	public class CompanyInformation {
		public final IndustryInformation idIndustryInformation;
		public final String name;
		public final String symbol;
		
		public CompanyInformation(IndustryInformation idIndustryInformation, String name,
				String symbol) {
			this.idIndustryInformation = idIndustryInformation;
			this.name = name;
			this.symbol = symbol;
		}

		@Override
		public String toString() {
			return "CompanyInformation [idIndustryInformation="
					+ idIndustryInformation.id + ", name=" + name + ", symbol="
					+ symbol + "]";
		}
		
		
	}
	
	public void addCompanyInformation(CompanyInformation companyInformation){
		Optional<CompanyInformation> value = Optional.fromNullable(companyInformation);
		
		try {
			queueIndustryInformation.put(value);
		} catch (InterruptedException e) {
			logger.warn("InterruptedException:  {} - on {}", e,  companyInformation);
		}
	}
	
	public void doWork(InputStream input, BlockingQueue<Optional<CompanyInformation>> queueIndustryInformation) throws JsonParseException, IOException{
		
		JsonFactory f = new JsonFactory();
		JsonParser jp = f.createJsonParser(input);

		JsonToken token2 = jp.nextToken(); // will return JsonToken.START_OBJECT (verify?)
		
		while (jp.nextToken() != JsonToken.END_OBJECT) {
		  String fieldname = jp.getCurrentName();
		  JsonToken token =  jp.getCurrentToken();
		  String text = jp.getText();
		  // jp.nextToken(); // move to value, or START_OBJECT/START_ARRAY
		  
		  
		  if("industry".equals(fieldname) && token == JsonToken.START_ARRAY){
			  
			  String currentId = null;
			  String currentName = null;
			  IndustryInformation industryInformation = null;
			  
			  // ciclo sulle industry
			  while(jp.nextToken() != JsonToken.END_ARRAY){
				  
				  String name = jp.getCurrentName();
				  
				  if("id".equals(name) && jp.getCurrentToken() == JsonToken.VALUE_STRING){
					  currentId = jp.getText(); 
				  }
 
				  if("name".equals(name) && jp.getCurrentToken() == JsonToken.VALUE_STRING){
					  currentName = jp.getText();
					  
					  // 
					  industryInformation = new IndustryInformation(currentId, currentName);
				  }
				  
				  // ciclo sulle company
				  
				  if("company".equals(name) && token == JsonToken.START_ARRAY){
					  
					  String currentNameCompany = null;
					  String currentSymbol = null;
					  CompanyInformation companyInformation = null;
					  
					  while(jp.nextToken() != JsonToken.END_ARRAY){
						  String nameInCompany = jp.getCurrentName();
					  
						  if("name".equals(nameInCompany) && jp.getCurrentToken() == JsonToken.VALUE_STRING){
							  currentNameCompany = jp.getText(); 
						  }
		 
						  if("symbol".equals(nameInCompany) && jp.getCurrentToken() == JsonToken.VALUE_STRING){
							  currentSymbol = jp.getText();
							  
							  // 
							  companyInformation = new CompanyInformation(industryInformation, currentNameCompany, currentSymbol);
							  try {
								queueIndustryInformation.put(Optional.of(companyInformation));
							  } catch (InterruptedException e) {
								  logger.warn("InterruptedException: {} - on: {}", e, companyInformation);
							  }
							  
						  }
						  
					  }
					  
					  
				  }
			  }
			  
		  }
		 
		}
		
		try {
			queueIndustryInformation.put(Optional.<CompanyInformation>absent());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jp.close(); // ensure resources get cleaned up timely and properly
		
	}

	
	
}
