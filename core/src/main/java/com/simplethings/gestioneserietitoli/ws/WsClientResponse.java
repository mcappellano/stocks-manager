package com.simplethings.gestioneserietitoli.ws;

public abstract class WsClientResponse<T> {
	
	public enum STATUS {
		OK,
		NO_RESPONSE
	}

	public final STATUS status;
	
	protected WsClientResponse(STATUS status) {
		this.status = status;
	}
			
}
