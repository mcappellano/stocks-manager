package com.simplethings.gestioneserietitoli.ws.stocksinformation;

import com.simplethings.gestioneserietitoli.parser.StockInformationParser.CompanyInformation;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse;

public class StocksInformationResponse extends WsClientResponse<CompanyInformation> {
	
	public StocksInformationResponse(STATUS status){
		super(status);
	}
	
	@Override
	public String toString() {
		return "StockInformationResponse [status=" + status + "]";
	}
}
