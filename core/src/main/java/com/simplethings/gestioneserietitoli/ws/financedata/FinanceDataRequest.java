package com.simplethings.gestioneserietitoli.ws.financedata;

import java.util.Calendar;
import static com.simplethings.gestioneserietitoli.utils.DateUtils.doFormat;

public class FinanceDataRequest {
	
	public enum TradingPeriod {
		DAILY("d"),
		WEEKLY("w"),
		MONTLY("m");
		
		private final String value;
		
		private TradingPeriod(String value){
			this.value = value;
		}
		
		public String value(){
			return value;
		}
	}
	
	private final String ticket;
	private final Calendar fromDate;
	private final Calendar toDate;
	private final TradingPeriod tradingPeriod;
	
	public FinanceDataRequest(String ticket, 
			Calendar fromDate, Calendar toDate, TradingPeriod tradingPeriod){
		
		if(ticket==null){
			throw new IllegalArgumentException("Invalid request paremeter: ticket is NULL");
		}
		
		if(fromDate!=null){
			fromDate.set(Calendar.MILLISECOND, 0);
		}
		
		if(toDate!=null){
			toDate.set(Calendar.MILLISECOND, 0);
		}
		
		this.ticket = ticket;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.tradingPeriod = tradingPeriod;
	}
	
		
	public String getTicket() {
		return ticket;
	}



	public Calendar getFromDate() {
		return fromDate;
	}



	public Calendar getToDate() {
		return toDate;
	}



	public TradingPeriod getTradingPeriod() {
		return tradingPeriod;
	}


	/*
	 * 
	 * s=BAS.DE&a=0&b=15&c=2000&d=0&e=31&f=2010&g=w&ignore=.csv
	 * 
	 */
	
	public String buildQueryString(){
		String queryString = "?s=" + ticket;
		
		if(fromDate!=null){
			int numDay = fromDate.get(Calendar.DAY_OF_MONTH);
			int numMonth = fromDate.get(Calendar.MONTH);
			int numYear = fromDate.get(Calendar.YEAR);
			queryString = queryString + "&a=" + numMonth + "&b=" + numDay + "&c=" + numYear;
		}
		
		if(toDate!=null){
			int numDay = toDate.get(Calendar.DAY_OF_MONTH);
			int numMonth = toDate.get(Calendar.MONTH);
			int numYear = toDate.get(Calendar.YEAR);
			queryString = queryString + "&d=" + numMonth + "&e=" + numDay + "&f=" + numYear;
		}
		
		if(tradingPeriod!=null){
			queryString = queryString + "&g=" + tradingPeriod.value();
		}
		
		return queryString+"&ignore=.csv";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((ticket == null) ? 0 : ticket.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result
				+ ((tradingPeriod == null) ? 0 : tradingPeriod.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinanceDataRequest other = (FinanceDataRequest) obj;
		if (fromDate == null) {
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (ticket == null) {
			if (other.ticket != null)
				return false;
		} else if (!ticket.equals(other.ticket))
			return false;
		if (toDate == null) {
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		if (tradingPeriod != other.tradingPeriod)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "FiananceDataRequest [ticket=" + ticket + ", fromDate="
				+ doFormat(fromDate) + ", toDate=" + doFormat(toDate) + ", tradingPeriod="
				+ tradingPeriod + "]";
	}
	
	

}
