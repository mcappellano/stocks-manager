package com.simplethings.gestioneserietitoli.ws.financedata;

import java.util.concurrent.BlockingQueue;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse;

public class FiananceDataResponse extends WsClientResponse<String> {
	
	public FiananceDataResponse(STATUS status, BlockingQueue<Optional<String>> queue){
		super(status);
	}
	
	public FiananceDataResponse(STATUS status){
		super(status);
	}
	
	@Override
	public String toString() {
		return "FiananceDataResponse [status=" + status + "]";
	}
}
