package com.simplethings.gestioneserietitoli.ws.financedata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerFinancialData;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse.STATUS;

public class FinanceDataWsClient {
	
	private static Logger logger = LoggerFactory.getLogger(FinanceDataWsClient.class);

	private static final String DEFAULT_URL= "http://ichart.yahoo.com/table.csv";
	
	private final URL url;
	private ResponseHandlerFinancialData responseHandler;
	
	public FinanceDataWsClient(ResponseHandlerFinancialData responseHandler) throws ApplicationException {
		this(DEFAULT_URL, responseHandler);
	}
	
	public FinanceDataWsClient(String urlValue, ResponseHandlerFinancialData responseHandler) throws ApplicationException {
		try {
			this.url = new URL(urlValue);
			this.responseHandler = responseHandler;
		} catch (MalformedURLException e) {
			logger.error("MalformedURLException, url: {} - {}", urlValue, e.getMessage());
			throw new ApplicationException("MalformedURLException url: " + urlValue, e);
		}

	}
	
	public FiananceDataResponse doRequest(FinanceDataRequest request) throws ApplicationException {
		
		BlockingQueue<Optional<String>> queue = new LinkedBlockingQueue<Optional<String>>();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		HttpGet httpGet = new HttpGet(url.toString() + request.buildQueryString());
		CloseableHttpResponse response;
		FiananceDataResponse gestioneTitoliResponse;
		try {
			response = httpclient.execute(httpGet);
			
			// The underlying HTTP connection is still held by the response object
			// to allow the response content to be streamed directly from the network socket.
			// In order to ensure correct deallocation of system resources
			// the user MUST either fully consume the response content  or abort request
			// execution by calling CloseableHttpResponse#close().

			try {
				StatusLine statusLine = response.getStatusLine();
			    int statusCode = statusLine.getStatusCode();
			    
			    logger.debug("statusCode: {}, for request: {}", statusCode, request);
			    
			    if(statusCode == 200){
			    	HttpEntity entity = response.getEntity();
			    	Thread worker = responseHandler.doStartQueueProcessor(request.getTicket(), queue);
			    	
			    	readHttpEntityIntoQueue(entity, queue);
			    	
			    	try {
						worker.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    				    	
			    	gestioneTitoliResponse = new FiananceDataResponse(STATUS.OK);
			    } else {
			    	gestioneTitoliResponse = new FiananceDataResponse(STATUS.NO_RESPONSE);
			    }
			} finally {
			    response.close();
			}
			
			
		} catch (ClientProtocolException e) {
			throw new ApplicationException("", e);
		} catch (IOException e) {
			throw new ApplicationException("", e);
		}
				
		return gestioneTitoliResponse;
	}
	
	
	private void readHttpEntityIntoQueue(HttpEntity httpEntity, BlockingQueue<Optional<String>> queue) throws ApplicationException {
		
		try {
			InputStream input = httpEntity.getContent();
			BufferedReader in
			   = new BufferedReader(new InputStreamReader(input));
			
			String line = null;
			
			while ((line = in.readLine()) !=null){
				queue.add(Optional.of(line));
			}
			
			// MARKER
			queue.add(Optional.<String>absent());
			
			in.close();
			
		} catch (IllegalStateException e) {
			throw new ApplicationException("Exception reading content", e);
		} catch (IOException e) {
			throw new ApplicationException("Exception reading content", e);
		}
		
	}
}
