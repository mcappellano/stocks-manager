package com.simplethings.gestioneserietitoli.ws.stocksinformation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerStocksInformation;
import com.simplethings.gestioneserietitoli.parser.StockInformationParser;
import com.simplethings.gestioneserietitoli.parser.StockInformationParser.CompanyInformation;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse.STATUS;

public class StocksInformationWsClient {
	
	private static Logger logger = LoggerFactory.getLogger(StocksInformationWsClient.class);
	
	private ResponseHandlerStocksInformation responseHandlerStocksInformation;
	
	public StocksInformationWsClient(
			ResponseHandlerStocksInformation responseHandlerStocksInformation) {
		this.responseHandlerStocksInformation = responseHandlerStocksInformation;
	}
	
	public StocksInformationResponse doWork() throws ApplicationException{
		StocksInformationResponse stockInformationResponse = null;
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		try {
			List<BasicNameValuePair> parameters= new ArrayList<BasicNameValuePair>();
			// parameters.add(new BasicNameValuePair("q", "select company.symbol, company.name from yahoo.finance.industry where id in (select industry.id from yahoo.finance.sectors)|unique(field=\"company.symbol\", hideRepeatCount=\"true\")"));
			parameters.add(new BasicNameValuePair("q", "select * from yahoo.finance.industry where id in (select industry.id from yahoo.finance.sectors)"));
			
			parameters.add(new BasicNameValuePair("env", "store://datatables.org/alltableswithkeys"));
			parameters.add(new BasicNameValuePair("format", "json"));
			
			String queryString = URLEncodedUtils.format(parameters, "UTF-8");
			queryString = queryString.replace("+", "%20");
			
			HttpGet httpGet = new HttpGet("https://query.yahooapis.com/v1/public/yql?" + queryString);
			
			System.out.println("" + httpGet.getURI());
			CloseableHttpResponse response;
			
			response = httpclient.execute(httpGet);
			
			// The underlying HTTP connection is still held by the response object
			// to allow the response content to be streamed directly from the network socket.
			// In order to ensure correct deallocation of system resources
			// the user MUST either fully consume the response content  or abort request
			// execution by calling CloseableHttpResponse#close().

			try {
				StatusLine statusLine = response.getStatusLine();
			    logger.info("STATUS_LINE: {}", statusLine);
			    
			    int statusCode = statusLine.getStatusCode();
			    
			    logger.info("statusCode: {}", statusCode);
			    
			    if(statusCode == 200){
			    	HttpEntity entity = response.getEntity();
			    	BlockingQueue<Optional<CompanyInformation>> queueIndustryInformation = new LinkedBlockingQueue<Optional<CompanyInformation>>();
			    	
			    	Thread tdProcessor = responseHandlerStocksInformation.doStartQueueProcessor(queueIndustryInformation);
			    	
			    	readHttpEntityIntoQueue(entity, queueIndustryInformation);
			    	
			    	try {
						tdProcessor.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    	
			    	stockInformationResponse = new StocksInformationResponse(STATUS.OK);
			    } else {
			    	stockInformationResponse = new StocksInformationResponse(STATUS.NO_RESPONSE);
			    }
			} finally {
			    response.close();
			}
			
			
		} catch (ClientProtocolException e) {
			throw new ApplicationException("", e);
		} catch (IOException e) {
			throw new ApplicationException("", e);
		}
		
		return stockInformationResponse;
	}

	StockInformationParser parser = new StockInformationParser();
	
	public BlockingQueue<Optional<CompanyInformation>> readHttpEntityIntoQueue(HttpEntity httpEntity, BlockingQueue<Optional<CompanyInformation>> queue) throws ApplicationException {
		StockInformationParser parser = new StockInformationParser();
		
		try {
			InputStream input = httpEntity.getContent();
			parser.doWork(input, queue);

		} catch (IllegalStateException e) {
			throw new ApplicationException("Exception reading content", e);
		} catch (IOException e) {
			throw new ApplicationException("Exception reading content", e);
		}
		
		return queue;
	}

}
