package com.simplethings.gestioneserietitoli.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {
	
	public static <T> List<T> doReverse(List<T> descList){
		List<T> ascList = new ArrayList<T>();
		int cursor = descList.size();
		while(cursor > 0){
			cursor--;
			T elem = descList.remove(cursor);
			ascList.add(elem);
		}
		
		return ascList;
	}

}
