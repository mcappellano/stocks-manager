package com.simplethings.gestioneserietitoli.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateUtils {
	
	public static final String SIMPLE_DATE_PATTERN = "dd/MM/yyyy";
	public static final String FEED_DATE_PATTERN = "yyyy-MM-dd";
	
	public static Calendar buildCalendarDate(String datePattern, String valueDate) throws ParseException {
		Date dateResult = buildDate(datePattern, valueDate);
		Calendar date = Calendar.getInstance();		
		date.setTime(dateResult);
		return date;
	}
	
	public static Date buildDate(String datePattern, String valueDate)throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(datePattern, Locale.ITALIAN);
		
		Date dateResult = formatter.parse(valueDate);
		return dateResult;
	}
	
	public static Date buildDate(int dayOfMonth, int month, int year){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, year);
		
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	public static List<Date> buildDateCampioniOfMonth(int month, int year, List<Integer> listOfDays){
		List<Date> listOfDate = new ArrayList<Date>();
		for(Integer dayOfMonth: listOfDays){
			Date valueDate = DateUtils.buildDate(dayOfMonth, month, year);
			listOfDate.add(valueDate);
		}
		
		return listOfDate;
	}
	
	public static Date getStartMonthDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.set(Calendar.DAY_OF_MONTH, 1);
		
		return cal.getTime();
	}
	
	public static Calendar[] getMonthRange(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return getMonthRange(cal);
	}
	
	public static Calendar[] getMonthRange(Calendar cal){
		
		Calendar startDayOfMonth = Calendar.getInstance();
		startDayOfMonth.set(Calendar.MILLISECOND, 0);
		startDayOfMonth.set(Calendar.HOUR_OF_DAY, 0);
		startDayOfMonth.set(Calendar.MINUTE, 0);
		startDayOfMonth.set(Calendar.SECOND, 0);
		
		startDayOfMonth.set(Calendar.DAY_OF_MONTH, 1);
		startDayOfMonth.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		startDayOfMonth.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		
		Calendar endDayOfMonth = Calendar.getInstance();
		endDayOfMonth.set(Calendar.MILLISECOND, 0);
		endDayOfMonth.set(Calendar.HOUR_OF_DAY, 0);
		endDayOfMonth.set(Calendar.MINUTE, 0);
		endDayOfMonth.set(Calendar.SECOND, 0);
		
		endDayOfMonth.set(Calendar.DAY_OF_MONTH, 1);
		endDayOfMonth.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		endDayOfMonth.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		endDayOfMonth.add(Calendar.MONTH, 1);
		endDayOfMonth.add(Calendar.DAY_OF_MONTH, -1);
		
		Calendar[] arrCal =  {startDayOfMonth, endDayOfMonth};
		return arrCal;
	}
	
	/*
	 * Compare if two dates are in the same month and year.
	 * 
	 *  Es: 
	 *  	2013-12-23 and 2013-12-25 return true
	 *  	2013-12-23 and 2013-11-25 or 2014-12-25 return false
	 * 
	 */
	
	public static boolean compareDateInMonth(Date dateA, Date dateB){
		Calendar cal_1 = Calendar.getInstance();
		cal_1.setTime(dateA);
		
		Calendar cal_2 = Calendar.getInstance();
		cal_2.setTime(dateB);
		
		int monthDateA = cal_1.get(Calendar.MONTH);
		int yearDateA = cal_1.get(Calendar.YEAR);
		
		int monthDateB = cal_2.get(Calendar.MONTH);
		int yearDateB = cal_2.get(Calendar.YEAR);
		
		if( yearDateA == yearDateB 
				&& monthDateA == monthDateB)
			return true;
		
		return false;
	}
	
	public static String doFormat(Date date){
		String PATTERN = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(PATTERN, Locale.ITALIAN);
		return formatter.format(date);
	}
	
	public static String doFormat(Calendar dateCal){
		if(dateCal==null)
			return  "NULL";
		
		String PATTERN = "yyyy-MM-dd";
		SimpleDateFormat formatter = new SimpleDateFormat(PATTERN, Locale.ITALIAN);
		return formatter.format(dateCal.getTime());
	}
}
