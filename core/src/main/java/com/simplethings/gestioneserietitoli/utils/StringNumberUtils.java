package com.simplethings.gestioneserietitoli.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class StringNumberUtils {
	
	public static String buildSeriesOfDay(List<Integer> seriesDay){
		String accumulator = "";
		
		for(Integer value: seriesDay){
			String partValue = String.format("%02d", value);
			accumulator += partValue;
		}
		
		return accumulator;
	}
	
	public static List<Integer> unserializeSeriesOfDay(String valueSeriesOfDay) 
			throws IllegalArgumentException {
		
		List<Integer> listOfDays = new ArrayList<Integer>();
		
		if(valueSeriesOfDay==null || valueSeriesOfDay.equals(""))
			throw new IllegalArgumentException("Invalid value");
		
		if(valueSeriesOfDay.length() % 2 != 0)
			throw new IllegalArgumentException("Invalid format series of days");
		
		for(int i = 0; i < valueSeriesOfDay.length(); i+=2){
			String tock = valueSeriesOfDay.substring(i, i+2);
			Integer intValue = null;
			try {
				intValue = Integer.parseInt(tock);
				listOfDays.add(intValue);
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Invalid format series of days");
			}
		}
		
		return listOfDays;
	}
	
	public static String buildSeparatedSeriesOfDouble(List<Double> series, char separator){
		String accumulator = "";
		
		boolean isFirst = true;
		for(Double value: series){
			if(isFirst){
				isFirst = false;
				accumulator += formatDoubleValue(value);
			} else {
				accumulator = accumulator + separator + formatDoubleValue(value);
			}
		}
		
		return accumulator;
	}

	public static String formatDoubleValue(double value){
		return String.format(Locale.US, "%.2f", value);
	}
	
	public static String buildSeparatedSeriesOfLong(List<Long> series, char separator){
		String accumulator = "";
		
		boolean isFirst = true;
		for(Long value: series){
			if(isFirst){
				isFirst = false;
				accumulator += value;
			} else {
				accumulator = accumulator + separator + value;
			}
		}
		
		return accumulator;
	}
	
	public static List<Double> unserializeSepareatedSeriesOfDouble(String valueSeriesOfDouble, char separator) throws IllegalArgumentException {
		List<Double> listOfDouble = new ArrayList<Double>();
		
		StringTokenizer st = new StringTokenizer(valueSeriesOfDouble, "" + separator);
		while(st.hasMoreTokens()){
			String tok = st.nextToken();
			Double value = null;
			try {
				value = Double.parseDouble(tok);
			} catch (NumberFormatException e){
				throw new IllegalArgumentException("Invalid value: " + tok + " of: " + valueSeriesOfDouble);
			}
			listOfDouble.add(value);
		}
		
		return listOfDouble;
	}
	
	public static List<Long> unserializeSepareatedSeriesOfLong(String valueSeriesOfLong, char separator) throws IllegalArgumentException {
		List<Long> listOfLong = new ArrayList<Long>();
		
		StringTokenizer st = new StringTokenizer(valueSeriesOfLong, "" + separator);
		while(st.hasMoreTokens()){
			String tok = st.nextToken();
			Long value = null;
			try {
				value = Long.parseLong(tok);
			} catch (NumberFormatException e){
				throw new IllegalArgumentException("Invalid value: " + tok + " of: " + valueSeriesOfLong);
			}
			listOfLong.add(value);
		}
		
		return listOfLong;
	}
	
}
