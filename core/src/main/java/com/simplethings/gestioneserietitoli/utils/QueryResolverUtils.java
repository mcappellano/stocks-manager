package com.simplethings.gestioneserietitoli.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.simplethings.gestioneserietitoli.model.FinanceData;

public class QueryResolverUtils {
	
	public static List<FinanceData> filterFromDate(List<FinanceData> listFinanceData, Date fromDate){
		List<FinanceData> returnListFinanceDate = new ArrayList<FinanceData>();
		
		Iterator<FinanceData> iFD = listFinanceData.iterator();
		while(iFD.hasNext()){
			FinanceData financeData = iFD.next();
			iFD.remove();
			
			if(financeData.getDate().compareTo(fromDate) >=0){
						returnListFinanceDate.add(financeData);
					}
		}
		return returnListFinanceDate;
	}
	
	public static List<FinanceData> filterToDate(List<FinanceData> listFinanceData, Date toDate){
		List<FinanceData> returnListFinanceDate = new ArrayList<FinanceData>();
		
		Iterator<FinanceData> iFD = listFinanceData.iterator();
		while(iFD.hasNext()){
			FinanceData financeData = iFD.next();
			iFD.remove();
			
			if(financeData.getDate().compareTo(toDate) <=0){
				returnListFinanceDate.add(financeData);
			}
		}
		return returnListFinanceDate;
	}

	public static List<FinanceData> filterIntervall(List<FinanceData> listFinanceData, Date fromDate, Date toDate){
		List<FinanceData> returnListFinanceDate = new ArrayList<FinanceData>();
		
		Iterator<FinanceData> iFD = listFinanceData.iterator();
		while(iFD.hasNext()){
			FinanceData financeData = iFD.next();
			iFD.remove();
			
			if(financeData.getDate().compareTo(fromDate) >=0 && 
					financeData.getDate().compareTo(toDate) <=0)
				returnListFinanceDate.add(financeData);
			
		}
		return returnListFinanceDate;
	}
}
