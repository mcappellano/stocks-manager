package com.simplethings.gestioneserietitoli.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;

public class StockMonthSeriesFilter {
	
	private static Logger logger = 
			LoggerFactory.getLogger(StockMonthSeriesFilter.class);
	
	/**
	 * 
	 * @param stockMonthSeries
	 * @param fromDate
	 * @param toDate
	 * @return null if stockMonthSeries should be empty
	 */
	public static StockMonthSeries doFilterStockMonthSeries(
			StockMonthSeries stockMonthSeries, Date fromDate, Date toDate) {

		StockMonthSeries stockMonthSeriesFiltered = null;

		if (fromDate == null && toDate == null)
			return stockMonthSeries;

		stockMonthSeriesFiltered = new StockMonthSeries(stockMonthSeries.stockName);
		
		Set<FinanceData> financeDataSet = stockMonthSeries.getFinanceDataSet();

		Iterator<FinanceData> iterFinanceData = financeDataSet.iterator();
		boolean first = true;
		boolean doCheck = false;

		while (iterFinanceData.hasNext()) {
			FinanceData financeData = iterFinanceData.next();

			if (first) {
				first = false;
				Date date = financeData.getDate();
				
				if(fromDate!= null && isInSomeMonth(date, fromDate))
					doCheck = true;
				
				if(toDate!= null && isInSomeMonth(date, toDate))
					doCheck = true;
			}

			if (doCheck
					&& fromDate != null && toDate != null) {
					
				if ((financeData.getDate().after(fromDate) || financeData.getDate()
						.equals(fromDate)) && (financeData.getDate().before(toDate) || financeData.getDate()
								.equals(toDate))){
					stockMonthSeriesFiltered.addFinanceData(financeData);
				}
				
			} else if (doCheck
					&& (fromDate != null && (financeData.getDate().after(fromDate) || financeData.getDate()
							.equals(fromDate)))) {
				stockMonthSeriesFiltered.addFinanceData(financeData);
			} else if (doCheck
					&& (toDate != null && (financeData.getDate().before(toDate) || financeData.getDate()
							.equals(toDate)))) {
				stockMonthSeriesFiltered.addFinanceData(financeData);
			} else if(!doCheck){
				// not doCheck
				stockMonthSeriesFiltered = stockMonthSeries;
				break;
			}

		}
		
		if(stockMonthSeriesFiltered.getFinanceDataSet().isEmpty())
			stockMonthSeriesFiltered = null;
		
		return stockMonthSeriesFiltered;
	}
	
	public static boolean isInSomeMonth(Date dateFirst, Date dateSecond){
		Calendar calDateFirst = Calendar.getInstance();
		calDateFirst.setTime(dateFirst);
		
		Calendar calDateSecond = Calendar.getInstance();
		calDateSecond.setTime(dateSecond);
		
		if(calDateFirst.get(Calendar.MONTH)
				== calDateSecond.get(Calendar.MONTH) &&
						calDateFirst.get(Calendar.YEAR) 
						== calDateSecond.get(Calendar.YEAR))
				return true;
		
		return false;
	}

}
