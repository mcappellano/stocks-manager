package com.simplethings.gestioneserietitoli.logic;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;

public class WorkSplitter {
	
	private static Logger logger = LoggerFactory.getLogger(WorkSplitter.class);
	
	private final StockSeriesDAO stockSeriesDAO;
	private final StockInformationDAO stockInformationDAO;
	
	public WorkSplitter(StockSeriesDAO stockSeriesDAO,
			StockInformationDAO stockInformationDAO) {
		this.stockSeriesDAO = stockSeriesDAO;
		this.stockInformationDAO = stockInformationDAO;
	}
	
	private class Splitter implements Runnable {
		
		private final BlockingQueue<Optional<Stock>> queue;
		
		public Splitter(BlockingQueue<Optional<Stock>> queue) {
			this.queue = queue;
		}


		@Override
		public void run() {
			
			List<Stock> listOfStock = new LinkedList<Stock>();

			ExecutorService executor = Executors.newFixedThreadPool(10);
			      
			while(true){
				
				try {
					Optional<Stock> opStock = queue.take();
					
					if(!opStock.isPresent())
						break;
					
					if(listOfStock.size() == 1000){
						executor.execute(new Worker(listOfStock));
						listOfStock = new LinkedList<Stock>();
						
					} 
					
					listOfStock.add(opStock.get());
				
				} catch (InterruptedException e) {
					logger.warn("InteruptedException: ", e);
				}
				
				
				
			}
			
			if(listOfStock.size() > 0){
				executor.execute(new Worker(listOfStock));
			}
			
			executor.shutdown();
			while(!executor.isTerminated()){
				try {
					executor.awaitTermination(100,TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					logger.warn("InteruptedException: ", e);
				}
			}
		}
		
	}
	
	private class Worker implements Runnable {
		
		private final List<Stock> listOfStock;

		public Worker(List<Stock> listOfStock) {
			this.listOfStock = listOfStock;
		}



		@Override
		public void run() {
			
			FinanceDataWsClient wsClient;
			try {
				wsClient = new FinanceDataWsClient(new ResponseHandlerFinancialData(stockSeriesDAO));
				FinanceDataSincronizerController dataSincronizer = new FinanceDataSincronizerController(
						stockSeriesDAO, wsClient);

				dataSincronizer.doWork(listOfStock);
				
			} catch (ApplicationException e) {
				logger.warn("InteruptedException: ", e);
			}
			
			
		}
		
		
	}
	
	public void doMassiveLoad() throws ApplicationException, SQLException{
		
		BlockingQueue<Optional<Stock>> queue = new LinkedBlockingQueue<Optional<Stock>>();
		
		Splitter splitter = new Splitter(queue);
		Thread tdSplitter = new Thread(splitter);
		
		tdSplitter.start();
				
		stockInformationDAO.getListOfActiveStock(queue);
		
		try {
			tdSplitter.join();
			logger.debug("AFTER JOIN SPLITTER!!!");
		} catch (InterruptedException e) {
			logger.warn("InteruptedException: ", e);
		}
		
		
	}
}
