package com.simplethings.gestioneserietitoli.logic;

import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;

public class ResponseHandlerFinancialData implements ResponseHandler {
	
	private static Logger logger = LoggerFactory.getLogger(ResponseHandlerFinancialData.class);
	
	private final StockSeriesDAO stockMonthSeriesDAO;
	
	public ResponseHandlerFinancialData(StockSeriesDAO stockMonthSeriesDAO) {
		this.stockMonthSeriesDAO = stockMonthSeriesDAO;
	}

	public class Worker implements Runnable {
		
		private String stockName;
		private final BlockingQueue<Optional<String>> queue;
		
		public Worker(String stockName, BlockingQueue<Optional<String>> queue){
			this.stockName = stockName;
			this.queue = queue;
		}
		
		public void run(){
			FinanceDataLoader dataLoaderProcessor = new FinanceDataLoader(stockName, stockMonthSeriesDAO, queue);
			dataLoaderProcessor.doProcess();
		}
	}

	public Thread doStartQueueProcessor(String stockName, final BlockingQueue<Optional<String>> queue) throws ApplicationException {
		Thread tdWorker = new Thread(new Worker(stockName, queue));
		tdWorker.start();
		return tdWorker;
	}
	
		
	
	
}
