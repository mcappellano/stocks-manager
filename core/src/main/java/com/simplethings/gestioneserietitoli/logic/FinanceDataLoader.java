package com.simplethings.gestioneserietitoli.logic;

import static com.simplethings.gestioneserietitoli.parser.FeedParser.doParseLine;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.FinanceDataMonthGroup;
import com.simplethings.gestioneserietitoli.model.FinanceDataYearGroup;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.utils.DateUtils;
import com.simplethings.gestioneserietitoli.utils.ListUtils;

public class FinanceDataLoader {
	
	private static Logger logger = LoggerFactory
			.getLogger(FinanceDataLoader.class);

	private String stockName;
	private StockSeriesDAO stockMonthSeriesDAO;
	private BlockingQueue<Optional<String>> queueInput;
	
	private BlockingQueue<Optional<FinanceData>> queueFinanceData = 
			new LinkedBlockingQueue<Optional<FinanceData>>();
	
	private BlockingQueue<FinanceDataYearGroup> queueYearFinanceData =
			new LinkedBlockingQueue<FinanceDataYearGroup>();
	
	private BlockingQueue<FinanceDataMonthGroup> queueMonthFinanceData =
			new LinkedBlockingQueue<FinanceDataMonthGroup>();
	

	public FinanceDataLoader(String stockName, StockSeriesDAO stockMonthSeriesDAO, 
			BlockingQueue<Optional<String>> queue) {
		this.stockName = stockName;
		this.stockMonthSeriesDAO = stockMonthSeriesDAO;
		this.queueInput = queue;
	}

	private class Dequeuer implements Runnable {

		private boolean active = true;

		@Override
		public void run() {

			boolean isFirstLine = true;

			while (active) {

				String line = null;

				Optional<String> val;
				try {
					val = queueInput.take();

					if (val.isPresent()) {
						line = val.get();
						if (isFirstLine) {
							isFirstLine = false;
						} else {
							FinanceData finData = doParseLine(line);
							queueFinanceData.put(Optional.of(finData));
						}
					} else {
						active = false;
					}

				} catch (InterruptedException e) {
					logger.warn("InterruptedException: {}", e);
				}

			}

			try {
				queueFinanceData.put(Optional.<FinanceData>absent());
			} catch (InterruptedException e) {
				logger.warn("InterruptedException: {} puntting MARK_NULL end financeData", e);
			}


		}

	}
	
	/*
	 * Splitta e raggruppa in base all'anno.
	 * 
	 */
	
	private class YearSplitter implements Runnable{
		
		private boolean active = true;
		
		@Override
		public void run() {
			
			Calendar cal = Calendar.getInstance();
			int currentYear = -1;
			
			FinanceDataYearGroup currentYearFinanceData = null;
			
			while(active){
				
				Optional<FinanceData> val = null;
				
				try {
					val = queueFinanceData.take();
					
					if (val.isPresent()){
						
						FinanceData finData = val.get();
						
						cal.setTime(finData.getDate());
						
						int year = cal.get(Calendar.YEAR);
						
						if( year != currentYear){
							
							if(currentYear!=-1){
								queueYearFinanceData.put(currentYearFinanceData);
							}
							
							currentYear = year;
							currentYearFinanceData = new FinanceDataYearGroup(year);
							
						}
						currentYearFinanceData.addFinanceData(finData);
					} else {
						active = false;
					}
					
				} catch (InterruptedException e) {
					logger.warn("InterruptedException: {}", e);
				}
				
			}
			
			if(currentYearFinanceData != null) {
				try {
					queueYearFinanceData.put(currentYearFinanceData);
				} catch (InterruptedException e) {
					logger.warn("InterruptedException: {}", e);
				}
			}
		}
		
		
		
	}
	
	/*
	 * Splitta e raggruppa in base al mese.
	 * 
	 */
	
	private class MonthSplitter implements Runnable{
		
		private volatile boolean active = true;
		
		public void setStoppedProducer(){
			active = false;
		}
		
		@Override
		public void run() {
			
			Calendar cal = Calendar.getInstance();
			
			while(active || queueYearFinanceData.peek() != null){
				
				FinanceDataYearGroup financeDataYearGroupItem = null;
				
				try {
					financeDataYearGroupItem = queueYearFinanceData.poll(200L, TimeUnit.MILLISECONDS);
					
					if(financeDataYearGroupItem ==  null)
						continue;
										
					int year = financeDataYearGroupItem.getYear();
					
					List<FinanceData> yearListFinanceData = financeDataYearGroupItem.getYearFinaceData();
					
					int currentMonth = -1;
					
					FinanceDataMonthGroup currentMonthFinanceDate = null;
					
					for(FinanceData finDate: yearListFinanceData){
						
						cal.setTime(finDate.getDate());
						
						int month = cal.get(Calendar.MONTH);
											
						if(month != currentMonth){
							
							if(currentMonth!= -1){
								queueMonthFinanceData.put(currentMonthFinanceDate);
							}
						
							currentMonth = month;
							currentMonthFinanceDate = new FinanceDataMonthGroup(year, currentMonth);
						}
						
						currentMonthFinanceDate.addFinanceData(finDate);
					}
												
					queueMonthFinanceData.put(currentMonthFinanceDate);
					
				} catch (InterruptedException e) {
					logger.warn("InterruptedException: {}", e);
				}
				
			}

		}
		
		
		
	}
	
	
	private class Aggregator implements Runnable {
		
		private volatile boolean active = true;
		
		private long processedRecord;
		
		public void setStoppedProducer(){
			active = false;
		}
		
		public synchronized long getProcessedRecord(){
			return processedRecord;
		}
		
		@Override
		public void run(){
			
			while(active || queueMonthFinanceData.peek() != null){
				
				try {
					FinanceDataMonthGroup financeDataMonthGroupItem = 
							queueMonthFinanceData.poll(200L, TimeUnit.MILLISECONDS);
					
					if(financeDataMonthGroupItem == null){
						continue;
					}
					
					synchronized(this){
						processedRecord++;
					}
					
					List<FinanceData> listFinanceData = 
							financeDataMonthGroupItem.getMonthFinaceData();
					
					// do reverse list
					List<FinanceData> ascListFinanceData = ListUtils.doReverse(listFinanceData);
					
					StockMonthSeries stockMonthSeries = new StockMonthSeries(stockName);
					
					for(FinanceData item: ascListFinanceData){
						stockMonthSeries.addFinanceData(item);
					}
					
					ascListFinanceData.clear();
					
					Date endDate = stockMonthSeries.convertToStrockMonthSeriesRow().endDate;
					Calendar[] arrCal = DateUtils.getMonthRange(endDate);
					
					try {
						List<StockMonthSeries> listStockMonthSeries = 
								stockMonthSeriesDAO.loadIntervall(stockName, 
										arrCal[0].getTime(), arrCal[1].getTime());
					
						if(listStockMonthSeries.size()==1){
							
							StockMonthSeries savedStockMonthSeries = 
									listStockMonthSeries.get(0);
							
							logger.debug("FOUND: {}", savedStockMonthSeries);
							logger.debug("AGGREGATING WITH: {}", financeDataMonthGroupItem);
							stockMonthSeries = savedStockMonthSeries.merge(stockMonthSeries);
							stockMonthSeriesDAO.update(stockMonthSeries);
						} else {
							stockMonthSeriesDAO.save(stockMonthSeries);
						}
					} catch (SQLException e){
						logger.error("SQLException: {}", e);
					}
				} catch (InterruptedException e) {
					logger.warn("InterruptedException: {}", e);
				}
				
				
			}
			
		}
		
	}
	
	
	public long doProcess(){
		
		long recordProcessed = 0;
		
		Aggregator aggregator = new Aggregator();
		Thread tdAggregator = new Thread(aggregator);
		tdAggregator.start();
		
		MonthSplitter monthSplitter = new MonthSplitter();
		Thread tdMonthSplitter = new Thread(monthSplitter);
		tdMonthSplitter.start();
		
		YearSplitter yearSplitter = new YearSplitter();
		Thread tdYearSplitter = new Thread(yearSplitter);
		tdYearSplitter.start();
		
		Dequeuer dequeuer = new Dequeuer();
		Thread tdDequeuer = new Thread(dequeuer);
		tdDequeuer.start();
		
		try {
			tdDequeuer.join();
			logger.debug("************** JOINED DEQUEUE");
			
			tdYearSplitter.join();
			logger.debug("************** JOINED YEAR_SPLITTER");
			
			// mandare la interrupt a tdMonthSplitter
			monthSplitter.setStoppedProducer();
			tdMonthSplitter.join();
			logger.debug("************** JOINED MONTH_SPLITTER");
			
			// mandare la interrupt a tdAggregator
			aggregator.setStoppedProducer();
			tdAggregator.join();
			recordProcessed = aggregator.getProcessedRecord();
			logger.debug("************** JOINED AGGREGATOR");
						
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return recordProcessed;
		
	}
	
	
	

}
