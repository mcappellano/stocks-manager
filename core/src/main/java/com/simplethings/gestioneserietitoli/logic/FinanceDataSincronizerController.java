package com.simplethings.gestioneserietitoli.logic;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.ws.WsClientResponse;
import com.simplethings.gestioneserietitoli.ws.financedata.FiananceDataResponse;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataRequest;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;

public class FinanceDataSincronizerController {

	private static Logger logger = LoggerFactory
			.getLogger(FinanceDataSincronizerController.class);

	private StockSeriesDAO stockSeriesDAO;
	private FinanceDataWsClient wsClient;
	protected Timer timer = new Timer();

	public FinanceDataSincronizerController(
			StockSeriesDAO stockSeriesDAO,
			FinanceDataWsClient wsClient) {
		this.stockSeriesDAO = stockSeriesDAO;
		this.wsClient = wsClient;
	}
	
	public void laodingDataForNewStock(Stock stock, Calendar nowCal) throws ApplicationException {
		
		String stockName = stock.getStockName();
				
		try {

			FinanceDataRequest gestioneTitoliRequest = doGestioneTitoliRequest(
					stockName, nowCal);
			
			if (gestioneTitoliRequest != null) {
				FiananceDataResponse gestioneTitoliResponse = wsClient
						.doRequest(gestioneTitoliRequest);
			} else {
				logger.info("{} is already sinchronized", stock);
			}

		} catch (SQLException e) {
			throw new ApplicationException("", e);
		}
	}

	public void laodingDataForNewStock(Stock stock) throws ApplicationException {
		
		Calendar nowCal = timer.getTime();
		
		laodingDataForNewStock(stock, nowCal);

	}

	public void deleteStock(Stock stock) throws ApplicationException {

	}
	
	
	public void doWork(List<Stock> listOfStock) throws ApplicationException {

		Calendar nowCal = timer.getTime();

		Iterator<Stock> iterStock = listOfStock.iterator();

		while (iterStock.hasNext()) {
			Stock stock = iterStock.next();

			laodingDataForNewStock(stock, nowCal);

			iterStock.remove();

		}

	}

	/**
	 * 
	 * @param stockName
	 * @return null if request non necessary, yet done for the day
	 * @throws SQLException
	 */

	public FinanceDataRequest doGestioneTitoliRequest(String stockName,
			Calendar calNow) throws SQLException {

		StockMonthSeries lastStockMonthSeries = stockSeriesDAO
				.loadLastStockMonthSeries(stockName);

		FinanceDataRequest gestioneTitoliRequest = null;

		if (lastStockMonthSeries != null) {
			// costruire la request
			// da last date + 1 a ora
			Date endDate = lastStockMonthSeries.convertToStrockMonthSeriesRow().endDate;
			Date nextEndDate = getNextDayDate(endDate);

			if (checkIfTheSameDate(calNow.getTime(), endDate)) {
				// in the same day
				return null;
			}

			if (checkIfTheSameDate(calNow.getTime(), nextEndDate)) {
				// from now
				gestioneTitoliRequest = new FinanceDataRequest(stockName,
						calNow, null, FinanceDataRequest.TradingPeriod.DAILY);

			} else {
				// range request
				Calendar calNextEndDate = Calendar.getInstance();
				calNextEndDate.setTime(nextEndDate);

				gestioneTitoliRequest = new FinanceDataRequest(stockName,
						calNextEndDate, calNow,
						FinanceDataRequest.TradingPeriod.DAILY);
			}

		} else {
			// request fino a ora
			gestioneTitoliRequest = new FinanceDataRequest(stockName, null,
					calNow, FinanceDataRequest.TradingPeriod.DAILY);
		}

		return gestioneTitoliRequest;
	}

	private Date getNextDayDate(Date date) {
		long dateLong = date.getTime();
		Date nextDate = new Date();
		nextDate.setTime(dateLong + 1000 * 60 * 60 * 24);
		return nextDate;
	}

	private boolean checkIfTheSameDate(Date date_1, Date date_2) {
		Calendar cal_1 = Calendar.getInstance();
		Calendar cal_2 = Calendar.getInstance();
		cal_1.setTime(date_1);
		cal_2.setTime(date_2);
		if (cal_1.get(Calendar.DAY_OF_YEAR) == cal_2.get(Calendar.DAY_OF_YEAR)
				&& cal_1.get(Calendar.YEAR) == cal_2.get(Calendar.YEAR)) {
			return true;
		}
		return false;

	}
}
