package com.simplethings.gestioneserietitoli.logic;

public class ApplicationException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ApplicationException(String message, Exception cause){
		super(message, cause);
	}

}
