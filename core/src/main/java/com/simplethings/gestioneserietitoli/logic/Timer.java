package com.simplethings.gestioneserietitoli.logic;

import java.util.Calendar;

public class Timer {

	public Calendar getTime(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		return 	cal;	
	}
}
