package com.simplethings.gestioneserietitoli.logic;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.simplethings.gestioneserietitoli.model.Stock;

public class StockLifeManager {
	
	private static StockLifeManager stockLifeManager = null;
	
	private Worker worker;
		
	private BlockingQueue<Stock> queue;
	
	private class Worker implements Runnable {
		
		private volatile boolean active = false;
		
		private Thread executionThread;
		
		public Thread getExecutionThread(){
			return executionThread;
		}

		public void setStopping(){
			active = false;
		}
		
		@Override
		public void run() {
			active = true;
			executionThread = Thread.currentThread();
			
			while(active){
				// dequeuer and processing
				
			}
		}
		
		
	}
	
	public StockLifeManager getStockLifeManager(){
		if(stockLifeManager==null){
			stockLifeManager = new StockLifeManager();
		}
		
		return stockLifeManager;
	}
	
	private StockLifeManager(){
		queue = new LinkedBlockingQueue<Stock>();
		
		worker = new Worker();
		
		Thread tdWorker = new Thread(worker);
		tdWorker.start();
		
	}
	
	public void activateStock(){
		
		// enqueuer
		
	}
	
	public void deactivateStock(){
		
		// enqueuer
		
	}
	
	public void shutdown(){
		
		worker.setStopping();
		
		try {
			Thread workThead = worker.getExecutionThread();
			
			if(workThead!= null)
				workThead.join();
		} catch (InterruptedException e) {
			
		}
		
	}
}
