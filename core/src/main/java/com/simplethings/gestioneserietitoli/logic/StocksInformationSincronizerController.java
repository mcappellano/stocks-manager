package com.simplethings.gestioneserietitoli.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.ws.stocksinformation.StocksInformationResponse;
import com.simplethings.gestioneserietitoli.ws.stocksinformation.StocksInformationWsClient;

/**
 * Class for loading Stock information on DB.
 * 
 * @author CAM
 *
 */

public class StocksInformationSincronizerController {
	
	private static Logger logger = LoggerFactory.getLogger(StocksInformationSincronizerController.class);
	
	private final StocksInformationWsClient client;
	
	public StocksInformationSincronizerController(StocksInformationWsClient client){
		this.client = client;
	}
	
	public void doWork(){
		
		try {

			StocksInformationResponse response = client.doWork();
						
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
