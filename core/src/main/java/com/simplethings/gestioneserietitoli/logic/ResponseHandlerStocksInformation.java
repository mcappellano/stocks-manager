package com.simplethings.gestioneserietitoli.logic;

import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.model.Stock;

import com.simplethings.gestioneserietitoli.parser.StockInformationParser.CompanyInformation;

public class ResponseHandlerStocksInformation implements ResponseHandler {
	
	private static Logger logger = LoggerFactory.getLogger(ResponseHandlerStocksInformation.class);
	
	private final StockInformationDAO stockInformationDAO;
		
	public ResponseHandlerStocksInformation(
			StockInformationDAO stockInformationDAO) {
		this.stockInformationDAO = stockInformationDAO;
	}

	private class StoreInDB implements Runnable{
		
		private BlockingQueue<Optional<CompanyInformation>> queue;
		
		public StoreInDB(BlockingQueue<Optional<CompanyInformation>> queue){
			this.queue = queue;
		}
		
		@Override
		public void run() {
			long counter = 0;
			CompanyInformation companyInformation = null;
			while(true){
				
				try {
					// logger.debug(">>>>> BEFORE TAKE");
					Optional<CompanyInformation> optionalCompanyInformation = queue.take();
					// logger.debug("<<<<< AFTER TAKE");
					
					if(!optionalCompanyInformation.isPresent())
						break;
					
					companyInformation = optionalCompanyInformation.get();
					// logger.debug("<<<<< AFTER TAKE2");
					stockInformationDAO.createStock(new Stock(companyInformation.symbol, companyInformation.name));
					// logger.debug("*** -> " + counter++);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					logger.error("SQLException {} ON {}", e.getMessage(), companyInformation);

				}
				
			}
			
		}
		
		
	}
	
	
	
	public Thread doStartQueueProcessor(
			BlockingQueue<Optional<CompanyInformation>> queueIndustryInformation) {
		Thread td = new Thread(new StoreInDB(queueIndustryInformation));
		td.start();
		return td;
	}
	
}
