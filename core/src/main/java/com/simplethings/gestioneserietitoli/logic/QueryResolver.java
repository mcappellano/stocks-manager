package com.simplethings.gestioneserietitoli.logic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.utils.QueryResolverUtils;

public class QueryResolver {
	
	private final StockSeriesDAO stockMonthSeriesDAO;
	
	public QueryResolver(StockSeriesDAO stockMonthSeriesDAO){
		this.stockMonthSeriesDAO = stockMonthSeriesDAO;
	}
	
	public List<FinanceData> load(String ticket) throws ApplicationException {
		
		List<FinanceData> resultFinanceDate = new ArrayList<FinanceData>();
		
		try {
			List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.load(ticket);
			
			for(StockMonthSeries stockMonthSeries: listStockMonthSeries){
				List<FinanceData> monthFinanceDate = 
						new ArrayList<FinanceData>(stockMonthSeries.getFinanceDataSet());
				resultFinanceDate.addAll(monthFinanceDate);
			}
						
		}  catch (SQLException e) {
			throw new ApplicationException("Error loading data for stock: " + ticket, e);
		}
       		
		return resultFinanceDate;
	}
	
	public List<FinanceData> loadToDate(String ticket, Date toDate) throws ApplicationException {
		
		List<FinanceData> resultFinanceDate = new ArrayList<FinanceData>();
		
		try {
			List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.loadToDate(ticket, toDate);
			
			for(StockMonthSeries stockMonthSeries: listStockMonthSeries){
				List<FinanceData> monthFinanceDate = 
						new ArrayList<FinanceData>(stockMonthSeries.getFinanceDataSet());
				resultFinanceDate.addAll(monthFinanceDate);
			}
						
		}  catch (SQLException e) {
			throw new ApplicationException("Error loading data for stock: " + ticket + 
					", to date: " + toDate, e);
		}
       		
		return QueryResolverUtils.filterToDate(resultFinanceDate, toDate);
	}
	
	public List<FinanceData> loadFromDate(String ticket, Date fromDate)	throws ApplicationException {
		
		List<FinanceData> resultFinanceDate = new ArrayList<FinanceData>();
		
		try {
			List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.loadFromDate(ticket, fromDate);
			
			for(StockMonthSeries stockMonthSeries: listStockMonthSeries){
				List<FinanceData> monthFinanceDate = 
						new ArrayList<FinanceData>(stockMonthSeries.getFinanceDataSet());
				resultFinanceDate.addAll(monthFinanceDate);
			}
						
		}  catch (SQLException e) {
			throw new ApplicationException("Error loading data for stock: " + ticket + 
					", from date: " + fromDate, e);
		}
       		
		return QueryResolverUtils.filterFromDate(resultFinanceDate, fromDate);
	}
	
	public List<FinanceData> loadIntervall(String ticket, Date fromDate, Date toDate) throws ApplicationException {
		
		List<FinanceData> resultFinanceDate = new ArrayList<FinanceData>();
		
		try {
			List<StockMonthSeries> listStockMonthSeries = stockMonthSeriesDAO.loadIntervall(ticket, fromDate, toDate);
			
			for(StockMonthSeries stockMonthSeries: listStockMonthSeries){
				List<FinanceData> monthFinanceDate = 
						new ArrayList<FinanceData>(stockMonthSeries.getFinanceDataSet());
				resultFinanceDate.addAll(monthFinanceDate);
			}
						
		} catch (SQLException e) {
			throw new ApplicationException("Error loading data for stock: " + ticket + 
					"in range from: " + fromDate + " to: " + toDate, e);
		}
       		
		return QueryResolverUtils.filterIntervall(resultFinanceDate, fromDate, toDate);
	}
	
	
}
