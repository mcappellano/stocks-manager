package com.simplethings.gestioneserietitoli.conf;

import java.util.Map;

import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class DbConfig {

	public final String dbDriverClassName;
	public final String dbUrl;
	public final String dbUsername;
	public final String dbPassword;

	private DbConfig(String dbDriverClassName, String dbUrl, String dbUsername,
			String dbPassword) {
		super();
		this.dbDriverClassName = dbDriverClassName;
		this.dbUrl = dbUrl;
		this.dbUsername = dbUsername;
		this.dbPassword = dbPassword;
	}

	public static synchronized DbConfig getConfig(String configFilePath)
			throws ApplicationException {

		Map<String, Map<String, String>> prop = ConfigYamlLoader.loadConfig(configFilePath);

		Map<String, String> dbConfig = prop.get("db");

		if (dbConfig == null)
			throw new ApplicationException("Db config not found",
					new IllegalArgumentException("Db config not found"));

		String driverClassName = dbConfig.get("driverClassName");
		String dbUrl = dbConfig.get("dbUrl");
		String username = dbConfig.get("username");
		if (username == null)
			username = "";

		String password = dbConfig.get("password");
		if (password == null)
			password = "";

		DbConfig config = new DbConfig(driverClassName, dbUrl, username,
				password);

		return config;
	}

	

}
