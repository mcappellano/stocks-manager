package com.simplethings.gestioneserietitoli.conf;

import java.util.HashMap;
import java.util.Map;

import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class Config {

	private static Map<String, Config> configHolder = 
			new HashMap<String, Config>();
	
	public final String dataSourceUrl;
	
	private Config(String dataSourceUrl) {
		this.dataSourceUrl = dataSourceUrl;
	}

	public static synchronized Config getConfig(String configFilePath) throws ApplicationException {
		
		Config config = configHolder.get(configFilePath);
		if(config==null){
			Map<String, Map<String, String>> prop = ConfigYamlLoader.loadConfig(configFilePath);
			
			Map<String, String> dataSourceConfig = prop.get("dataSource");
			
			if(dataSourceConfig == null)
				throw new ApplicationException("DataSource config not found", 
						new IllegalArgumentException("DataSource config not found"));
			
			String dataSourceUrl = dataSourceConfig.get("url");
						
			config =  new Config(dataSourceUrl);
			configHolder.put(configFilePath, config);
		}
		
		return config;
	}
		
}
