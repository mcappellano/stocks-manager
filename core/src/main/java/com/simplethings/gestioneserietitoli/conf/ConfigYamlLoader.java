package com.simplethings.gestioneserietitoli.conf;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import com.simplethings.gestioneserietitoli.logic.ApplicationException;

public class ConfigYamlLoader {
	
	public static Map<String, Map<String, String>> loadConfig(
			String configFilePath) throws ApplicationException {
		Map<String, Map<String, String>> map = null;

		try {
			InputStream inputStream = Config.class
					.getResourceAsStream(configFilePath);

			if (inputStream == null) {
				throw new ApplicationException("", new FileNotFoundException(
						"property file '" + configFilePath
								+ "' not found in the classpath"));
			}

			Yaml yaml = new Yaml();

			map = (Map) yaml.load(inputStream);

		} catch (Exception e) {
			throw new ApplicationException("Error loading config file: "
					+ configFilePath, e);
		}

		return map;
	}

}
