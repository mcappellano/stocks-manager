create table IF NOT EXISTS  STOCKS_INFORMATION ( 
	id bigint PRIMARY KEY auto_increment,
	stock_name varchar(20) UNIQUE NOT NULL,
	stock_descr varchar(40) NOT NULL,
	active boolean default false);
	
create table IF NOT EXISTS STOCK_SERIES (
	id bigint PRIMARY KEY auto_increment, 
	id_stock bigint NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	day_series varchar(62),
	open_value_series varchar(500),
	height_value_series varchar(500),
	low_value_series varchar(500),
	close_value_series varchar(500),
	volume_value_series varchar(500),
	adj_close_value_series varchar(500),
       	FOREIGN KEY(id_stock) REFERENCES STOCKS_INFORMATION(id),
   		CONSTRAINT uData UNIQUE (id_stock, start_date, end_date)
);






