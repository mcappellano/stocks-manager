package com.simplethings.gestioneserietitoli.web;

import static com.simplethings.gestioneserietitoli.web.utils.DataFormatter.PATTERN_JSON;
import static com.simplethings.gestioneserietitoli.web.utils.DataFormatter.doFormat;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.FinanceDataSincronizerController;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerFinancialData;
import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;

public class ActivateStockServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(ActivateStockServlet.class);
	
	private StockInformationDAO stockInformationDAO = null;
	private FinanceDataSincronizerController dataSincronizer = null;
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				stockInformationDAO = 
						new StockInformationDAO(applicationDataSourceContainer.getDataSource());
				
				StockSeriesDAO stockSeriesDAO = 
						new StockSeriesDAO(applicationDataSourceContainer.getDataSource());
				
				FinanceDataWsClient wsClient = new FinanceDataWsClient(new ResponseHandlerFinancialData(stockSeriesDAO));
				dataSincronizer = new FinanceDataSincronizerController(stockSeriesDAO, wsClient);
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
    	
    	String pathInfo = request.getPathInfo();
		
		String stockName = null;
		
		if(pathInfo.startsWith("/")){
			stockName = pathInfo.substring(1);
		}
		
		response.setContentType("text/plain");
		Writer out = response.getWriter();
		try {
			
			boolean result = stockInformationDAO.activateStock(stockName);
			
			if(result){
				Stock stock = stockInformationDAO.searchByStockName(stockName);
				if(stock!=null)
					dataSincronizer.laodingDataForNewStock(stock);
			}
			
			logger.debug("END AFTER SLEEP {}", pathInfo);
			out.write("OK");
		} catch (Exception e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
    }

	public void doWork(OutputStream out, List<StockMonthSeries> listStockMonthSeries, String stockName) 
		throws ApplicationException {
		try {

			JsonFactory jfactory = new JsonFactory();

			JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
			jGenerator.useDefaultPrettyPrinter();
			
			jGenerator.writeStartObject(); // {

			jGenerator.writeStringField("stock_name", stockName); // "name" : "mkyong"
			// jGenerator.writeNumberField("age", 29); // "age" : 29

			jGenerator.writeFieldName("values"); // "values" :
			jGenerator.writeStartArray(); // [

			
			
			for (StockMonthSeries stockMonthSeries : listStockMonthSeries) {
				Set<FinanceData> financeDataSet = stockMonthSeries
						.getFinanceDataSet();
				Iterator<FinanceData> iterFinanceData = financeDataSet
						.iterator();
				while (iterFinanceData.hasNext()) {
					FinanceData fd = iterFinanceData.next();
					
					jGenerator.writeStartObject(); // {
					
					jGenerator.writeStringField("date",  doFormat(fd.getDate(), PATTERN_JSON));
					jGenerator.writeNumberField("open_value", fd.getOpenValue());
					jGenerator.writeNumberField("hight_value", fd.getHightValue());
					jGenerator.writeNumberField("close_value", fd.getCloseValue());
					jGenerator.writeNumberField("volume", fd.volume);
					jGenerator.writeNumberField("adj_close_value", fd.adjCloseValue);
					
					jGenerator.writeEndObject(); // }
					}
			}
						
			jGenerator.writeEndArray(); // ]

			jGenerator.writeEndObject(); // }

			jGenerator.close();

		} catch (JsonGenerationException e) {
			throw new ApplicationException("Error generating json response", e);
		} catch (JsonMappingException e) {
			throw new ApplicationException("Error generating json response", e);
		} catch (IOException e) {
			throw new ApplicationException("Error generating json response", e);
		}

	}

}
