package com.simplethings.gestioneserietitoli.web;

import static com.simplethings.gestioneserietitoli.web.utils.DataFormatter.PATTERN_JSON2;
import static com.simplethings.gestioneserietitoli.web.utils.DataFormatter.doFormat;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.FinanceData;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;
import com.simplethings.gestioneserietitoli.web.utils.StockDataPathInformationParser;

public class JSONServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(JSONServlet.class);

	private StockSeriesDAO stockSeriesDAO = null;
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				
				stockSeriesDAO = 
						new StockSeriesDAO(applicationDataSourceContainer.getDataSource());
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}
    
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String pathInfo = request.getPathInfo();
		
		String stockName = null;
		Date fromDate = null;
		Date toDate = null;
		
		response.setContentType("application/json");
		
		if(pathInfo == null){
			// return 400 status code: Bad Request
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		} else {
			logger.debug("PathInfo: {}", pathInfo);
			StockDataPathInformationParser sdp = null;
			
			try {
				sdp = new StockDataPathInformationParser(pathInfo);
			} catch (ParseException e) {
				// return 400 status code: Bad Request
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
		
			stockName = sdp.getStockCode();
			// ricava fromDate e toData
			
			fromDate = sdp.getFromDate();
			toDate = sdp.getToDate();
			
		}
		
		OutputStream out = response.getOutputStream();
		try {
			List<StockMonthSeries> listStockMonthSeries = null;
			
			if(fromDate!=null && toDate!=null){
				listStockMonthSeries = stockSeriesDAO.loadIntervall(stockName, fromDate, toDate);
			} else if(fromDate!=null) {
				listStockMonthSeries = stockSeriesDAO.loadFromDate(stockName, fromDate);
			} else if(toDate!=null) {
				listStockMonthSeries = stockSeriesDAO.loadToDate(stockName, toDate);
			} else {
				listStockMonthSeries = stockSeriesDAO.load(stockName);
			}
			
			doWork(out, listStockMonthSeries, stockName);

		} catch (ApplicationException e) {
			logger.error("Application exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		} catch (SQLException e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
		
	}

	public void doWork(OutputStream out, List<StockMonthSeries> listStockMonthSeries, String stockName) 
		throws ApplicationException {
		try {

			JsonFactory jfactory = new JsonFactory();

			JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
			jGenerator.useDefaultPrettyPrinter();
			
			jGenerator.writeStartObject(); // {

			jGenerator.writeStringField("stock_name", stockName); // "name" : "mkyong"
			// jGenerator.writeNumberField("age", 29); // "age" : 29

			jGenerator.writeFieldName("values"); // "values" :
			jGenerator.writeStartArray(); // [

			for (StockMonthSeries stockMonthSeries : listStockMonthSeries) {
				Set<FinanceData> financeDataSet = stockMonthSeries
						.getFinanceDataSet();
				Iterator<FinanceData> iterFinanceData = financeDataSet
						.iterator();
				
				while (iterFinanceData.hasNext()) {
					FinanceData fd = iterFinanceData.next();
					
					jGenerator.writeStartObject(); // {
					// date
					jGenerator.writeStringField("d",  doFormat(fd.getDate(), PATTERN_JSON2));
					// open value
					jGenerator.writeNumberField("ov", fd.getOpenValue());
					// hight value
					jGenerator.writeNumberField("hv", fd.getHightValue());
					// close_value
					jGenerator.writeNumberField("cv", fd.getCloseValue());
					// volume
					jGenerator.writeNumberField("v", fd.getVolume());
					// adjust value
					jGenerator.writeNumberField("av", fd.getAdjCloseValue());
					
					/*
					jGenerator.writeStringField("date",  doFormat(fd.getDate(), PATTERN_JSON));
					jGenerator.writeNumberField("open_value", fd.getOpenValue());
					jGenerator.writeNumberField("hight_value", fd.getHightValue());
					jGenerator.writeNumberField("close_value", fd.getCloseValue());
					jGenerator.writeNumberField("volume", fd.getVolume());
					jGenerator.writeNumberField("adj_close_value", fd.getAdjCloseValue());
					*/
					jGenerator.writeEndObject(); // }
					}
			}
						
			jGenerator.writeEndArray(); // ]

			jGenerator.writeEndObject(); // }

			jGenerator.close();

		} catch (JsonGenerationException e) {
			throw new ApplicationException("Error generating json response", e);
		} catch (JsonMappingException e) {
			throw new ApplicationException("Error generating json response", e);
		} catch (IOException e) {
			throw new ApplicationException("Error generating json response", e);
		}

	}

}
