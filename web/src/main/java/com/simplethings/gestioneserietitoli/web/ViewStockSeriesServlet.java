package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;
import com.simplethings.gestioneserietitoli.model.StockMonthSeries;

import static com.simplethings.gestioneserietitoli.web.utils.DataFormatter.doParse;

public class ViewStockSeriesServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(ViewStockSeriesServlet.class);
	
	private StockSeriesDAO stockSeriesDAO = null;
	private StockInformationDAO stockInformationDAO = null;
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				
				stockSeriesDAO = 
						new StockSeriesDAO(applicationDataSourceContainer.getDataSource());
				
				stockInformationDAO = 
						new StockInformationDAO(applicationDataSourceContainer.getDataSource());
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
						
			// ?startDate=&endDate=&stockName
			String stockNameValue = request.getParameter("stockName");
			String startDateValue = request.getParameter("fromDate");
			String endDateValue = request.getParameter("toDate");
			
			List<Stock> listOfActiveStock = stockInformationDAO.getListOfActiveStock();
			request.setAttribute("listOfActiveStock", listOfActiveStock);
			
			if(stockNameValue==null){
				request.getRequestDispatcher("/view/indexSelectFromActiveStock.ftl").forward(request, response);
				return;
			}
			
			Date fromDate = doParse(startDateValue, "yyyy-MM-dd");
			Date toDate = doParse(endDateValue, "yyyy-MM-dd");
			
			Stock stockData = stockInformationDAO.searchByStockName(stockNameValue);
			
			if(stockData == null || !stockData.isActive()){
				request.setAttribute("stockName", stockNameValue);
				// se non attiva
				if(stockData!=null)
					request.setAttribute("stockData", stockData);
				
				request.getRequestDispatcher("/view/indexNoStock.ftl").forward(request, response);
			} else {
				List<StockMonthSeries> listStockMonthSeries = null;
				
				if(fromDate!=null && toDate!=null){
					listStockMonthSeries = stockSeriesDAO.loadIntervall(stockNameValue, fromDate, toDate);
				} else if(fromDate!=null) {
					listStockMonthSeries = stockSeriesDAO.loadFromDate(stockNameValue, fromDate);
				} else if(toDate!=null) {
					listStockMonthSeries = stockSeriesDAO.loadToDate(stockNameValue, toDate);
				} else {
					listStockMonthSeries = stockSeriesDAO.load(stockNameValue);
				}
				
				request.setAttribute("stockData", stockData);
				
				if(fromDate != null) request.setAttribute("fromDate", fromDate);
				if(toDate != null) request.setAttribute("toDate", toDate);
				
				request.setAttribute("stockSeries", listStockMonthSeries);
				request.getRequestDispatcher("/view/index.ftl").forward(request, response);
			}
		} catch (SQLException e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
		
	}

	

}
