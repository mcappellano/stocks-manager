package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerStocksInformation;
import com.simplethings.gestioneserietitoli.logic.StocksInformationSincronizerController;
import com.simplethings.gestioneserietitoli.ws.stocksinformation.StocksInformationWsClient;

public class LoadStocksInformationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory
			.getLogger(LoadStocksInformationServlet.class);

	private StockInformationDAO stockInformationDAO = null;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			Config configApp = Config.getConfig("/config.properties");
			WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
					new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
			
			stockInformationDAO = 
					new StockInformationDAO(applicationDataSourceContainer.getDataSource());

		} catch (ApplicationException e) {
			throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
		}

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/json");
		PrintWriter writer = response.getWriter();
		long start = System.currentTimeMillis();

		ResponseHandlerStocksInformation responseHandlerStocksInformation = new ResponseHandlerStocksInformation(
				stockInformationDAO);

		StocksInformationWsClient stocksInformationWsClient = new StocksInformationWsClient(
				responseHandlerStocksInformation);

		StocksInformationSincronizerController workerController = new StocksInformationSincronizerController(
				stocksInformationWsClient);

		workerController.doWork();

		long endTime = System.currentTimeMillis();

		writer.println("WORK DONE LOAD STOCK_INFORMATION !! in: "
				+ (endTime - start) / 1000.0);

	}

}
