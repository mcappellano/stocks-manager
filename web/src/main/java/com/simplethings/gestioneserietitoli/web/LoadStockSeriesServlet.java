package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.WorkSplitter;

public class LoadStockSeriesServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(LoadStockSeriesServlet.class);
	
	private StockSeriesDAO stockSeriesDAO = null;
	private StockInformationDAO stockInformationDAO = null;
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				
				stockSeriesDAO = 
						new StockSeriesDAO(applicationDataSourceContainer.getDataSource());
				
				stockInformationDAO = 
						new StockInformationDAO(applicationDataSourceContainer.getDataSource());
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("plain/text");
		PrintWriter writer = response.getWriter();
		try {
			long start = System.currentTimeMillis();
			
			WorkSplitter workSplitter = new WorkSplitter(stockSeriesDAO,
					stockInformationDAO);
			workSplitter.doMassiveLoad();
			
			long endTime = System.currentTimeMillis();
			
			writer.println("WORK DONE LOAD STOCK_SERIES!! in: " + (endTime - start)/1000.0);

		} catch (ApplicationException e) {
			logger.error("Application exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		} catch (SQLException e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
		
	}

	

}
