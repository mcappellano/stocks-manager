package com.simplethings.gestioneserietitoli.web.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DataFormatter {
	
	private static String PATTERN = "dd-MM-yyyy";
	public static String PATTERN_JSON = "yyyy-MM-dd";
	public static String PATTERN_JSON2 = "yyyyMMdd";
	
	public static String doFormat(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat(PATTERN, Locale.ITALIAN);
		return formatter.format(date);
	}
	
	public static String doFormat(Date date, String pattern){
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.ITALIAN);
		return formatter.format(date);
	}
	
	public static String doFormat(double value){
		return String.format(Locale.ITALIAN, "%.2f", value);
	}

	
	public static String doFormatLong(long value){
		return String.format(Locale.ITALIAN, "%,d", value);
	}
	
	/*
	 * @param dateValue
	 * return null if error in parsing 'dateValue'
	 */
	public static Date doParse(String dateValue, String pattern){
		if(dateValue==null || dateValue.equals(""))
			return null;
		
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.ITALIAN);
		
		try {
			date = formatter.parse(dateValue);
		} catch (ParseException e) {}
		
		return date;
	}
}
