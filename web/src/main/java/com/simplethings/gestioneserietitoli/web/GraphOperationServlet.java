package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;

public class GraphOperationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(GraphOperationServlet.class);
	
	private StockSeriesDAO stockSeriesDAO = null;
	private StockInformationDAO stockInformationDAO = null;
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				
				stockSeriesDAO = 
						new StockSeriesDAO(applicationDataSourceContainer.getDataSource());
				
				stockInformationDAO = 
						new StockInformationDAO(applicationDataSourceContainer.getDataSource());
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Stock> listOfActiveStock;
		try {
			listOfActiveStock = stockInformationDAO.getListOfActiveStock();
			request.setAttribute("listOfActiveStock", listOfActiveStock);
			
			request.getRequestDispatcher("/view/graphPage.ftl").forward(request, response);
			
		} catch (SQLException e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
		
		
	}

	

}
