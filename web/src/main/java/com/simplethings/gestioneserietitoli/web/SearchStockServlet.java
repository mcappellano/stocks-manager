package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;

public class SearchStockServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(SearchStockServlet.class);
	
	private StockInformationDAO stockInformationDAO = null;
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				
				stockInformationDAO = 
						new StockInformationDAO(applicationDataSourceContainer.getDataSource());
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String stockName = request.getParameter("term");
		
		response.setContentType("application/json");
		OutputStream out = response.getOutputStream();
		try {

			List<Stock> listStockMonthSeries = stockInformationDAO
					.searchByDescrOrStockCode(stockName);
			
			doWork(out, listStockMonthSeries, stockName);

		} catch (ApplicationException e) {
			logger.error("Application exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		} catch (SQLException e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
		
	}

	public void doWork(OutputStream out, List<Stock> listStockMonthSeries, String stockName) 
			throws ApplicationException {
		
		JsonFactory jfactory = new JsonFactory();

		JsonGenerator jGenerator;
		try {
			jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
			jGenerator.useDefaultPrettyPrinter();
			
			jGenerator.writeStartArray();
						
			for(Stock stock: listStockMonthSeries){
				jGenerator.writeStartObject(); // {
				jGenerator.writeStringField("id",  stock.getStockName());
				jGenerator.writeStringField("value", stock.getStockDescription());
				jGenerator.writeEndObject(); // }
			}
					
			jGenerator.writeEndArray();
			
			jGenerator.close();
			
		} catch (JsonGenerationException e) {
			throw new ApplicationException("Error generating json response", e);
		} catch (JsonMappingException e) {
			throw new ApplicationException("Error generating json response", e);
		} catch (IOException e) {
			throw new ApplicationException("Error generating json response", e);
		}
		
		
		
	}
		
}
