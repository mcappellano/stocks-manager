package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminOperationsMenuServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(AdminOperationsMenuServlet.class);
	
    public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		   		    
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.getRequestDispatcher("/view/admin/adminOperationsPage.ftl").forward(request, response);
				
	}

	

}
