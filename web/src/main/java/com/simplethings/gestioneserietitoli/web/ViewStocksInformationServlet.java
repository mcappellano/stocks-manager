package com.simplethings.gestioneserietitoli.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.model.Stock;

public class ViewStocksInformationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(ViewStocksInformationServlet.class);

	private StockInformationDAO stockInformationDAO = null;
	
	public void init(ServletConfig config) throws ServletException {
		    super.init(config);
		    try {
				Config configApp = Config.getConfig("/config.properties");
				WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
						new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);
				
				stockInformationDAO = 
						new StockInformationDAO(applicationDataSourceContainer.getDataSource());
				
			} catch (ApplicationException e) {
				throw new ServletException("INIT EXCEPTION: " + e.getMessage(), e);
			}
		    
	}
	  
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			int LIMIT_SIZE = 1000;
			
			int currentPageNumber = 1;
			
			String valPageNumber = request.getParameter("pageNumber");
			
			if(valPageNumber!= null){
				try {
					currentPageNumber = Integer.parseInt(valPageNumber);
				} catch (NumberFormatException e){
					// trash data
				}
			}
			
			int OFFSET = (currentPageNumber -1) * LIMIT_SIZE;
			
			int numberAllStocks = stockInformationDAO.getCountNumberOfAllStock();
			
			int totalPageNumber = 0;
			
			if(numberAllStocks!=0){
				totalPageNumber = numberAllStocks/LIMIT_SIZE;
			    if( (totalPageNumber % LIMIT_SIZE) != 0)
			    	totalPageNumber++;
			}
			
			List<Stock> listOfStock = stockInformationDAO.getListOfAllStock(LIMIT_SIZE, OFFSET);
			
			request.setAttribute("currentPageNumber", currentPageNumber);
			request.setAttribute("totalPageNumber", totalPageNumber);
			request.setAttribute("stocksInformationList", listOfStock);
			request.getRequestDispatcher("/view/admin/indexStocksInformation.ftl").forward(request, response);
			
		} catch (SQLException e) {
			logger.error("SQLException exceprion: {} - cause: {}", e,
					e.getCause());
			throw new ServletException("", e);
		}
		
	}

	

}
