package com.simplethings.gestioneserietitoli.web.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.StringTokenizer;

public class StockDataPathInformationParser {

	private String stockCode;
	private Date fromDate;
	private Date toDate;
	
	public StockDataPathInformationParser(String pathInformation) throws ParseException{
				
		StringTokenizer st = new StringTokenizer(pathInformation, "/");
		int counter = 0;
	    while (st.hasMoreTokens()) {
	         String token = st.nextToken();

	         if(counter==0){
	        	 stockCode = token;
	         } else {
	        	 // FROM or TO
	        	 fillDate(token);
	         }
	         counter++;
	         
	         if(counter > 3)
	        	 throw new ParseException("Invalid number of tokens", 1);
	     }
	     
	    if(counter==0)
	    	throw new ParseException("Invalid number of tokens", 1);
	}

	public String getStockCode() {
		return stockCode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public Date getToDate() {
		return toDate;
	}
	
	private void fillDate(String token) throws ParseException{
		if(token.startsWith("F")){
			fromDate = getDateToken(token);
		} else if (token.startsWith("T")){
			toDate = getDateToken(token);
		} else 
			throw new ParseException("Invalid argument", 1);
	}
	
	private Date getDateToken(String token) throws ParseException{
		Date dateValue = null;
		try{
			String value = token.substring(1);
			dateValue = DataFormatter.doParse(value, DataFormatter.PATTERN_JSON2);
			if(dateValue==null)
				throw new ParseException("Invalid data format", 1);
			
		} catch(IndexOutOfBoundsException e){
			throw new ParseException("Invalid data format", 1);
		}
		
		
		return dateValue;
	}
}
