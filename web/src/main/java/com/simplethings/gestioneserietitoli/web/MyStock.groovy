package com.simplethings.gestioneserietitoli.web

import java.util.Date;

class MyStock {
	String dateValue
	double openValue
	double hightValue
	double lowValue
	double closeValue
	long volume
	double adjCloseValue
	
	MyStock(String dateValue, double openValue, double hightValue,
			double lowValue, double closeValue, long volume,
			double adjCloseValue){
			this.dateValue = dateValue
			this.openValue = openValue
			this.hightValue = hightValue
			this.lowValue = lowValue
			this.closeValue = closeValue
			this.volume = volume
			this.adjCloseValue = adjCloseValue
	}
	
}
