package com.simplethings.gestioneserietitoli.db;

import com.simplethings.gestioneserietitoli.logic.ApplicationException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class WebAppApplicationDataSourceContainer {

	private DataSource dataSource = null;
	
	public WebAppApplicationDataSourceContainer(String resourceBindingName) throws ApplicationException {
		Context ctx;
		try {
			ctx = new InitialContext();
			dataSource = (DataSource) ctx.lookup(resourceBindingName);
		} catch (NamingException e) {
			throw new ApplicationException("Failed to find: " + resourceBindingName, e);
		}
		 		 
	}
	
	public DataSource getDataSource(){
		return dataSource;
	}
	

}
