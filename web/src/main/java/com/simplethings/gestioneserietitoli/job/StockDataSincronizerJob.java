package com.simplethings.gestioneserietitoli.job;

import java.sql.SQLException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simplethings.gestioneserietitoli.conf.Config;
import com.simplethings.gestioneserietitoli.db.StockInformationDAO;
import com.simplethings.gestioneserietitoli.db.StockSeriesDAO;
import com.simplethings.gestioneserietitoli.db.WebAppApplicationDataSourceContainer;
import com.simplethings.gestioneserietitoli.logic.ApplicationException;
import com.simplethings.gestioneserietitoli.logic.FinanceDataSincronizerController;
import com.simplethings.gestioneserietitoli.logic.ResponseHandlerFinancialData;
import com.simplethings.gestioneserietitoli.ws.financedata.FinanceDataWsClient;

public class StockDataSincronizerJob implements Job {

	private static Logger logger = LoggerFactory
			.getLogger(StockDataSincronizerJob.class);

	public void execute(JobExecutionContext jec) throws JobExecutionException {
		logger.info("Job {} in execution", jec.getJobDetail().getDescription());

		try {
			Config config = Config.getConfig("/config.properties");
			
			WebAppApplicationDataSourceContainer applicationDataSourceContainer = 
					new WebAppApplicationDataSourceContainer(config.dataSourceUrl);
			
			StockSeriesDAO stockSeriesDAO = 
					new StockSeriesDAO(applicationDataSourceContainer.getDataSource());
			
			StockInformationDAO stockInformationDAO = 
					new StockInformationDAO(applicationDataSourceContainer.getDataSource());
			
			FinanceDataWsClient wsClient = new FinanceDataWsClient(new ResponseHandlerFinancialData(stockSeriesDAO));
			FinanceDataSincronizerController dataSincronizer = new FinanceDataSincronizerController(
					stockSeriesDAO, wsClient);

			dataSincronizer.doWork(stockInformationDAO.getListOfActiveStock());

		} catch (ApplicationException e) {
			logger.error("Error execution job, message: {}, cause: {}",
					e.getMessage(), e.getCause());
			throw new JobExecutionException("Error execution job, message: "
					+ e.getMessage(), e, false);
		} catch (SQLException e) {
			logger.error("Error execution job, message: {}, cause: {}",
					e.getMessage(), e.getCause());
			throw new JobExecutionException("Error execution job, message: "
					+ e.getMessage(), e, false);
		}

	}
}
