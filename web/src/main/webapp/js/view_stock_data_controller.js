/*
* Controller of stock data row.
* 
* Higth coupled with stockInformationRow.ftl
*
*/

   jQuery.noConflict();
		
   var URL_ATTIVAZIONE_SERVICE = "../ActivateStock/";
   var URL_DISATTIVAZIONE_SERVICE = "../DisactivateStock/";
   
   function changeLink(stockCode, stockDescription, isLink){
   	     // '.' char in stock code bacomes '_' char
	  var stockCodeIdLabel = stockCode.replace(/\./g, "_");
	  
	  var spanIdStock = jQuery('#stock-code-' + stockCodeIdLabel);
	  
	  var spanDescriptionStock = jQuery('#stock-description-' + stockCodeIdLabel);
	  
	  if(isLink) {
	  	spanIdStock.html('<a href="../index?stockName=' + stockCode +'" title="view stock data">' + 
						'<span style="margin-right: 10px">' + stockCode + '</span>' + 
						'</a>');
		spanDescriptionStock.html('<a href="../index?stockName=' + stockCode +'" title="view stock data">' + 
						'<span style="margin-right: 10px">' + stockDescription + '</span>' + 
						'</a>');				
	  } else {
	    spanIdStock.html('<span style="margin-right: 10px">' + stockCode + '</span>');
	    spanDescriptionStock.html('<span style="margin-right: 10px">' + stockDescription + '</span>');
	  }
   }
   
   function statusActivationManager(stockCode, stockDescription){
	  // '.' char in stock code bacomes '_' char
	  var stockCodeCssLabel = stockCode.replace(/\./g, "_");
	  
	  jQuery('#btn-' + stockCodeCssLabel).click(function () {
		var status = jQuery('#status-' + stockCodeCssLabel);
		// alert("" + status.val());
		
		// current status
		var statusAttivo;
		if(status.val() == "disattivo"){
	    	statusAttivo = false;
	    	status.val("attivo");
	    } else if( status.val() == "attivo"){
	    	statusAttivo = true;
	    	status.val("disattivo");
	    }
		
	  	var btn = jQuery(this);
	  	// ADDED
	  	btn.button();
	  	
	  	// chiamata ajax
	  	if(!statusAttivo){
	    	btn.button('activating');
	  		
	  		// chiamata attivazione
	  		jQuery.ajax(URL_ATTIVAZIONE_SERVICE + stockCode).always(function () {
	  			btn.button('disattivo');
	  	    	var label = jQuery('#label-' + stockCodeCssLabel);
	  	    	label.text("Attivo");
	  	        label.removeClass('label-default').addClass('label-success');
	  	        
	  	        changeLink(stockCode, stockDescription, true);
	   		});
	      	
	  	} else {
	  		btn.button('disactivating');
	  		
	  		// chiamata deattivazione
	  		jQuery.ajax(URL_DISATTIVAZIONE_SERVICE + stockCode).always(function () {
	  			btn.button('attivo');
	  	  		var label = jQuery('#label-' + stockCodeCssLabel);
	  	  		label.text("Disattivo");
	  	  	    label.removeClass('label-success').addClass('label-default');
	  	  	    
	  	  	    changeLink(stockCode, stockDescription, false);
	    });
	  		
	  		
	  	}
	  	
	    // changing hidden status
	    if(!statusAttivo){
	    	status.val("attivo");
	  	} else {
	  		status.val("disattivo");
	    }
	    // alert("AFTER: " + status.val());
	      
	  });
  }
  
