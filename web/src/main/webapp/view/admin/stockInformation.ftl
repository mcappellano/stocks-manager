<#import "../lib/utilsAdmin.ftl" as u> 
<#import "stockInformationRow.ftl" as r>

<@u.page title="Finance Data">
	
	<ul class="nav nav-tabs" role="tablist">
  		<li><a href="../index">Home</a></li>
  		<li><a href="../graph">Grafici</a></li>
  		<li class="active"><a href="../admin">Amministrazione</a></li>
	</ul>
	
	<div  class="banner">
		<h1>Stock Information</h1>
		<h2 class="hidden-small">* Management *</h2>
	</div>
	
	<#if stockInformation??>
 
	<!-- main content -->
	<table class="datatable">
	<tr>
		<th class="datatable-col"><span>CODE</span></th>
		<th class="datatable-col"><span>DESCRIPTION</span></th>
		<th class="datatable-col"><span>STATUS</span></th>
		<th class="datatable-col"><span>ACTION</span></th>
	</tr>
	
	<tr>
		<@r.row stockInformation=stockInformation/>
	</tr>
		

	</table>
	<!-- end main content -->
		
	<#else>
		No stock information found!!
	</#if>
	
	
	
</@u.page> 