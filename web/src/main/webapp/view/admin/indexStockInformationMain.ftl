<#import "stockInformationRow.ftl" as r>

<#macro table>

	<table class="datatable">
	<tr>
		<th class="datatable-col"><span>CODE</span></th>
		<th class="datatable-col"><span>DESCRIPTION</span></th>
		<th class="datatable-col"><span>STATUS</span></th>
		<th class="datatable-col"><span>ACTION</span></th>
	</tr>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    -->   
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    
    <#list stocksInformationList as stockInformation>
		<tr>
		    <@r.row stockInformation=stockInformation/>
		</tr>
	</#list>	

	</table>


</#macro>