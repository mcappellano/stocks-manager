<#macro row stockInformation>
<td>
		    <span id="stock-code-${stockInformation.stockName?replace('.','_')}">
				<#if stockInformation.isActive()>
					<a href="../index?stockName=${stockInformation.stockName}" title="view stock data">
						<span style="margin-right: 10px">${stockInformation.stockName}</span>
					</a>
				<#else>
					<span style="margin-right: 10px">${stockInformation.stockName}</span>
				</#if>
			</span>
			</td>
			<td>
			<span id="stock-description-${stockInformation.stockName?replace('.','_')}">
				<#if stockInformation.isActive()>
						<a href="../index?stockName=${stockInformation.stockName}" title="view stock data">
							<span style="margin-right: 10px">${stockInformation.stockDescription}</span>
						</a>
				<#else>
						<span style="margin-right: 10px">${stockInformation.stockDescription}</span>
				</#if>
			</span>
			</td>
			<#if stockInformation.isActive()>
			    <td>
			    	<input type="hidden" id="status-${stockInformation.stockName?replace('.','_')}" value="attivo">
			    	<span id="label-${stockInformation.stockName?replace('.','_')}" class="label label-success lbl-att-dis">Attivo</span>
			    </td>
				<td>
					<button type="button" id="btn-${stockInformation.stockName?replace('.','_')}" data-activating-text="ACTIVATING..." data-disactivating-text="DISACTIVATING..." data-disattivo-text="DISACTIVATE" data-attivo-text="ACTIVATE" class="btn btn-primary btn-att-dis">
  					DISACTIVATE
					</button>
				</td> 
   			<#else>
   				<td>
   					<input type="hidden" id="status-${stockInformation.stockName?replace('.','_')}" value="disattivo">
   					<span id="label-${stockInformation.stockName?replace('.','_')}" class="label label-default lbl-att-dis">Disattivo</span>
   				</td>
				<td>
					<button type="button" id="btn-${stockInformation.stockName?replace('.','_')}" data-activating-text="ACTIVATING..." data-disactivating-text="DISACTIVATING..." data-disattivo-text="DISACTIVATE" data-attivo-text="ACTIVATE" class="btn btn-primary btn-att-dis">
  					ACTIVATE
					</button>
				</td>
			</#if>
			<script>
				statusActivationManager("${stockInformation.stockName}", "${stockInformation.stockDescription}");
			</script>
</#macro>			