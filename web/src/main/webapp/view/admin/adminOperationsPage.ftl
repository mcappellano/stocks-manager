<#import "../lib/utilsAdmin.ftl" as u> 

<@u.page title="Finance Data">

<ul class="nav nav-tabs" role="tablist">
  <li><a href="../index">Home</a></li>
  <li><a href="../graph">Grafici</a></li>
  <li class="active"><a href="../admin">Amministrazione</a></li>
</ul>

	<div  class="banner">
		<h1>Admin</h1>
		<h2 class="hidden-small">* Operations *</h2>
	</div>
	
	<div align="center">
		<div align="left" style="width: 40%">
			<ul>
				<li><a href="../admin">Manage Stocks Information</a></li>
				<li><a href="./load-stock-series">LOAD STOCK SERIES</a></li>
				<li><a href="./load-stock-informations">LOAD STOCKS INFORMATION</a></li>
			</ul>
		</div>
  	</div>

</@u.page>
