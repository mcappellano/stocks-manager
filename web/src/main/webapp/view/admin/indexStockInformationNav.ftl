<#macro slider currentPageNumber totalPageNumber>

		<#if totalPageNumber!= 0>	
				<#if currentPageNumber < 7>
					<#assign startNum = 1>
					<#if totalPageNumber < 10>
						<#assign endNum = totalPageNumber>
					<#else> 
						<#assign endNum = 10>
					</#if>
				<#else>
				    <#if currentPageNumber < totalPageNumber - 5>
				    	<#assign startNum = currentPageNumber - 5 
				    		endNum = currentPageNumber + 5 >
				    <#else>
				    	<#assign startNum = totalPageNumber - 10 
				    		endNum = totalPageNumber >
				    </#if>
				</#if>
				
				<div style="margin-top: 40px; text-align: center;">
					<div class="btn-group">
					    <#if currentPageNumber != 1>
					    	<a class="btn btn-default" role="button" href="./?pageNumber=${currentPageNumber - 1}">&lt;&lt;</a>
					    </#if>
						<#list startNum..endNum as i>
						    <#if i == currentPageNumber >
								<a class="btn btn-primary active" role="button"  href="./?pageNumber=${i}">${i}</a>
							<#else>
					    		<a class="btn btn-default" role="button" href="./?pageNumber=${i}">${i}</a>
					    	</#if>
					    </#list>
					    <#if currentPageNumber != totalPageNumber>
					    	<a class="btn btn-default" role="button" href="./?pageNumber=${currentPageNumber + 1}">&gt;&gt;</a>
					    </#if>
					</div>
				</div>
			</#if>
	
	</#macro>
	