<#import "../lib/utilsAdmin.ftl" as u> 
<#import "./indexStockInformationNav.ftl" as p>  
<#import "./indexStockInformationMain.ftl" as m>  
<#import "./indexStockInformationSearch.ftl" as s> 


<@u.page title="Finance Data">
	
	<ul class="nav nav-tabs" role="tablist">
  		<li><a href="../index">Home</a></li>
  		<li><a href="../graph">Grafici</a></li>
  		<li class="active"><a href="../admin">Amministrazione</a></li>
	</ul>
	
	<div class="contenitore">
		<div  class="banner header">
			<h1>Stocks Data</h1>
			<h2 class="hidden-small">* Management *</h2>
		</div>
		<div style="position: absolute; top: 20px; right: 20px;" class="scroll">
			<@s.search />
			
			<script>
				jQuery.noConflict();
		
				jQuery(function($) {
					$(".scroll").jScroll({speed : "fast", top: 20});
				});
			</script>
			
		</div>
	<div>
	<div>
		<@m.table />
    	<@p.slider currentPageNumber=currentPageNumber totalPageNumber=totalPageNumber />
	</div>
	
	
</@u.page> 