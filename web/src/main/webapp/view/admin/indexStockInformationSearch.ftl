<#macro search>

		<div>
			<input id="search" type="text" size="32" title="type stock name or symbol" placeholder="Search by stock name or symbol"/><img src="../img/glyphicons_027_search.png" style=" margin-left: 0.5em"/>
		</div>
			
		<script>
		
		jQuery.noConflict();
		
		jQuery(function($) {
		 
		    $("#search").autocomplete({
		        source: "../SearchStock",
		        minLength: 2,
		        select: function(event, ui) {
		            $( "#search" ).val( ui.item.value + " / " + ui.item.id );
		            var idItem = ui.item.id;
		            if(idItem != '#') {
		                location.href = './view-stock-information?stockName=' + idItem;
		            }
					return false;
		        }
		        /*
		        ,
		 		
		        html: true, // optional ($.ui.autocomplete.html.js required)
		 
		      	// optional (if other layers overlap autocomplete list)
		        open: function(event, ui) {
		            $(".ui-autocomplete").css("z-index", 1000);
		        }
		        */
		    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
		    	return $( "<li>" )
					.append( "<a><strong>" + item.value + "</strong> / " + item.id  + "</a>" )
					.appendTo( ul );
			};
	 
		});
	
	</script>
	
	
</#macro>