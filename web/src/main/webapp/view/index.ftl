<#import "./lib/utils.ftl" as u> 
<#import "./indexSearchBox.ftl" as s> 

<@u.page title="Finance Data">

<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="./index">Home</a></li>
  <li><a href="./graph">Grafici</a></li>
  <li><a href="./admin">Amministrazione</a></li>
</ul>

	<div class="contenitore">
		<div  class="banner header">
			<h1>Finance Data</h1>
			<h2 class="hidden-small">* '${stockData.stockName} - ${stockData.stockDescription}' *</h2>
			<#if fromDate?? && toDate??>
				<h2>From: ${fromDate?date} - To: ${toDate?date}</h2>
			<#elseif fromDate??>
				<h2>From: ${fromDate?date}</h2>
			<#elseif toDate??>
				<h2>To: ${toDate?date}</h2>
			</#if>
		</div>
		<script src="./js/jquery.jscroll.min.js"></script>
		<div style="position: absolute; top: 20px; right: 20px; width: 360px;" class="scroll">
			<@s.searchForm stockData.stockName/>
			
			<script>
				jQuery.noConflict();
		
				jQuery(function($) {
					$(".scroll").jScroll({speed : "fast", top: 20});
				});
			</script>
			
		</div>
	<div>
	
  <table class="datatable"> 
    <tr>
		<th class="datatable-col"><span>DATA</span></th>
		<th class="datatable-col"><span>OPEN </span></th>
		<th class="datatable-col"><span>HIGHT VALUE</span></th>
		<th class="datatable-col"><span>CLOSE VALUE</span></th>
		<th class="datatable-col"><span>VOLUME</span></th>
		<th class="datatable-col"><span>ADJUST VALUE</span></th>
	</tr>
    <#list stockSeries as stockSerie>
    	<#list stockSerie.financeDataSet as financeData>
    	   <tr>
		        <td class="datatable-value"><span>${financeData.date?date}</span></td>
		        <td class="datatable-value"><span>${financeData.openValue}</span></td>
				<td class="datatable-value"><span>${financeData.hightValue}</span></td>
				<td class="datatable-value"><span>${financeData.closeValue}</span></td>
				<td class="datatable-value"><span>${financeData.volume}</span></td>
				<td class="datatable-value"><span>${financeData.adjCloseValue}</span></td>
		    </tr>
    	</#list>
    </#list>
  </table>
  
  
</@u.page>
