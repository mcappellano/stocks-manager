<#import "./lib/utils.ftl" as u> 
<#import "./indexSearchBox.ftl" as s> 

<@u.page title="Finance Data">

<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="./index">Home</a></li>
  <li><a href="./graph">Grafici</a></li>
  <li><a href="./admin">Amministrazione</a></li>
</ul>

    <div class="contenitore">
    
		<div  class="banner">
			<h1>Finance Data</h1>
			
			<#if stockData??>
				<h2 class="hidden-small">* '${stockName}' - '${stockData.stockDescription}' * not active!</h2>
			<#else>
				<h2 class="hidden-small">* '${stockName}' * not existing!</h2>
			</#if>
		</div>
	
		<script src="./js/jquery.jscroll.min.js"></script>
		<div style="position: absolute; top: 20px; right: 20px;" class="scroll">
				
			<#if stockData??>
				<@s.searchForm stockData.stockName/>
			<#else>
				<@s.searchForm "NOTHING"/>
			</#if>
			
			<script>
				jQuery.noConflict();
		
				jQuery(function($) {
					$(".scroll").jScroll({speed : "fast", top: 20});
				});
			</script>
			
		</div>
	<div>
	
</@u.page>