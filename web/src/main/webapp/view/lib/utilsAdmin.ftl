<#macro page title>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico"> 
	
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
	<script src="../js/jquery-ui-1.11.1/external/jquery/jquery.js"></script>
	<script src="../js/jquery-ui-1.11.1/jquery-ui.js"></script>
	<script src="../js/view_stock_data_controller.js"></script>
	 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  		
    <#-- This processes the enclosed content:  -->
    <#nested>
    
    <#include "./copyright.ftl"> 
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    -->   
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
  	<script src="../js/jquery.jscroll.min.js"></script>
  	 <script>
  	 	/*
    	var bootstrapButton = $.fn.button.noConflict() // return $.fn.button to previously assigned value
		$.fn.bootstrapBtn = bootstrapButton            // give $().bootstrapBtn the Bootstrap functionality
		*/
    </script>   
  </body>
  </html>
</#macro>