<#macro searchForm stockName>  
			    
				<form id="form-request-financial-data" action="." method="GET">
				
				<div class="dataform" style="text-align: left;">
				    <div class="input-group">
				    	<select name="stockName" class="form-control">
				    		<#list listOfActiveStock as stockItem>
				    		    <#if stockItem.stockName == stockName>
				    		    	<option value="${stockItem.stockName}" selected>'${stockItem.stockDescription}' - ${stockItem.stockName}</option>
				    		    <#else>
				    		    	<option value="${stockItem.stockName}">'${stockItem.stockDescription}' - ${stockItem.stockName}</option>
				    		    </#if>
				    		</#list>
				    	</select>
				    	<span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="icon glyphicon glyphicon-search" aria-hidden="true"></i></button></span>
			    	</div>
			    	
            		<div class="panel-group" id="accordion" style="margin-top: 1em">
					 <div class="panel panel-primary">
					  <div class="panel-heading">
					   <h4 class="panel-title">
					   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#pannello-1">MORE OPTIONS</a>
					   </h4>
					  </div>
					  <div id="pannello-1" class="panel-collapse collapse">
					  <div class="panel-body">
					  	<table class="searchbox">
				    		<tr><td>FROM DATE:</td><td><input type="TEXT" id="fromDate" name="fromDate" placeholder="yyyy-MM-dd" size="10"/></td></tr>
				    		<tr><td>TO DATE:</td><td><input type="TEXT" id="toDate" name="toDate" placeholder="yyyy-MM-dd" size="10"/></td></tr>
				    	</table>
					  </div>
					  </div>
					 </div>
					</div>

			   <!-- END DIV dataform -->	
				</div>
	    	
		</form>
		<script src="./js/jquery.validate.min.js"></script>
		<script src="./js/additional-methods.min.js"></script>
		<!--
		<script src="./js/localization/messages_it.min.js"></script>    
		-->		
		<script>
 		$( "#form-request-financial-data" ).validate({
			rules: {
				fromDate: {
					dateISO: true
					},
				toDate: {
					dateISO: true
					}
			}
		});
		</script>
		
</#macro>			
			




