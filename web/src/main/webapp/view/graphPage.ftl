<#import "./lib/utils.ftl" as u> 

<@u.page title="Finance Data">

<ul class="nav nav-tabs" role="tablist">
  <li><a href="./index">Home</a></li>
  <li  class="active"><a href="./graph">Grafici</a></li>
  <li><a href="./admin">Amministrazione</a></li>
</ul>

	<div  class="banner">
		<h1>Finance Data</h1>
		<h2 class="hidden-small">* View graph of stock's historical quotes *</h2>
	</div>

  	<script src="./js/jquery-ui-1.11.1/external/jquery/jquery.js"></script>
	<script src="./js/d3.min.js"></script>	
	<script>
    
    var data2;

	var format = d3.time.format("%Y%m%d");

	var parseDate = format.parse;

	function _init(stockName, valFromDate, valToDate){
		// fetch data from database
		//should have some sort of API for getting the data
		data2=new Array();
		
		if(valFromDate!=null){
			valFromDate = "/F" + valFromDate;		
		} else {
			valFromDate = "";
		}
		
		if(valToDate!= null){
			valToDate = "/T" + valToDate;
		} else {
			valToDate = "";
		}
		
		$.ajax({
		    url: "./JSON/" + stockName + valFromDate + valToDate,
			context: document.body,
			dataType: "json", 
			headers : {Accept : "application/json","Access-Control-Allow-Origin" : "*"},
			type: 'GET', 
			async: false,
			success: function(data, textStatus, jqXHR){
				
				for(var i=0; i < data.values.length; i++) {
					var fund=new Object();
				    
					fund.date = parseDate(data.values[i].d);
				
					fund.close_value = parseFloat(data.values[i].cv);
				
					fund.volume = data.values[i].v;
				
					data2[i] = fund;
				}
				
				draw(data2);
			},
			error: function(jqHXR, textStatus, errorThrown) {
				console.log('ajax error in get survey ID call:' +textStatus + ' ' + errorThrown);
			}
	
		 }); // end of the ajax call

	}

	function draw(data) {
		"use strict";
		
		// remove previous if any		
		d3.select("svg")
       		.remove();
       
		var margin = 60,
			width = 700 - margin,
			height = 500 - margin;
		
		var time_extent = d3.extent(
			data, function(d){return d.date}
			);
		
		var time_scale = d3.time.scale()
			.domain(time_extent)
			.range([margin, width]);
		
		var count_extent = d3.extent(
				data, function(d){return d.close_value}
				);
		
		var count_scale = d3.scale.linear()
				.domain(count_extent)
				.range([height, margin]);
		
		var x_axis = d3.svg.axis().scale(time_scale);
		
		var y_axis = d3.svg.axis().scale(count_scale).orient("left");
				
		d3.select("div.content_graph")
			.append("svg")
			.attr("width", width+margin)
			.attr("height", height+margin)
			.append("g")
			.attr("class","chart");
		
		/*
		d3.select("svg")
			.selectAll("circle.stocks")
			.data(data)
			.enter()
			.append("circle")
			.attr("class", "stocks");
		
		d3.selectAll("circle")
			.attr("cy", function(d){return count_scale(d.close_value);})
			.attr("cx", function(d){return time_scale(d.date);})
			.attr("r", 1);
		*/
		var line = d3.svg.line()
			.x(function(d){return time_scale(d.date)})
			.y(function(d){return count_scale(d.close_value)});
		
		d3.select("svg")
			.append("path")
			.attr("d", line(data))
			.attr("class", "stock_series");
		
		d3.select("svg")
			.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + (height + 5) + ")")
			.call(x_axis);
		
		d3.select("svg")
			.append("g")
			.attr("class", "y axis")
			.attr("transform", "translate(" + margin + ", " + (0 + 5) + ")")
			.call(y_axis);
		
		d3.select(".x.axis")
			.append("text")
			.text("time")
			.attr("x", (width / 2) - margin)
			.attr("y", margin / 1.5);
		
		d3.select(".y.axis")
			.append("text")
			.text("stock value")
			.attr("transform", "rotate (-90, -35, 0) translate(-320)");
	}

	</script>
		
	<div class="banner">
        <#if listOfActiveStock?size &gt;  0>
        <div class="container-fluid">
  			<div class="row">
    			<div class="col-md-4"></div>
    			
    			<div class="col-md-4">
    			    
    				<form id="form-request-financial-data" action="#" method="GET" >
    				
    				<div class="dataform" style="text-align: left;">
    				    <div class="input-group">
    				    	<select name="stockName" id="stockName" class="form-control">
					    		<#list listOfActiveStock as stockItem>
					    			<option value="${stockItem.stockName}">'${stockItem.stockDescription}' - ${stockItem.stockName}</option>
					    		</#list>
					    	</select>
					    	<span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="icon glyphicon glyphicon-search" aria-hidden="true"></i></button></span>
				    	</div>
				    	
                		<div class="panel-group" id="accordion" style="margin-top: 1em">
						 <div class="panel panel-primary">
						  <div class="panel-heading">
						   <h4 class="panel-title">
						   <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#pannello-1">MORE OPTIONS</a>
						   </h4>
						  </div>
						  <div id="pannello-1" class="panel-collapse collapse">
						  <div class="panel-body">
						  	<table class="searchbox">
					    		<tr><td>FROM DATE:</td><td><input type="TEXT" id="fromDate" name="fromDate" placeholder="yyyy-MM-dd" size="10"/></td></tr>
					    		<tr><td>TO DATE:</td><td><input type="TEXT" id="toDate" name="toDate" placeholder="yyyy-MM-dd" size="10"/></td></tr>
					    	</table>
						  </div>
						  </div>
						 </div>
						</div>

				   <!-- END DIV dataform -->	
					</div>
		    	
			</form>
    		<script src="./js/jquery.validate.min.js"></script>
			<script src="./js/additional-methods.min.js"></script>
			<!--
    		<script src="./js/localization/messages_it.min.js"></script>    
    		-->		
    		<script>
		    		jQuery.noConflict();
				
					jQuery(function($) {
					
		    			function doBuildGraph(){
			    			// check first if is valid
				    					    			
			    			var form = $( "#form-request-financial-data" );
							
							if(form.valid()){
			    				var fromDate = $("#fromDate").val();
			    				var toDate = $("#toDate").val();
					    		
					    		var valFromDate = null;
					    		var valToDate = null;
					    		
					    		if(fromDate!=null && fromDate.trim()!= ""){	
			    					valFromDate = fromDate.replace(/-/g, "");
			    				} 
			    				    
			    				if(toDate!=null && toDate.trim()!= ""){    
			    					valToDate = toDate.replace(/-/g, "");
			    				} 			    				
			    				
			    				var stockName = $("#stockName :selected").attr('value');
			    				_init(stockName, valFromDate, valToDate);
			    				
			    			}
				    			
		    			return false;
				    }
		    		
		     		$( "#form-request-financial-data" ).validate({
						rules: {
							fromDate: {
								dateISO: true
								},
							toDate: {
								dateISO: true
								}
						}
					});
					
					$( "#form-request-financial-data" ).submit(function( event ) {
							doBuildGraph();
							
							event.preventDefault();
						});
					
					});
			</script>

    			
    			
    			</div>
    			
    			
    			
  				<div class="col-md-4"></div>
  			</div>
		</div>

       	<#else>
		Nessuna stock ancora attiva. Vai nella sezione di amministrazione per attivare le stock.		
		</#if>
	</div>
	<div class="container-fluid">
  			<div class="row">
    			<div class="col-md-3"></div>
    			
    			<div class="col-md-6">
					<div class='content_graph'>
					</div>
				</div>
				
				<div class="col-md-3"></div>
			</div>
	</div>
</@u.page>
