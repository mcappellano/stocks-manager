import java.util.Date
import java.text.ParseException

import groovy.json.JsonBuilder

import com.simplethings.gestioneserietitoli.utils.*
import com.simplethings.gestioneserietitoli.model.*
import com.simplethings.gestioneserietitoli.db.*
import com.simplethings.gestioneserietitoli.conf.*

import com.simplethings.gestioneserietitoli.web.MyStock

import static com.simplethings.gestioneserietitoli.web.utils.DataFormatter.*

def json = new JsonBuilder()

Config configApp = Config.getConfig("/config.properties");
WebAppApplicationDataSourceContainer appContainer = 
new WebAppApplicationDataSourceContainer(configApp.dataSourceUrl);

StockInformationDAO stockInformationDAO = new StockInformationDAO(appContainer.dataSource);

StockSeriesDAO stockMonthSeriesDAO = new StockSeriesDAO(appContainer.dataSource);

def stockNameValue = request.getParameter("stockName");
def startDateValue = request.getParameter("startDate");
def endDateValue = request.getParameter("endDate");

if(!stockNameValue){
	stockNameValue = "IBM";
}

def listOsStocks = stockInformationDAO.listOfActiveStock

def stockDescription = null

listOsStocks.any {
	if(it.stockName == stockNameValue)
		stockDescription = it.stockDescription
}

def startDate = null 
try {
	startDate = (startDateValue) ? Date.parse("yyyyMMdd", startDateValue) : null
} catch (ParseException e) {}

def endDate = null
try {
	endDate =  (endDateValue) ? Date.parse("yyyyMMdd", endDateValue) : null
} catch (ParseException e) {}	
	

def results = null
if (startDate == null && endDate == null)
 	results = stockMonthSeriesDAO.load(stockNameValue)
else if (startDate != null && endDate != null)
	results = stockMonthSeriesDAO.loadIntervall(stockNameValue, startDate, endDate)
else if (startDate)
	results = stockMonthSeriesDAO.loadFromDate(stockNameValue, startDate)
else if (endDate)
	results = stockMonthSeriesDAO.loadToDate(stockNameValue, endDate)
	
def accList = []

Iterator iter = results.iterator();

while (iter.hasNext()){
	StockMonthSeries g = iter.next();
	iter.remove()
	def listFinanceDataSet = g.financeDataSet
	listFinanceDataSet.each { v ->
		accList << new MyStock(v.date.format("yyyy_MM_dd"), v.openValue,
			v.hightValue, v.lowValue, v.closeValue, v.volume, v.adjCloseValue)
	}
	
}

json.stocks {
	stock_name stockNameValue
	if(stockDescription)
		stock_description stockDescription	
	
	if(startDate)
		start_date startDateValue
	if(endDate)
		end_date endDateValue
		
	values accList
}
		
response.contentType = 'application/json'
out << json.toPrettyString()


