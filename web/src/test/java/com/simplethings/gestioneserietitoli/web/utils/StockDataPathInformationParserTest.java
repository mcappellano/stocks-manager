package com.simplethings.gestioneserietitoli.web.utils;

import java.text.ParseException;
import java.util.Date;

import junit.framework.TestCase;

public class StockDataPathInformationParserTest extends TestCase {

	public void testOKStockDataPathInformationParser() {
		try {
			StockDataPathInformationParser sdp = new StockDataPathInformationParser("/IBM/");
			assertEquals("IBM", sdp.getStockCode());
			assertNull(sdp.getFromDate());
			assertNull(sdp.getToDate());
			
		} catch (ParseException e) {
			fail("should not fail");
		}
	}
	
	public void testOKStockDataPathInformationParserWithFromDate() {
		try {
			Date dateValueExpected = DataFormatter.doParse("20120120", DataFormatter.PATTERN_JSON2);
			StockDataPathInformationParser sdp = new StockDataPathInformationParser("/IBM/F20120120");
			assertEquals("IBM", sdp.getStockCode());
			assertEquals(dateValueExpected, sdp.getFromDate());
			assertNull(sdp.getToDate());
			
		} catch (ParseException e) {
			fail("should not fail");
		}
	}
	
	public void testOKStockDataPathInformationParserWithToDate() {
		try {
			Date dateValueExpected = DataFormatter.doParse("20120120", DataFormatter.PATTERN_JSON2);
			StockDataPathInformationParser sdp = new StockDataPathInformationParser("/IBM/T20120120");
			assertEquals("IBM", sdp.getStockCode());
			assertNull(sdp.getFromDate());
			assertEquals(dateValueExpected, sdp.getToDate());
			
		} catch (ParseException e) {
			fail("should not fail");
		}
	}
	
	public void testOKStockDataPathInformationParserWithFromAndToDate() {
		try {
			Date dateValueExpectedFrom = DataFormatter.doParse("20120120", DataFormatter.PATTERN_JSON2);
			Date dateValueExpectedTo = DataFormatter.doParse("20130120", DataFormatter.PATTERN_JSON2);
			StockDataPathInformationParser sdp = new StockDataPathInformationParser("/IBM/F20120120/T20130120");
			assertEquals("IBM", sdp.getStockCode());
			assertEquals(dateValueExpectedFrom, sdp.getFromDate());
			assertEquals(dateValueExpectedTo, sdp.getToDate());
			
		} catch (ParseException e) {
			fail("should not fail");
		}
	}
	
	public void testErrorStockDataPathInformationParserWithFromAndToDate() {
		try {
			StockDataPathInformationParser sdp = new StockDataPathInformationParser("/");
			fail("should fail");
			
		} catch (ParseException e) {
			
		}
	}
	
	public void testStockDataPathInformationParser() {
		try {
			new StockDataPathInformationParser("");
			fail("should not fail");
		} catch (ParseException e) {
			
		}
	}

}
