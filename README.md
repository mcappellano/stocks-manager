README
======

Application that find and store stock data using *Yahoo financial data*. Es: http://ichart.yahoo.com/table.csv?s=GOOGL

API used
--------
> Web services used to gather datas about stocks and stock's historical quotes.
> 
> * for stock's finacial datas (stock's historical quotes); here there is [api description](https://code.google.com/p/yahoo-finance-managed/wiki/YahooFinanceAPIs) of *Yahoo financial data*.
> 
> * for stock's informations, like stock name, stock code, description using this service:  
> `http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.industry where id in (select industry.id from yahoo.finance.sectors)&env=store://datatables.org/alltableswithkeys&format=json`,  
> with url escaping becomes:  
> `http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.industry%20where%20id%20in%20%28select%20industry.id%20from%20yahoo.finance.sectors%29&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&format=json`  

Deploy
------
Application can by deployed on *OpenShift* as a `jbossews` simply by committing in your `jbossews` cartridge or can run locally in Jetty.

How deploy locally in Jetty
---------------------------
Application user [H2 Database Engine](http://www.h2database.com/html/main.html); View documentation for creating a DB -- is very simple.

DB schema configuration is in `./core/schema/create.sql`.

Data source configuration is in `./web/src/main/webapp/WEB-INF/jetty-env.xml`.

How deploy on OpenShift platform
--------------------------------
Nothing to do: only commit from this directory: `git commit push`  

In `./openshift/config/context.xml` there is the DB and schema creation if not present and run automatically .

Description
-----------

User can choose the stock to view. If stock isn't yet monitored, user can add stock to be monitored from **admin** menu. User can even unregister stock to be monitored.

When a stock is monitored, the data are stored in a local db like numeric series to reduce data on db.

STUFF INFORMATION
-----------------

Other projects information on [project wiki](https://bitbucket.org/mcappellano/stocks-manager/wiki/Home)

DOC OPENSHIFT
-------------
The OpenShift `jbossews` cartridge documentation can be found at:

http://openshift.github.io/documentation/oo_cartridge_guide.html#tomcat

